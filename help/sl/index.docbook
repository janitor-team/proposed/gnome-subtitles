<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE article PUBLIC "-//OASIS//DTD DocBook XML V4.1.2//EN" "http://www.oasis-open.org/docbook/xml/4.1.2/docbookx.dtd" [
<!ENTITY legal SYSTEM "legal.xml">
<!ENTITY appversion "0.4">
<!ENTITY manrevision "1.0">
<!ENTITY date "May 2007">
<!ENTITY app "<application>Gnome Subtitles</application>">
]>
<!--
    (Do not remove this comment block.)
    Maintained by the GNOME Documentation Project
    http://developer.gnome.org/projects/gdp
    Template version: 2.0 beta
    Template last modified Apr 11, 2002
    -->
<!-- =============Document Header ============================= -->
<article id="index" lang="sl">
  <!-- please do not change the id; for translations, change lang to -->
  <!-- appropriate code -->
  <articleinfo>
    <title><application>Gnome Podnapisi</application> Priročnik R1.0</title>
    
    <copyright lang="en">
      <year>2007</year>
      <holder>Erin Bloom</holder>
    </copyright>
    <abstract role="description"><para>Delovanje <application>Gnome Podnapisov</application></para>
    </abstract>
    <!-- An address can be added to the publisher information.  If a role is
          not specified, the publisher/author is the same for all versions of the
          document.  -->
    <publisher>
      <publishername>Dokumentacijski projekt GNOME</publishername>
    </publisher>
    
      <legalnotice id="legalnotice">
	<para>Dovoljeno je kopiranje in razširjanje dokumenta pod pogoji GNU Prostega dovoljenja dokumentacije (GFDL), različice 1.1 ali kasnejše, kot je objavljena s strani Free Software Foundation. Kopijo dokumenta lahko najdete med <ulink type="help" url="ghelp:fdl">razdelki pomoči</ulink> ali pa v datoteki COPYING-DOCS, ki je priložena programu.</para>
         <para>Priročnik je del zbirke priročnikov GNOME distribuiranih pod GFDL. V primeru da želite ta priročnik distribuirati ločeno od zbirke, lahko to storite z dodajanjem kopije licence priročniku, kot je opisano v 6. odseku licence.</para>

	<para>Veliko imen, ki jih uporabijo podjetja za ločevanje njihovih produktov in storitev so zahtevane kot blagovna znamka. Če se ta imena pojavijo v katerikoli dokumentaciji GNOME in se člani dokumentacijskega projekta gnome zavedajo blagovnih znamk, so navedena v velikih črkah ali začetnih velikih črkah.</para>

	<para lang="en">
	  DOCUMENT AND MODIFIED VERSIONS OF THE DOCUMENT ARE PROVIDED
	  UNDER  THE TERMS OF THE GNU FREE DOCUMENTATION LICENSE
	  WITH THE FURTHER UNDERSTANDING THAT:

	  <orderedlist>
		<listitem>
		  <para lang="en">DOCUMENT IS PROVIDED ON AN "AS IS" BASIS,
                    WITHOUT WARRANTY OF ANY KIND, EITHER EXPRESSED OR
                    IMPLIED, INCLUDING, WITHOUT LIMITATION, WARRANTIES
                    THAT THE DOCUMENT OR MODIFIED VERSION OF THE
                    DOCUMENT IS FREE OF DEFECTS MERCHANTABLE, FIT FOR
                    A PARTICULAR PURPOSE OR NON-INFRINGING. THE ENTIRE
                    RISK AS TO THE QUALITY, ACCURACY, AND PERFORMANCE
                    OF THE DOCUMENT OR MODIFIED VERSION OF THE
                    DOCUMENT IS WITH YOU. SHOULD ANY DOCUMENT OR
                    MODIFIED VERSION PROVE DEFECTIVE IN ANY RESPECT,
                    YOU (NOT THE INITIAL WRITER, AUTHOR OR ANY
                    CONTRIBUTOR) ASSUME THE COST OF ANY NECESSARY
                    SERVICING, REPAIR OR CORRECTION. THIS DISCLAIMER
                    OF WARRANTY CONSTITUTES AN ESSENTIAL PART OF THIS
                    LICENSE. NO USE OF ANY DOCUMENT OR MODIFIED
                    VERSION OF THE DOCUMENT IS AUTHORIZED HEREUNDER
                    EXCEPT UNDER THIS DISCLAIMER; AND
		  </para>
		</listitem>
		<listitem>
		  <para lang="en">UNDER NO CIRCUMSTANCES AND UNDER NO LEGAL
                       THEORY, WHETHER IN TORT (INCLUDING NEGLIGENCE),
                       CONTRACT, OR OTHERWISE, SHALL THE AUTHOR,
                       INITIAL WRITER, ANY CONTRIBUTOR, OR ANY
                       DISTRIBUTOR OF THE DOCUMENT OR MODIFIED VERSION
                       OF THE DOCUMENT, OR ANY SUPPLIER OF ANY OF SUCH
                       PARTIES, BE LIABLE TO ANY PERSON FOR ANY
                       DIRECT, INDIRECT, SPECIAL, INCIDENTAL, OR
                       CONSEQUENTIAL DAMAGES OF ANY CHARACTER
                       INCLUDING, WITHOUT LIMITATION, DAMAGES FOR LOSS
                       OF GOODWILL, WORK STOPPAGE, COMPUTER FAILURE OR
                       MALFUNCTION, OR ANY AND ALL OTHER DAMAGES OR
                       LOSSES ARISING OUT OF OR RELATING TO USE OF THE
                       DOCUMENT AND MODIFIED VERSIONS OF THE DOCUMENT,
                       EVEN IF SUCH PARTY SHALL HAVE BEEN INFORMED OF
                       THE POSSIBILITY OF SUCH DAMAGES.
		  </para>
		</listitem>
	  </orderedlist>
	</para>
  </legalnotice>


    
    <authorgroup>
      <author role="maintainer" lang="en">
        <firstname>Erin</firstname>
        <surname>Bloom</surname>
        <affiliation>
          <orgname>GNOME Documentation Project</orgname>
          <address> <email>doc-writer2@gnome.org</email> </address>
        </affiliation>
      </author>

<!-- This is appropriate place for other contributors: translators,
      maintainers,  etc. Commented out by default.
      
      <othercredit role="translator">
        <firstname>Latin</firstname>
        <surname>Translator 1</surname>
        <affiliation>
          <orgname>Latin Translation Team</orgname>
          <address> <email>translator@gnome.org</email> </address>
        </affiliation>
        <contrib>Latin translation</contrib>
      </othercredit>
      -->
    </authorgroup>
    
  
  <!-- According to GNU FDL, revision history is mandatory if you are -->
  <!-- modifying/reusing someone else's document.  If not, you can omit it. -->
  <!-- Remember to remove the &manrevision; entity from the revision entries other -->
  <!-- than the current revision. -->
  <!-- The revision numbering system for GNOME manuals is as follows: -->
  <!-- * the revision number consists of two components -->
  <!-- * the first component of the revision number reflects the release version of the GNOME desktop. -->
  <!-- * the second component of the revision number is a decimal unit that is incremented with each revision of the manual. -->
  <!-- For example, if the GNOME desktop release is V2.x, the first version of the manual that -->
  <!-- is written in that desktop timeframe is V2.0, the second version of the manual is V2.1, etc. -->
  <!-- When the desktop release version changes to V3.x, the revision number of the manual changes -->
  <!-- to V3.0, and so on. -->
  <revhistory>
    <revision lang="en">
      <revnumber>gnome-subtitles Manual V1.0</revnumber>
      <date>May 2007</date>
      <revdescription>
        <para role="author" lang="en">Erin Bloom
          <email>docwriter2@gnome.org</email>
        </para>
        <para role="publisher" lang="en">GNOME Documentation Project</para>
      </revdescription>
    </revision>
  </revhistory>
  
  <releaseinfo>Ta priročnik opisuje gnome-podnapise različico 0.4</releaseinfo>
  <legalnotice>
    <title>Odziv</title>
    <para lang="en">To report a bug or make a suggestion regarding the <application>Gnome Subtitles</application> application or
      this manual, follow the directions in the <ulink url="help:gnome-feedback" type="help">GNOME Feedback Page</ulink>.
    </para>
    <!-- Translators may also add here feedback address for translations -->
  </legalnotice>
</articleinfo>
<!-- Index Section -->
<indexterm zone="index" lang="en">
  <primary>Gnome Subtitles</primary>
</indexterm>
<indexterm zone="index" lang="en">
  <primary>gnomesubtitles</primary>
</indexterm>

<!-- ============= Document Body ============================= -->
<!-- ============= Introduction ============================== -->
<!-- Use the Introduction section to give a brief overview of what
    the application is and what it does. -->
<sect1 id="gs-introduction">
  <title>Uvod</title>
  <para><application>Gnome Podnapisi</application> je urejevalnik podnapisov za namizje GNOME.<application>Gnome Subtitles</application> podpira najbolj pogoste besedilne zapise podnapisov in omogoča urejanje, pretvarjanje in usklajevanje podnapisov. Njegove zmožnosti sledijo.</para>
  <itemizedlist>
    <listitem>
      <para>Zapisi podnapisov:</para> 
      <itemizedlist> 
        <listitem><para>Napredna Sub Station Alpha</para></listitem> 
        <listitem><para>MicroDVD</para></listitem> 
        <listitem><para>MPlayer</para></listitem> 
        <listitem><para>Mplayer 2</para></listitem> 
        <listitem><para>MPSub</para></listitem> 
        <listitem><para>SubRip</para></listitem> 
        <listitem><para>Sub Station Alpha</para></listitem> 
        <listitem><para>SubViewer 1.0</para></listitem> 
        <listitem><para>SubViewer 2.0</para></listitem> 
      </itemizedlist> 
    </listitem> 
    <listitem><para>Vgrajeni video predogled</para> 
      <itemizedlist>
        <listitem><para>Uporablja mplayer kot zaledje</para></listitem>
        <listitem><para>Prikaže dolžino videa trenutni položaj</para></listitem>
        <listitem><para>Nastavi čas podnapisov na osnovi položaja videa</para></listitem>
        <listitem><para>Samodejno izbere video pri odpiranju podnapisov</para></listitem>
      </itemizedlist>
    </listitem> 
    <listitem>
      <para>Uporabniški vmesnik</para>
      <itemizedlist>
        <listitem><para>KVJTKD-Kar vidiš je to kar dobiš</para></listitem>
        <listitem><para>Urejanje glav podnapisov</para></listitem>
        <listitem><para>Najdi in zamenjaj, vključujoč podporo za logične izraze</para></listitem>
        <listitem><para>Razveljavi/Uveljavi</para></listitem>
      </itemizedlist>
    </listitem>
    <listitem>
      <para>Dejanja časov</para>
      <itemizedlist>
        <listitem><para>Samodejno prilagajanje časov osnovano na 2 točkah časa/usklajevanja</para></listitem>
        <listitem><para>Zamik podnapisov za določen zamik (ki je lahko osnovan na videu)</para></listitem>	
        <listitem><para>Pretvori med hitrostmi sličic</para></listitem>
        <listitem><para>Urejanje časov in okvirjev</para></listitem>
      </itemizedlist>
    </listitem>
    <listitem><para>Druge zmožnosti</para>
      <itemizedlist>
        <listitem><para>Znakovno kodiranje samodejno zazna zapis podnapisa (ob odpiranju)</para></listitem>
        <listitem><para>Izbira več znakovnih kodiranj</para></listitem>
        <listitem><para>Sprosti branje podnapisov za branje podnapisov, ki vsebujejo napake</para></listitem>
      </itemizedlist>
    </listitem>
  </itemizedlist>
</sect1>

<!-- =========== Getting Started ============================== -->
<!-- Use the Getting Started section to describe the steps required
    to start the application and to describe the user interface components
  of the application. If there is other information that it is important
    for readers to know before they start using the application, you should
    also include this information here. 
    If the information about how to get started is very short, you can 
    include it in the Introduction and omit this section. -->

<sect1 id="gs-getting-started">
  <title>Kako začeti</title> 
  
  <sect2 id="gs-start">
    <title>Za zagon <application>Gnome Podnapisi</application></title>
    <para><application><application>Gnome Podnapise</application></application> lahko zaženete na naslednje načine:</para>
    <variablelist>
      <varlistentry>
        <term>meni <guimenu>Programi</guimenu></term>
        <listitem>
          <para lang="en">Choose
            <menuchoice>
              <guisubmenu>Sound &amp; Video</guisubmenu>
              <guimenuitem>Gnome Subtitles</guimenuitem>
            </menuchoice>. </para>
        </listitem>
      </varlistentry>
      <varlistentry>
        <term>Ukazna vrstica</term>
        <listitem>
          <para lang="en">Type <command>gnome-subtitles</command> <replaceable>filename</replaceable> and press <keycap>Enter</keycap>.</para>
          <para>Argument <replaceable>ime datoteke</replaceable> je izbiren. V primeru, da je naveden, bo program ob zagonu odprl navedeno datoteko.</para>
        </listitem>
      </varlistentry>
    </variablelist>
  </sect2>
</sect1>
<sect1 id="gs-working-with-files">
  <title>Delo z datotekami</title>
  <sect2 id="gs-creating-new-doc">
    <title>Ustvarjanje novega dokumenta podnapisov</title>
    <procedure>
      <title>Ustvarjanje novega dokumenta podnapisov</title>
      <step performance="required">
      <para lang="en">
      Go to <menuchoice>
              <guisubmenu>File</guisubmenu>
              <guimenuitem>New</guimenuitem>
            </menuchoice>. </para>
      <para>V urejevalniku bi morala biti odprta nova datoteka</para>
      </step>
      </procedure>
  </sect2>
    <sect2 id="gs-opendingfile">
    <title>Odpiranje datoteke</title>
      <procedure>
      <title>Odpiranje datoteke</title>
      <step performance="required">
      <para lang="en">
      Go to <menuchoice>
              <guisubmenu>File</guisubmenu>
              <guimenuitem>Open</guimenuitem>
            </menuchoice>. </para>
      <para>Odpreti bi se morali okno za odpiranje datotek</para>
      </step>
      <step performance="required">
      <para>Izberite datoteko podnapisov, ki jo želite odpreti.</para>
      <substeps>
      <step performance="optional">
      <para lang="en">If you want to specify the file's character coding, choose one from the Character Coding list. If not specified, the caracter coding will be auto detected.</para>
      </step>
      <step performance="optional">
      <para>V primeru, da želite video odpreti nemudoma, izberite video s seznama videov.</para>
      </step>
      </substeps>
      </step>
      </procedure>
      <note><para>Pri izbiranju videa lahko izberete le videe v trenutni mapi. V primeru da video za odpiranje ni v isti mapi kot datoteka podnapisov, lahko video odprete po odpiranju datoteke podnapisov.</para>
      </note>
  </sect2>
    <sect2 id="gs-saving-file">
    <title>Shranjevanje datoteke</title>
      <procedure>
      <title>Shranjevanje datoteke</title>
      <para>Datoteko lahko shranite običajno, ali pa uporabite Shrani kot za nastavitev različnih možnosti.</para>
      <step performance="required">
      <para lang="en">
      Go to <menuchoice>
              <guisubmenu>File</guisubmenu>
              <guimenuitem>Save</guimenuitem>
            </menuchoice>. </para>
      <para>Odpreti bi se morali okno za odpiranje datotek</para>
      </step>
    </procedure>
    <note><para>V primeru, da shranjujete novo datoteko se bo pojavilo okno za shranjevanje</para></note>
    <procedure>
      <title>Shranjevanje kot novo datoteko</title>
      <step performance="required">
      <para lang="en">
      Go to <menuchoice>
              <guisubmenu>File</guisubmenu>
              <guimenuitem>Save As</guimenuitem>
            </menuchoice>. </para>
      <para>Odpreti bi se moralo okno za shranjevanje</para>
      </step>
      <step performance="required"><para>Vnesite novo ime v vašo datoteko podnapisov</para></step>
      <step performance="optional"><para>V primeru, da želite shraniti datoteko na drugem mestu, prebrskajte na novo mesto v brskalniku datotek</para></step>
      <step performance="optional"><para>V primeru, da želite shraniti datoteko v drugem zapisu, kot naštetem v seznamu zapisov podnapisov, izberite drug zapis.</para></step>
      <step performance="optional"><para>V primeru, da želite shraniti datoteko v drugem znakovnem kodiranju kot privzetem znakovnem kodiranju, spremenite kodiranje v seznamu znakovnih kodiranj.</para></step>
      <step performance="required"><para>Kliknite shrani</para></step>
    </procedure>
  </sect2>
  
    <sect2 id="gs-select-char-coding">
    <title>Izbiranje znakovnega kodiranja</title>
    <para>V primeru da v vaši datoteki podnapisov uporabljate posebne zanke, se boste želeli prepričati da je vaša datoteka shranjena v kodiranju, ki podpira te znake.</para>
    <caution><para>Shranjevanje datoteke v znakovnem kodiranju, ki ne podpira posebnih znakov, bo povzročilo izgubo podatkov o znakih. To se zgodi ob poskusu shranjevanja več bajtih znakov in znakovnem kodiranju enega bajta.</para></caution>
  </sect2>
    <sect2 id="gs-editing-headers">
    <title>Urejanje glav podnapisov</title>

        <procedure>
  <para>Nekateri zapisi podnapisov vsebujejo glave datotek, ki vsebujejo podrobnosti o datoteki. Ta polja lahko urejate v <application>Gnome Podnapisih</application> z</para>
      <step performance="required">
      <para lang="en">
      Go to <menuchoice>
              <guisubmenu>File</guisubmenu>
              <guimenuitem>Headers</guimenuitem>
            </menuchoice>. </para>
      <para>Odprlo se bo okno glav. Vsebuje 4 zavihke.</para>
      </step>
      <step performance="required"> <para>Izberite zavihek, ki ustreza zapisu vaše datoteke podnapisov</para></step>
      <step performance="required"><para>Izpolnite primerna polja.</para></step>
      <step performance="required"><para>Ko ste končali z vnašanjem podatkov glave, kliknite v redu</para></step>
    </procedure>
  </sect2>
</sect1>
    <!-- ============= Working with Subtitles ================================== -->
<sect1 id="gs-working-with-subs">
<title>Delo s podnapisi</title>
<note><para>Ta pomoč našteje tipkovne bližnjice za izvajanje nalog. Vse naloge je mogoče namesto s tipkovnimi bližnjicami mogoče izvesti z menijem Uredi.</para></note>
      <sect2 id="gs-wws-adding">
        <title>Dodajanje podnapisa</title>
        <para>Novi podnapisi so dodani pred ali za trenutni podnapis.</para>
        <procedure>
          <title>Za dodajanje novega podnapisa za trenutni podnapis</title>
      <step performance="required">
      <para lang="en">Type <keycap>Insert</keycap> or <keycap>Ctrl</keycap>+<keycap>Enter</keycap></para>
      </step>
    </procedure>
            <procedure>
          <title>Za dodajanje podnapisa pred trenutnim podnapisom</title>
      <step performance="required">
      <para lang="en">Type <keycap>Shift</keycap>+<keycap>Ctrl</keycap>+<keycap>Enter</keycap></para>
      </step>
    </procedure>
      </sect2>     
      <sect2 id="gs-wws-moving">
        <title>Premikanje med podnapisi</title>
        <para>Za izbiro podnapisa lahko uporabite miško in kliknete podnapis, ali uporabite tipke na tipkovnici za premik med podnapisi, ko je kazalec v oknu urejanja</para>
        <procedure>
          <title>Za premik na naslednji podnapis</title>
      <step performance="required">
      <para lang="en">Type <keycap>Ctrl</keycap>+<keycap>Page Down</keycap></para>
      </step>
    </procedure>
            <procedure>
          <title>Za premik na predhodni podnapis</title>
      <step performance="required">
      <para lang="en">Type <keycap>Ctrl</keycap>+<keycap>Page Up</keycap></para>
      </step>
    </procedure>
      </sect2>     
      <sect2 id="gs-wws-removing">
        <title>Odstranjevanje podnapisov</title>
      <procedure><step><para>Pritisnite <keycap>Izbriši</keycap></para></step></procedure>
      </sect2>      
      <sect2 id="gs-wws-batch">
        <title>Delo z več podnapisi</title>
        <para lang="en">Sometimes you will want to select multiple subtitles.  Use <keycap>Shift</keycap> to
          select sequential subtitles, and <keycap>Ctrl</keycap> to select non-sequential subtitles</para>
        <para lang="en">To select all subtitles type <keycap>Ctrl</keycap>+<keycap>A</keycap></para>
      </sect2>      
      <sect2 id="gs-wws-editing">
        <title>Urejanje besedila</title>
        <para>Za urejanje besedila v podnapisu</para>
          <procedure>
            <step><para>Izberite podnapis</para></step>
            <step><para>Kliknite okno uredi za premik kazalca v okno urejanja</para></step>
          </procedure>
      </sect2>
      <sect2 id="gs-wws-format">
        <title>Oblikovanje besedila</title>
        <note><para>Obliko zapisa (krepko, poševno, podčrtano) je mogoče uveljaviti samo v celotni vrstici. Trenutno morate za posebno oblikovanje le določenih znakov urediti datoteko podnapisa v urejevalniku besedila.</para></note>
      </sect2>
      <sect2 id="gs-wws-oops">
        <title>Razveljavljanje in uveljavljanje</title>
        <procedure><title>Za razveljavljanje dejanja</title><step><para lang="en">Type <keycap>Ctrl</keycap>+
              <keycap>Z</keycap></para></step></procedure>
        <procedure><title>Za uveljavljanje dejanja</title><step><para lang="en">Type <keycap>Ctrl</keycap>+
              <keycap>Y</keycap> or <keycap>Ctrl</keycap>+<keycap>Shift</keycap>+<keycap>Z
              </keycap></para></step></procedure>
      </sect2>      
      <sect2 id="gs-wws-ccp">
        <title>Izrezovanje, kopiranje, prilepljenje </title>
        <para>Besedilo lahko izrežete kopirate in prilepite v podnapis.</para>
        <procedure><title>Za kopiranje besedila</title><step><para lang="en"><keycap>Ctrl</keycap>+
              <keycap>C</keycap></para></step></procedure>
        <procedure><title>Za izrez besedila</title><step><para lang="en"><keycap>Ctrl</keycap>+
              <keycap>X</keycap></para></step></procedure>
        <procedure><title>Za prilepljenje besedila</title><step><para lang="en"><keycap>Ctrl</keycap>+
              <keycap>V</keycap></para></step></procedure>
      </sect2>
</sect1>
    <!-- ============= find/replace ================================== -->
<!--    <sect1 id="gs-findreplace">
      <title>Finding and Replacing</title>
      <sect2 id="gs-fr-finding">
        <title>Finding</title>
        <para>Questions and general discussions should be posted on the <ulink url="http://sourceforge.net/forum/?group_id=129996" type="http">Forum</ulink> (no registration needed).</para>
      </sect2>
            <sect2 id="gs-fr-replacing">
        <title>Replacing</title>
        <para>You might get a quick response by asking the <ulink url="http://lists.sourceforge.net/lists/listinfo/gnome-subtitles-general" type="http">Mailing List</ulink> (<ulink url="http://sourceforge.net/mailarchive/forum.php?forum=gnome-subtitles-general" type="http">archives</ulink> are available).</para>
      </sect2>
      <sect2 id="gs-fr-options">
        <title>Find and Replace Options</title>
        <para>Questions and general discussions should be posted on the <ulink url="http://sourceforge.net/forum/?group_id=129996" type="http">Forum</ulink> (no registration needed).</para>
      </sect2>
      <sect2 id="gs-regex">
        <title>Using Regular Expressions</title>
        <para>Bugs and Feature Requests can be reported at the official <ulink url="http://bugzilla.gnome.org/browse.cgi?product=gnome-subtitles" type="http">Gnome Bugzilla</ulink>.</para>
      </sect2>
    </sect1>
-->
    <!-- ============= Timings ================================== -->
    <sect1 id="gs-timings">
      <title>Upravljanje s podnapisi</title>
      <sect2 id="gs-timings-settingview">
        <title>Nastavljanje enot podnapisov</title>
        <para>V programu <application>Gnome Podnapisi</application> lahko nastavite in prilagodite podnapise v časovnih enotah (minute in sekunde) ali v enotah sličic. Privzeta enota je čas.</para>
            <procedure><title>Za uporabo enot sličic</title>
              <step><para lang="en">Go to <menuchoice>
              <guisubmenu>View</guisubmenu>
              <guimenuitem>Frames</guimenuitem>
            </menuchoice>. </para></step></procedure>
              <procedure><title>Za uporabo enot časa</title>
              <step><para lang="en">Go to <menuchoice>
              <guisubmenu>View</guisubmenu>
              <guimenuitem>Times</guimenuitem>
            </menuchoice>. </para></step></procedure>
      </sect2>
      <sect2 id="gs-timings-Adjusting">
        <title>Prilagajanje časov</title>
           <procedure>
             <step><para>V primeru da želite prilagoditi samo čas nekaj podnapisov izberite podnapise za katere želite prilagoditi čase</para></step>
              <step><para lang="en">Go to <menuchoice>
              <guisubmenu>Timings</guisubmenu>
              <guimenuitem>Adjust</guimenuitem>
            </menuchoice>. </para></step>
            <step><para>Izberite nov začetni čas prvega podnapisa in nov začetni čas zadnjega podnapisa</para></step>
      </procedure>
      </sect2>
            <sect2 id="gs-timings-Shifitng">
        <title>Zamikanje časov</title>
        <para>Zamik časov omogoča zamik enega ali več podnapisov naprej ali nazaj za določeno količino. Za razliko od zmožnosti Prilagodi podnapise, zamikanje podnapisov ne vpliva na dolžino podnapisov.</para>
              <procedure><title>Za uporabo zamika podnapisov</title>
                <step><para>Izberite kateri podnapis želite zamakniti. V primeru, da želite zamakniti čas/okvir enega ali vseh podnapisov pred ali za določenim podnapisom, izberite ta podnapis, če pa želite zamakniti več kot en podnapis, izberite vse podnapise, ki jih želite zamakniti.</para></step>
              <step><para lang="en">Go to <menuchoice>
              <guisubmenu>Timings</guisubmenu>
              <guimenuitem>Shift</guimenuitem>
            </menuchoice>. </para></step>
            <step><para>Vnesite čas (ali sličice, če uporabljate enote sličic) za prilagoditev podnapisov. V primeru, da želite podnapise premakniti nazaj, vnesite negativno vrednost, sicer pa vnesite pozitivno vrednost.</para></step>
            <step><para>Izberite kako naj se zamaknejo podnapisi.</para></step>
            <step><para>Kliknite dvigalko za zamik podnapisov.</para></step>
      </procedure>
      </sect2>
      <sect2 id="gs-timings-inputfr">
        <title>Nastavljanje hitrosti sličic vhoda in videa</title>
        <para>Ko odprete video, <application>Gnome Podnapisi</application> hitrosti sličic videa ne nastavijo samodejno. </para>
        <para><application>Gnome Podnapisi</application> upravljajo s podnapisi na osnovi časa, ne glede na način ogleda. Zato ob spremembi hitrosti sličic videa <application>Gnome Podnapisi</application> prilagodijo okvirje za nadomestitev spremembe. Trajanje podnapisov ostane enako.</para>
        <caution><para>V primeru, da vrsta zapisa vaših podnapisov za določitev prikaza podnapisa uporablja sličice (MicroDVD), potem lahko sprememba hitrosti sličic videa povzroči neusklajenost podnapisov.</para></caution>
        <procedure><title>Nastavitev hitrosti sličic videa</title>
              <step><para lang="en">Go to <menuchoice>
              <guisubmenu>Timings</guisubmenu>
              <guimenuitem>Video Frame Rate</guimenuitem>
            </menuchoice> and select the appropriate framerate.</para></step>
      </procedure>
      <procedure><title>Nastavitev vhodne hitrosti sličic</title>
              <step><para lang="en">Go to <menuchoice>
              <guisubmenu>Timings</guisubmenu>
              <guimenuitem>Input Frame Rate</guimenuitem>
            </menuchoice> and select the appropriate framerate.</para></step>
      </procedure>
      </sect2>
    </sect1>
     <!-- ============= Video ================================== -->
    <sect1 id="gs-video">
      <title>Delo z videi</title>
      <sect2 id="gs-video-opening">
        <title>Odpiranje in zapiranje videa</title>
        <procedure>
          <title>Za odprtje videa</title>
          <step><para lang="en">Go to <menuchoice>
              <guisubmenu>Video</guisubmenu>
              <guimenuitem>Open</guimenuitem>
            </menuchoice>
          </para></step>
        </procedure>
        <procedure>
          <title>Za zapiranje videa</title>
          <step><para lang="en">Go to <menuchoice>
              <guisubmenu>Video</guisubmenu>
              <guimenuitem>Close</guimenuitem>
            </menuchoice>
          </para></step>
        </procedure>
      </sect2>
      <sect2 id="gs-video-playback">
        <title>Predvajanje</title>
        <para><application>Gnome Podnapisi</application> bo predvajal video v oknu ogleda z vašimi podnapisi.</para>
      </sect2>
      <sect2 id="gs-video-seeking">
        <title>Iskanje do izbora</title>
        <para>Lahko greste na točko v naloženem videu, kjer se podnapis začne</para>
        <procedure>
          <title>Za iskanje podnapisa</title>
          <step><para lang="en">Go to <menuchoice>
              <guisubmenu>Video</guisubmenu>
              <guimenuitem>Seek to Selection</guimenuitem>
            </menuchoice>
          </para></step>
        </procedure>
      </sect2>
      <sect2 id="gs-video-settingsubs">
        <title>Nastavljanje položajev začetka ali konca podnapisa</title>
        <para>Čas začetka ali konca podnapisov lahko nastavite glede na točko na kateri je naloženi video v premoru</para>
        <procedure>
          <title>Za nastavljanje začetnega časa podnapisov</title>
          <step><para lang="en">Go to <menuchoice>
              <guisubmenu>Video</guisubmenu>
              <guimenuitem>Set Subtitle Start</guimenuitem>
            </menuchoice>
          </para></step>
        </procedure>
        <procedure>
          <title lang="en">To set subtitle ending time</title>
          <step><para lang="en">Go to <menuchoice>
              <guisubmenu>Video</guisubmenu>
              <guimenuitem>Set Subtitle End</guimenuitem>
            </menuchoice>
          </para></step>
        </procedure>
      </sect2>
    </sect1>
   <!-- ============= Support ================================== -->
    <sect1 id="gs-support">
      <title>Kje je mogoče dobiti dodatno podporo</title>
      <sect2 id="gs-support-forums">
        <title>Forumi</title>
        <para>Vprašanja in splošne razprave objavite na <ulink url="http://sourceforge.net/forum/?group_id=129996" type="http">forumu</ulink> (vpis ni zahtevan).</para>
      </sect2>
            <sect2 id="gs-support-mailinglist">
        <title>Dopisni seznam</title>
        <para>Hiter odgovor je mogoče dobiti na <ulink url="http://lists.sourceforge.net/lists/listinfo/gnome-subtitles-general" type="http">dopisnem seznamu</ulink>  (na voljo je tudi <ulink url="http://sourceforge.net/mailarchive/forum.php?forum_name=gnome-subtitles-general" type="http">arhiv</ulink>).</para>
      </sect2>
      <sect2 id="gs-support-bugzilla">
        <title>Hrošči in zmožnosti</title>
        <para lang="en">Bugs and Feature Requests can be reported in the <ulink url="https://gitlab.gnome.org/GNOME/gnome-subtitles/issues/" type="http">GNOME issue tracker</ulink>.</para>
      </sect2>
    </sect1>
  </article>
