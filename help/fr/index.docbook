<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE article PUBLIC "-//OASIS//DTD DocBook XML V4.1.2//EN" "http://www.oasis-open.org/docbook/xml/4.1.2/docbookx.dtd" [
<!ENTITY legal SYSTEM "legal.xml">
<!ENTITY appversion "0.4">
<!ENTITY manrevision "1.0">
<!ENTITY date "May 2007">
<!ENTITY app "<application>Gnome Subtitles</application>">
]>
<!--
    (Do not remove this comment block.)
    Maintained by the GNOME Documentation Project
    http://developer.gnome.org/projects/gdp
    Template version: 2.0 beta
    Template last modified Apr 11, 2002
    -->
<!-- =============Document Header ============================= -->
<article id="index" lang="fr">
  <!-- please do not change the id; for translations, change lang to -->
  <!-- appropriate code -->
  <articleinfo>
    <title>Manuel de <application>Gnome Subtitles</application> v1.0</title>
    
    <copyright lang="en">
      <year>2007</year>
      <holder>Erin Bloom</holder>
    </copyright>
    <abstract role="description"><para>Le fonctionnement de <application>Gnome Subtitles</application></para>
    </abstract>
    <!-- An address can be added to the publisher information.  If a role is
          not specified, the publisher/author is the same for all versions of the
          document.  -->
    <publisher>
      <publishername>Projet de documentation GNOME</publishername>
    </publisher>
    
      <legalnotice id="legalnotice">
	<para>Permission vous est donnée de copier, distribuer et/ou modifier ce document selon les termes de la Licence GNU Free Documentation License, Version 1.1 ou ultérieure publiée par la Free Software Foundation sans section inaltérable, sans texte de première page de couverture ni texte de dernière page de couverture. Vous trouverez un exemplaire de cette licence en suivant ce <ulink type="help" url="ghelp:fdl">lien</ulink> ou dans le fichier COPYING-DOCS fourni avec le présent manuel.</para>
         <para>Ce manuel fait partie de la collection de manuels GNOME distribués selon les termes de la licence de documentation libre GNU. Si vous souhaitez distribuer ce manuel indépendamment de la collection, vous devez joindre un exemplaire de la licence au document, comme indiqué dans la section 6 de celle-ci.</para>

	<para>La plupart des noms utilisés par les entreprises pour distinguer leurs produits et services sont des marques déposées. Lorsque ces noms apparaissent dans la documentation GNOME et que les membres du projet de Documentation GNOME sont informés de l'existence de ces marques déposées, soit ces noms entiers, soit leur première lettre est en majuscule.</para>

	<para lang="en">
	  DOCUMENT AND MODIFIED VERSIONS OF THE DOCUMENT ARE PROVIDED
	  UNDER  THE TERMS OF THE GNU FREE DOCUMENTATION LICENSE
	  WITH THE FURTHER UNDERSTANDING THAT:

	  <orderedlist>
		<listitem>
		  <para lang="en">DOCUMENT IS PROVIDED ON AN "AS IS" BASIS,
                    WITHOUT WARRANTY OF ANY KIND, EITHER EXPRESSED OR
                    IMPLIED, INCLUDING, WITHOUT LIMITATION, WARRANTIES
                    THAT THE DOCUMENT OR MODIFIED VERSION OF THE
                    DOCUMENT IS FREE OF DEFECTS MERCHANTABLE, FIT FOR
                    A PARTICULAR PURPOSE OR NON-INFRINGING. THE ENTIRE
                    RISK AS TO THE QUALITY, ACCURACY, AND PERFORMANCE
                    OF THE DOCUMENT OR MODIFIED VERSION OF THE
                    DOCUMENT IS WITH YOU. SHOULD ANY DOCUMENT OR
                    MODIFIED VERSION PROVE DEFECTIVE IN ANY RESPECT,
                    YOU (NOT THE INITIAL WRITER, AUTHOR OR ANY
                    CONTRIBUTOR) ASSUME THE COST OF ANY NECESSARY
                    SERVICING, REPAIR OR CORRECTION. THIS DISCLAIMER
                    OF WARRANTY CONSTITUTES AN ESSENTIAL PART OF THIS
                    LICENSE. NO USE OF ANY DOCUMENT OR MODIFIED
                    VERSION OF THE DOCUMENT IS AUTHORIZED HEREUNDER
                    EXCEPT UNDER THIS DISCLAIMER; AND
		  </para>
		</listitem>
		<listitem>
		  <para lang="en">UNDER NO CIRCUMSTANCES AND UNDER NO LEGAL
                       THEORY, WHETHER IN TORT (INCLUDING NEGLIGENCE),
                       CONTRACT, OR OTHERWISE, SHALL THE AUTHOR,
                       INITIAL WRITER, ANY CONTRIBUTOR, OR ANY
                       DISTRIBUTOR OF THE DOCUMENT OR MODIFIED VERSION
                       OF THE DOCUMENT, OR ANY SUPPLIER OF ANY OF SUCH
                       PARTIES, BE LIABLE TO ANY PERSON FOR ANY
                       DIRECT, INDIRECT, SPECIAL, INCIDENTAL, OR
                       CONSEQUENTIAL DAMAGES OF ANY CHARACTER
                       INCLUDING, WITHOUT LIMITATION, DAMAGES FOR LOSS
                       OF GOODWILL, WORK STOPPAGE, COMPUTER FAILURE OR
                       MALFUNCTION, OR ANY AND ALL OTHER DAMAGES OR
                       LOSSES ARISING OUT OF OR RELATING TO USE OF THE
                       DOCUMENT AND MODIFIED VERSIONS OF THE DOCUMENT,
                       EVEN IF SUCH PARTY SHALL HAVE BEEN INFORMED OF
                       THE POSSIBILITY OF SUCH DAMAGES.
		  </para>
		</listitem>
	  </orderedlist>
	</para>
  </legalnotice>


    
    <authorgroup>
      <author role="maintainer" lang="en">
        <firstname>Erin</firstname>
        <surname>Bloom</surname>
        <affiliation>
          <orgname>GNOME Documentation Project</orgname>
          <address> <email>doc-writer2@gnome.org</email> </address>
        </affiliation>
      </author>

<!-- This is appropriate place for other contributors: translators,
      maintainers,  etc. Commented out by default.
      
      <othercredit role="translator">
        <firstname>Latin</firstname>
        <surname>Translator 1</surname>
        <affiliation>
          <orgname>Latin Translation Team</orgname>
          <address> <email>translator@gnome.org</email> </address>
        </affiliation>
        <contrib>Latin translation</contrib>
      </othercredit>
      -->
    </authorgroup>
    
  
  <!-- According to GNU FDL, revision history is mandatory if you are -->
  <!-- modifying/reusing someone else's document.  If not, you can omit it. -->
  <!-- Remember to remove the &manrevision; entity from the revision entries other -->
  <!-- than the current revision. -->
  <!-- The revision numbering system for GNOME manuals is as follows: -->
  <!-- * the revision number consists of two components -->
  <!-- * the first component of the revision number reflects the release version of the GNOME desktop. -->
  <!-- * the second component of the revision number is a decimal unit that is incremented with each revision of the manual. -->
  <!-- For example, if the GNOME desktop release is V2.x, the first version of the manual that -->
  <!-- is written in that desktop timeframe is V2.0, the second version of the manual is V2.1, etc. -->
  <!-- When the desktop release version changes to V3.x, the revision number of the manual changes -->
  <!-- to V3.0, and so on. -->
  <revhistory>
    <revision lang="en">
      <revnumber>gnome-subtitles Manual V1.0</revnumber>
      <date>May 2007</date>
      <revdescription>
        <para role="author" lang="en">Erin Bloom
          <email>docwriter2@gnome.org</email>
        </para>
        <para role="publisher" lang="en">GNOME Documentation Project</para>
      </revdescription>
    </revision>
  </revhistory>
  
  <releaseinfo>Ce manuel documente la version 0.4 de gnome-subtitles.</releaseinfo>
  <legalnotice>
    <title>Votre avis</title>
    <para lang="en">To report a bug or make a suggestion regarding the <application>Gnome Subtitles</application> application or
      this manual, follow the directions in the <ulink url="help:gnome-feedback" type="help">GNOME Feedback Page</ulink>.
    </para>
    <!-- Translators may also add here feedback address for translations -->
  </legalnotice>
</articleinfo>
<!-- Index Section -->
<indexterm zone="index" lang="en">
  <primary>Gnome Subtitles</primary>
</indexterm>
<indexterm zone="index" lang="en">
  <primary>gnomesubtitles</primary>
</indexterm>

<!-- ============= Document Body ============================= -->
<!-- ============= Introduction ============================== -->
<!-- Use the Introduction section to give a brief overview of what
    the application is and what it does. -->
<sect1 id="gs-introduction">
  <title>Introduction</title>
  <para><application>Gnome Subtitles</application> est un éditeur de sous-titres pour l'environnement GNOME. <application>Gnome Subtitles</application> prend en charge les formats texte de sous-titres et permet l'édition, la conversion et la synchronisation de sous-tires. Ses fonctionnalités sont les suivantes :</para>
  <itemizedlist>
    <listitem>
      <para>Formats de sous-titres :</para> 
      <itemizedlist> 
        <listitem><para>Advanced Sub Station Alpha</para></listitem> 
        <listitem><para>MicroDVD</para></listitem> 
        <listitem><para>MPlayer</para></listitem> 
        <listitem><para>Mplayer 2</para></listitem> 
        <listitem><para>MPSub</para></listitem> 
        <listitem><para>SubRip</para></listitem> 
        <listitem><para>Sub Station Alpha</para></listitem> 
        <listitem><para>SubViewer 1.0</para></listitem> 
        <listitem><para>SubViewer 2.0</para></listitem> 
      </itemizedlist> 
    </listitem> 
    <listitem><para>Aperçu intégré de la vidéo</para> 
      <itemizedlist>
        <listitem><para>Utilisation de mplayer pour la lecture</para></listitem>
        <listitem><para>Affichage de la durée de la vidéo et de la position actuelle</para></listitem>
        <listitem><para>Synchronisation des sous-titres basée sur la position dans la vidéo</para></listitem>
        <listitem><para>Sélection automatique de la vidéo à l'ouverture des sous-titres</para></listitem>
      </itemizedlist>
    </listitem> 
    <listitem>
      <para>Interface utilisateur</para>
      <itemizedlist>
        <listitem><para>WYSIWYG - What you see is what you get (« Ce que vous voyez correspond à ce que vous obtenez »)</para></listitem>
        <listitem><para>Édition des en-têtes de sous-titres</para></listitem>
        <listitem><para>Rechercher et remplacer, avec la prise en charge des expressions régulières</para></listitem>
        <listitem><para>Annuler / Rétablir</para></listitem>
      </itemizedlist>
    </listitem>
    <listitem>
      <para>Opérations de synchronisation</para>
      <itemizedlist>
        <listitem><para>Ajustement automatique de la synchronisation sur base de 2 temps ou points de synchronisation</para></listitem>
        <listitem><para>Décalage des sous-titres par un délai donné (qui peut être basé sur la vidéo)</para></listitem>	
        <listitem><para>Conversion entre différentes fréquences d'images</para></listitem>
        <listitem><para>Modification des temps et des images</para></listitem>
      </itemizedlist>
    </listitem>
    <listitem><para>Autre fonctionnalités</para>
      <itemizedlist>
        <listitem><para>Auto-détection (à l'ouverture) du codage des caractères des sous-titres</para></listitem>
        <listitem><para>Choix entre différents codages de caractères</para></listitem>
        <listitem><para>Lecture indulgente des sous-titres, pour lire des sous-titres contenant des erreurs.</para></listitem>
      </itemizedlist>
    </listitem>
  </itemizedlist>
</sect1>

<!-- =========== Getting Started ============================== -->
<!-- Use the Getting Started section to describe the steps required
    to start the application and to describe the user interface components
  of the application. If there is other information that it is important
    for readers to know before they start using the application, you should
    also include this information here. 
    If the information about how to get started is very short, you can 
    include it in the Introduction and omit this section. -->

<sect1 id="gs-getting-started">
  <title>Premiers pas</title> 
  
  <sect2 id="gs-start">
    <title>Démarrage de <application>Gnome Subtitles</application></title>
    <para>Vous pouvez démarrer <application><application>Gnome Subtitles</application></application> des manières suivantes :</para>
    <variablelist>
      <varlistentry>
        <term>Menu <guimenu>Applications</guimenu></term>
        <listitem>
          <para lang="en">Choose
            <menuchoice>
              <guisubmenu>Sound &amp; Video</guisubmenu>
              <guimenuitem>Gnome Subtitles</guimenuitem>
            </menuchoice>. </para>
        </listitem>
      </varlistentry>
      <varlistentry>
        <term>Ligne de commande</term>
        <listitem>
          <para lang="en">Type <command>gnome-subtitles</command> <replaceable>filename</replaceable> and press <keycap>Enter</keycap>.</para>
          <para>Le paramètre <replaceable>nom-de-fichier</replaceable> est facultatif. Quand il est indiqué, l'application ouvre le fichier au démarrage.</para>
        </listitem>
      </varlistentry>
    </variablelist>
  </sect2>
</sect1>
<sect1 id="gs-working-with-files">
  <title>Utilisation des fichiers</title>
  <sect2 id="gs-creating-new-doc">
    <title>Création d'un nouveau fichier de sous-titres</title>
    <procedure>
      <title>Création d'un nouveau fichier de sous-titres</title>
      <step performance="required">
      <para lang="en">
      Go to <menuchoice>
              <guisubmenu>File</guisubmenu>
              <guimenuitem>New</guimenuitem>
            </menuchoice>. </para>
      <para>Un nouveau fichier devrait apparaître dans l'éditeur.</para>
      </step>
      </procedure>
  </sect2>
    <sect2 id="gs-opendingfile">
    <title>Ouverture d'un fichier</title>
      <procedure>
      <title>Ouverture d'un fichier</title>
      <step performance="required">
      <para lang="en">
      Go to <menuchoice>
              <guisubmenu>File</guisubmenu>
              <guimenuitem>Open</guimenuitem>
            </menuchoice>. </para>
      <para>La fenêtre d'ouverture de fichier devrait apparaître.</para>
      </step>
      <step performance="required">
      <para>Choisissez le fichier de sous-titres que vous voulez ouvrir.</para>
      <substeps>
      <step performance="optional">
      <para lang="en">If you want to specify the file's character coding, choose one from the Character Coding list. If not specified, the caracter coding will be auto detected.</para>
      </step>
      <step performance="optional">
      <para>Si vous voulez choisir une vidéo pour qu'elle s'ouvre immédiatement, choisissez une vidéo dans la liste des vidéos.</para>
      </step>
      </substeps>
      </step>
      </procedure>
      <note><para>Quand vous choisissez une vidéo, vous pouvez seulement choisir parmi les vidéos du dossier actuel. Si la vidéo que vous voulez ouvrir n'est pas dans le même répertoire que le fichier de sous-titres, vous pourrez l'ouvrir après avoir ouvert le fichier de sous-titres.</para>
      </note>
  </sect2>
    <sect2 id="gs-saving-file">
    <title>Enregistrement d'un fichier</title>
      <procedure>
      <title>Enregistrement d'un fichier</title>
      <para>Vous pouvez soit enregistrer un fichier normalement, soit utiliser « Enregistrer sous » pour sélectionnner différentes options.</para>
      <step performance="required">
      <para lang="en">
      Go to <menuchoice>
              <guisubmenu>File</guisubmenu>
              <guimenuitem>Save</guimenuitem>
            </menuchoice>. </para>
      <para>La fenêtre d'ouverture de fichier devrait apparaître.</para>
      </step>
    </procedure>
    <note><para>Si vous enregistrez un fichier pour la première fois, la fenêtre « Enregistrer sous » apparaît.</para></note>
    <procedure>
      <title>Enregistrement d'un nouveau fichier</title>
      <step performance="required">
      <para lang="en">
      Go to <menuchoice>
              <guisubmenu>File</guisubmenu>
              <guimenuitem>Save As</guimenuitem>
            </menuchoice>. </para>
      <para>La fenêtre « Enregistrer sous » devrait apparaître.</para>
      </step>
      <step performance="required"><para>Saisissez un nouveau nom pour le fichier de sous-titres.</para></step>
      <step performance="optional"><para>Si vous voulez enregistrer le fichier à un emplacement différent, choisissez le nouvel emplacement dans le sélecteur de fichier.</para></step>
      <step performance="optional"><para>Si vous voulez enregistrer le fichier dans un format différent de celui qui estaffiché dans la liste des formats, choisissez un format différent.</para></step>
      <step performance="optional"><para>Si vous voulez enregistrer le fichier dans un codage de caractères différent de l'encodage par défaut, changez le codage dans la liste des codages de caractères.</para></step>
      <step performance="required"><para>Cliquez sur Enregistrer.</para></step>
    </procedure>
  </sect2>
  
    <sect2 id="gs-select-char-coding">
    <title>Sélection d'un codage de caractères</title>
    <para>Si vous utilisez des caractères spéciaux dans votre fichier de sous-titres, vous devrez vous assurer que votre fichier est enregistré dans un codage prenant en charge ces caractères.</para>
    <caution><para>Enregistrer un fichier dans un codage de caractères qui ne prend pas en charge les caractères spéciaux provoquera une perte de données. Cela se produit quand vous essayez d'enregistrer des caractères multi-octets dans un codage de caractères à simple octet.</para></caution>
  </sect2>
    <sect2 id="gs-editing-headers">
    <title>Édition des en-têtes des sous-titres</title>

        <procedure>
  <para>Certains formats de sous-titres ont des en-têtes de fichier contenant des informations sur le fichier. Vous pouvez modifier ces champs dans <application>Gnome Subtitles</application>.</para>
      <step performance="required">
      <para lang="en">
      Go to <menuchoice>
              <guisubmenu>File</guisubmenu>
              <guimenuitem>Headers</guimenuitem>
            </menuchoice>. </para>
      <para>La fenêtre En-têtes s'ouvre. Elle possède quatre onglets.</para>
      </step>
      <step performance="required"> <para>Choisissez l'onglet correspondant au format de votre fichier de sous-titres.</para></step>
      <step performance="required"><para>Remplissez les champs appropriés.</para></step>
      <step performance="required"><para>Quand vous avez fini d'entrer les informations d'en-tête, cliquez sur Valider.</para></step>
    </procedure>
  </sect2>
</sect1>
    <!-- ============= Working with Subtitles ================================== -->
<sect1 id="gs-working-with-subs">
<title>Utilisation des sous-titres</title>
<note><para>Cette aide présente les raccourcis clavier pour accomplir certaines tâches. Toutes les tâches peuvent également être réalisées en utilisant le menu Édition plutôt que les raccourcis clavier.</para></note>
      <sect2 id="gs-wws-adding">
        <title>Ajout d'un sous-titre</title>
        <para>De nouveaux sous-titres sont ajoutés avant ou après le sous-titre actuel.</para>
        <procedure>
          <title>Pour ajouter un nouveau sous-titre après le sous-titre actuel</title>
      <step performance="required">
      <para lang="en">Type <keycap>Insert</keycap> or <keycap>Ctrl</keycap>+<keycap>Enter</keycap></para>
      </step>
    </procedure>
            <procedure>
          <title>Pour ajouter un nouveau sous-titre avant le sous-titre actuel</title>
      <step performance="required">
      <para lang="en">Type <keycap>Shift</keycap>+<keycap>Ctrl</keycap>+<keycap>Enter</keycap></para>
      </step>
    </procedure>
      </sect2>     
      <sect2 id="gs-wws-moving">
        <title>Déplacement entre les sous-titres</title>
        <para>Pour sélectionner des sous-titres, vous pouvez soit utiliser la souris pour cliquer sur le sous-titre, ou, quand le curseur est dans la fenêtre d'édition, vous pouvez utiliser des raccourcis clavier pour vous déplacer entre des sous-titres.</para>
        <procedure>
          <title>Pour aller au sous-titre suivant</title>
      <step performance="required">
      <para lang="en">Type <keycap>Ctrl</keycap>+<keycap>Page Down</keycap></para>
      </step>
    </procedure>
            <procedure>
          <title>Pour aller au sous-titre précédent</title>
      <step performance="required">
      <para lang="en">Type <keycap>Ctrl</keycap>+<keycap>Page Up</keycap></para>
      </step>
    </procedure>
      </sect2>     
      <sect2 id="gs-wws-removing">
        <title>Suppression de sous-titres</title>
      <procedure><step><para>Appuyez sur <keycap>Suppr</keycap>.</para></step></procedure>
      </sect2>      
      <sect2 id="gs-wws-batch">
        <title>Manipulation de plusieurs sous-titres</title>
        <para lang="en">Sometimes you will want to select multiple subtitles.  Use <keycap>Shift</keycap> to
          select sequential subtitles, and <keycap>Ctrl</keycap> to select non-sequential subtitles</para>
        <para lang="en">To select all subtitles type <keycap>Ctrl</keycap>+<keycap>A</keycap></para>
      </sect2>      
      <sect2 id="gs-wws-editing">
        <title>Édition de texte</title>
        <para>Pour éditer le texte d'un sous-titre</para>
          <procedure>
            <step><para>Sélectionnez le sous-titre.</para></step>
            <step><para>Cliquez sur la fenêtre d'édition pour placer le curseur dans celle-ci.</para></step>
          </procedure>
      </sect2>
      <sect2 id="gs-wws-format">
        <title>Formatage du texte</title>
        <note><para>Un type de formatage (gras, italique, souligné) peut seulement être appliqué pour une ligne entière. Actuellement, si vous voulez n'avoir que certains caractères formatés particulièrement, vous devez modifier le fichier de sous-titres dans un éditeur de texte.</para></note>
      </sect2>
      <sect2 id="gs-wws-oops">
        <title>Annulation et rétablissement</title>
        <procedure><title>Pour annuler une action</title><step><para lang="en">Type <keycap>Ctrl</keycap>+
              <keycap>Z</keycap></para></step></procedure>
        <procedure><title>Pour rétablir une action</title><step><para lang="en">Type <keycap>Ctrl</keycap>+
              <keycap>Y</keycap> or <keycap>Ctrl</keycap>+<keycap>Shift</keycap>+<keycap>Z
              </keycap></para></step></procedure>
      </sect2>      
      <sect2 id="gs-wws-ccp">
        <title>Couper, copier et coller</title>
        <para>Vous pouvez couper, copier et coller le texte d'un sous-titre.</para>
        <procedure><title>Pour copier du texte</title><step><para lang="en"><keycap>Ctrl</keycap>+
              <keycap>C</keycap></para></step></procedure>
        <procedure><title>Pour couper du texte</title><step><para lang="en"><keycap>Ctrl</keycap>+
              <keycap>X</keycap></para></step></procedure>
        <procedure><title>Pour coller du texte</title><step><para lang="en"><keycap>Ctrl</keycap>+
              <keycap>V</keycap></para></step></procedure>
      </sect2>
</sect1>
    <!-- ============= find/replace ================================== -->
<!--    <sect1 id="gs-findreplace">
      <title>Finding and Replacing</title>
      <sect2 id="gs-fr-finding">
        <title>Finding</title>
        <para>Questions and general discussions should be posted on the <ulink url="http://sourceforge.net/forum/?group_id=129996" type="http">Forum</ulink> (no registration needed).</para>
      </sect2>
            <sect2 id="gs-fr-replacing">
        <title>Replacing</title>
        <para>You might get a quick response by asking the <ulink url="http://lists.sourceforge.net/lists/listinfo/gnome-subtitles-general" type="http">Mailing List</ulink> (<ulink url="http://sourceforge.net/mailarchive/forum.php?forum=gnome-subtitles-general" type="http">archives</ulink> are available).</para>
      </sect2>
      <sect2 id="gs-fr-options">
        <title>Find and Replace Options</title>
        <para>Questions and general discussions should be posted on the <ulink url="http://sourceforge.net/forum/?group_id=129996" type="http">Forum</ulink> (no registration needed).</para>
      </sect2>
      <sect2 id="gs-regex">
        <title>Using Regular Expressions</title>
        <para>Bugs and Feature Requests can be reported at the official <ulink url="http://bugzilla.gnome.org/browse.cgi?product=gnome-subtitles" type="http">Gnome Bugzilla</ulink>.</para>
      </sect2>
    </sect1>
-->
    <!-- ============= Timings ================================== -->
    <sect1 id="gs-timings">
      <title>Manipulation des sous-titres</title>
      <sect2 id="gs-timings-settingview">
        <title>Choix de l'unité</title>
        <para>Dans <application>Gnome Subtitles</application>, vous pouvez sélectionner etajuster les sous-titres par unité de temps (minutes et secondes) ou par unité d'images. L'unité par défaut est le temps.</para>
            <procedure><title>Pour utiliser les images comme unité</title>
              <step><para lang="en">Go to <menuchoice>
              <guisubmenu>View</guisubmenu>
              <guimenuitem>Frames</guimenuitem>
            </menuchoice>. </para></step></procedure>
              <procedure><title>Pour utiliser les unités de temps</title>
              <step><para lang="en">Go to <menuchoice>
              <guisubmenu>View</guisubmenu>
              <guimenuitem>Times</guimenuitem>
            </menuchoice>. </para></step></procedure>
      </sect2>
      <sect2 id="gs-timings-Adjusting">
        <title>Ajustement de la synchronisation</title>
           <procedure>
             <step><para>Si vous voulez ajuster la synchronisation de certains sous-tires seulement, sélectionnez les sous-titres que vous voulez synchroniser.</para></step>
              <step><para lang="en">Go to <menuchoice>
              <guisubmenu>Timings</guisubmenu>
              <guimenuitem>Adjust</guimenuitem>
            </menuchoice>. </para></step>
            <step><para>Choisissez le nouveau début du premier sous-titre et la nouvelle fin du dernier sous-titre.</para></step>
      </procedure>
      </sect2>
            <sect2 id="gs-timings-Shifitng">
        <title>Décalage des synchronisations</title>
        <para>Le décalage des synchronisations permet de déplacer un ou plusieurs sous-titres d'une certaine valeur, en avant ou en arrière. Contrairement à la fonction d'ajustement des sous-titres, le décalage des sous-titres n'a pas d'incidence sur la durée des sous-titres.</para>
              <procedure><title>Pour décaler les sous-titres</title>
                <step><para>Sélectionnez le ou les sous-titres que vous voulez décaler. Si vous voulez décaler le temps ou l'image d'un ou de tous les sous-tires avant ou après un sous-titre particulier, sélectionnez ce sous-titre. Si vous voulez décaler plus d'un sous-titre particulier, sélectionnez tous les sous-titres que vous voulez décaler.</para></step>
              <step><para lang="en">Go to <menuchoice>
              <guisubmenu>Timings</guisubmenu>
              <guimenuitem>Shift</guimenuitem>
            </menuchoice>. </para></step>
            <step><para>Saisissez la durée (ou le nombre d'images, si vous utilisez cette unité) pour ajuster les sous-titres. Si vous voulez déplacer des sous-titres vers l'arrière, saisissez une valeur négative, sinon saisissez une valeur positive.</para></step>
            <step><para>Sélectionnez la méthode de décalage des sous-titres.</para></step>
            <step><para>Cliquez sur Décaler pour décaler les sous-titres.</para></step>
      </procedure>
      </sect2>
      <sect2 id="gs-timings-inputfr">
        <title>Sélection de la fréquence d'images vidéo et en entrée</title>
        <para>Quand vous ouvez une vidéo, <application>Gnome Subtitles</application> ne définit pas automatiquement la fréquence des images de celle-ci.</para>
        <para><application>Gnome Subtitles</application> manipule les sous-titres en fonction du temps, quel que soit le mode de visualisation. Donc, quand vous modifiez la fréquence des images de la vidéo, <application>Gnome Subtitles</application> ajuste les images pour compenser le changement. La durée des sous-titres reste la même.</para>
        <caution><para>Si le format de sous-titres utilise les images pour déterminer quand un sous-titre doit être affiché (MicroDVD), modifier la fréquence des images de la vidéo peut causer une perte de synchronisation des sous-titres.</para></caution>
        <procedure><title>Pour définir la fréquence des images vidéo</title>
              <step><para lang="en">Go to <menuchoice>
              <guisubmenu>Timings</guisubmenu>
              <guimenuitem>Video Frame Rate</guimenuitem>
            </menuchoice> and select the appropriate framerate.</para></step>
      </procedure>
      <procedure><title>Pour définir la fréquence des images de l'entrée</title>
              <step><para lang="en">Go to <menuchoice>
              <guisubmenu>Timings</guisubmenu>
              <guimenuitem>Input Frame Rate</guimenuitem>
            </menuchoice> and select the appropriate framerate.</para></step>
      </procedure>
      </sect2>
    </sect1>
     <!-- ============= Video ================================== -->
    <sect1 id="gs-video">
      <title>Utilisation des vidéos</title>
      <sect2 id="gs-video-opening">
        <title>Ouverture et fermeture d'une vidéo</title>
        <procedure>
          <title>Pour ouvrir une vidéo</title>
          <step><para lang="en">Go to <menuchoice>
              <guisubmenu>Video</guisubmenu>
              <guimenuitem>Open</guimenuitem>
            </menuchoice>
          </para></step>
        </procedure>
        <procedure>
          <title>Pour fermer une vidéo</title>
          <step><para lang="en">Go to <menuchoice>
              <guisubmenu>Video</guisubmenu>
              <guimenuitem>Close</guimenuitem>
            </menuchoice>
          </para></step>
        </procedure>
      </sect2>
      <sect2 id="gs-video-playback">
        <title>Lecture</title>
        <para><application>Gnome Subtitles</application> lit la vidéo dans la fenêtre de visualisation avec vos sous-titres.</para>
      </sect2>
      <sect2 id="gs-video-seeking">
        <title>Positionnement de la sélection</title>
        <para>Vous pouvez aller au point de la vidéo où un sous-titre commence.</para>
        <procedure>
          <title>Pour aller à un sous-titre</title>
          <step><para lang="en">Go to <menuchoice>
              <guisubmenu>Video</guisubmenu>
              <guimenuitem>Seek to Selection</guimenuitem>
            </menuchoice>
          </para></step>
        </procedure>
      </sect2>
      <sect2 id="gs-video-settingsubs">
        <title>Définition du début et de la fin d'un sous-titre</title>
        <para>Vous pouvez définir le début ou la fin d'un sous-titre en fonction du moment où la vidéo est mise en pause.</para>
        <procedure>
          <title>Pour définir le début d'un sous-titre</title>
          <step><para lang="en">Go to <menuchoice>
              <guisubmenu>Video</guisubmenu>
              <guimenuitem>Set Subtitle Start</guimenuitem>
            </menuchoice>
          </para></step>
        </procedure>
        <procedure>
          <title>Pour définir la fin d'un sous-titre</title>
          <step><para lang="en">Go to <menuchoice>
              <guisubmenu>Video</guisubmenu>
              <guimenuitem>Set Subtitle End</guimenuitem>
            </menuchoice>
          </para></step>
        </procedure>
      </sect2>
    </sect1>
   <!-- ============= Support ================================== -->
    <sect1 id="gs-support">
      <title>Recherche d'aide supplémentaire</title>
      <sect2 id="gs-support-forums">
        <title>Forums</title>
        <para>Les questions et les discussions générales devraient être soumises dans le <ulink url="http://sourceforge.net/forum/?group_id=129996" type="http">forum</ulink> (pas d'inscription requise).</para>
      </sect2>
            <sect2 id="gs-support-mailinglist">
        <title>Liste de diffusion</title>
        <para>Il est possible d'obtenir une réponse rapide en interrogeant la <ulink url="http://lists.sourceforge.net/lists/listinfo/gnome-subtitles-general" type="http">liste de diffusion</ulink> (des <ulink url="http://sourceforge.net/mailarchive/forum.php?forum_name=gnome-subtitles-general" type="http">archives</ulink> sont disponibles). En anglais uniquement.</para>
      </sect2>
      <sect2 id="gs-support-bugzilla">
        <title>Anomalies et fonctionnalités</title>
        <para lang="en">Bugs and Feature Requests can be reported in the <ulink url="https://gitlab.gnome.org/GNOME/gnome-subtitles/issues/" type="http">GNOME issue tracker</ulink>.</para>
      </sect2>
    </sect1>
  </article>
