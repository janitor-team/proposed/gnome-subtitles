<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE article PUBLIC "-//OASIS//DTD DocBook XML V4.1.2//EN" "http://www.oasis-open.org/docbook/xml/4.1.2/docbookx.dtd" [
<!ENTITY legal SYSTEM "legal.xml">
<!ENTITY appversion "0.4">
<!ENTITY manrevision "1.0">
<!ENTITY date "May 2007">
<!ENTITY app "<application>Gnome Subtitles</application>">
]>
<!--
    (Do not remove this comment block.)
    Maintained by the GNOME Documentation Project
    http://developer.gnome.org/projects/gdp
    Template version: 2.0 beta
    Template last modified Apr 11, 2002
    -->
<!-- =============Document Header ============================= -->
<article id="index" lang="pt-BR">
  <!-- please do not change the id; for translations, change lang to -->
  <!-- appropriate code -->
  <articleinfo>
    <title>Manual do <application>Gnome Subtitles</application> v1.0</title>
    
    <copyright lang="en">
      <year>2007</year>
      <holder>Erin Bloom</holder>
    </copyright>
    <abstract role="description"><para lang="en">The workings of <application>Gnome Subtitles</application></para>
    </abstract>
    <!-- An address can be added to the publisher information.  If a role is
          not specified, the publisher/author is the same for all versions of the
          document.  -->
    <publisher>
      <publishername>Projeto de documentação do GNOME</publishername>
    </publisher>
    
      <legalnotice id="legalnotice">
	<para>Permissão concedida para copiar, distribuir e/ou modificar este documento sob os termos da Licença de Documentação Livre GNU (GNU Free Documentation License), Versão 1.1 ou qualquer versão mais recente publicada pela Free Software Foundation; sem Seções Invariantes, Textos de Capa Frontal, e sem Textos de Contracapa. Você pode encontrar uma cópia da licença GFDL neste <ulink type="help" url="ghelp:fdl">link</ulink> ou no arquivo COPYING-DOCS distribuído com este manual.</para>
         <para>Este manual é parte da coleção de manuais do GNOME distribuídos sob a GFDL. Se você quiser distribuí-lo separadamente da coleção, você pode fazê-lo adicionando ao manual uma cópia da licença, como descrito na seção 6 da licença.</para>

	<para>Muitos dos nomes usados por empresas para distinguir seus produtos e serviços são reivindicados como marcas registradas. Onde esses nomes aparecem em qualquer documentação do GNOME e os membros do Projeto de Documentação do GNOME estiverem cientes dessas marcas registradas, os nomes aparecerão impressos em letras maiúsculas ou com iniciais em maiúsculas.</para>

	<para lang="en">
	  DOCUMENT AND MODIFIED VERSIONS OF THE DOCUMENT ARE PROVIDED
	  UNDER  THE TERMS OF THE GNU FREE DOCUMENTATION LICENSE
	  WITH THE FURTHER UNDERSTANDING THAT:

	  <orderedlist>
		<listitem>
		  <para lang="en">DOCUMENT IS PROVIDED ON AN "AS IS" BASIS,
                    WITHOUT WARRANTY OF ANY KIND, EITHER EXPRESSED OR
                    IMPLIED, INCLUDING, WITHOUT LIMITATION, WARRANTIES
                    THAT THE DOCUMENT OR MODIFIED VERSION OF THE
                    DOCUMENT IS FREE OF DEFECTS MERCHANTABLE, FIT FOR
                    A PARTICULAR PURPOSE OR NON-INFRINGING. THE ENTIRE
                    RISK AS TO THE QUALITY, ACCURACY, AND PERFORMANCE
                    OF THE DOCUMENT OR MODIFIED VERSION OF THE
                    DOCUMENT IS WITH YOU. SHOULD ANY DOCUMENT OR
                    MODIFIED VERSION PROVE DEFECTIVE IN ANY RESPECT,
                    YOU (NOT THE INITIAL WRITER, AUTHOR OR ANY
                    CONTRIBUTOR) ASSUME THE COST OF ANY NECESSARY
                    SERVICING, REPAIR OR CORRECTION. THIS DISCLAIMER
                    OF WARRANTY CONSTITUTES AN ESSENTIAL PART OF THIS
                    LICENSE. NO USE OF ANY DOCUMENT OR MODIFIED
                    VERSION OF THE DOCUMENT IS AUTHORIZED HEREUNDER
                    EXCEPT UNDER THIS DISCLAIMER; AND
		  </para>
		</listitem>
		<listitem>
		  <para lang="en">UNDER NO CIRCUMSTANCES AND UNDER NO LEGAL
                       THEORY, WHETHER IN TORT (INCLUDING NEGLIGENCE),
                       CONTRACT, OR OTHERWISE, SHALL THE AUTHOR,
                       INITIAL WRITER, ANY CONTRIBUTOR, OR ANY
                       DISTRIBUTOR OF THE DOCUMENT OR MODIFIED VERSION
                       OF THE DOCUMENT, OR ANY SUPPLIER OF ANY OF SUCH
                       PARTIES, BE LIABLE TO ANY PERSON FOR ANY
                       DIRECT, INDIRECT, SPECIAL, INCIDENTAL, OR
                       CONSEQUENTIAL DAMAGES OF ANY CHARACTER
                       INCLUDING, WITHOUT LIMITATION, DAMAGES FOR LOSS
                       OF GOODWILL, WORK STOPPAGE, COMPUTER FAILURE OR
                       MALFUNCTION, OR ANY AND ALL OTHER DAMAGES OR
                       LOSSES ARISING OUT OF OR RELATING TO USE OF THE
                       DOCUMENT AND MODIFIED VERSIONS OF THE DOCUMENT,
                       EVEN IF SUCH PARTY SHALL HAVE BEEN INFORMED OF
                       THE POSSIBILITY OF SUCH DAMAGES.
		  </para>
		</listitem>
	  </orderedlist>
	</para>
  </legalnotice>


    
    <authorgroup>
      <author role="maintainer" lang="en">
        <firstname>Erin</firstname>
        <surname>Bloom</surname>
        <affiliation>
          <orgname>GNOME Documentation Project</orgname>
          <address> <email>doc-writer2@gnome.org</email> </address>
        </affiliation>
      </author>

<!-- This is appropriate place for other contributors: translators,
      maintainers,  etc. Commented out by default.
      
      <othercredit role="translator">
        <firstname>Latin</firstname>
        <surname>Translator 1</surname>
        <affiliation>
          <orgname>Latin Translation Team</orgname>
          <address> <email>translator@gnome.org</email> </address>
        </affiliation>
        <contrib>Latin translation</contrib>
      </othercredit>
      -->
    </authorgroup>
    
  
  <!-- According to GNU FDL, revision history is mandatory if you are -->
  <!-- modifying/reusing someone else's document.  If not, you can omit it. -->
  <!-- Remember to remove the &manrevision; entity from the revision entries other -->
  <!-- than the current revision. -->
  <!-- The revision numbering system for GNOME manuals is as follows: -->
  <!-- * the revision number consists of two components -->
  <!-- * the first component of the revision number reflects the release version of the GNOME desktop. -->
  <!-- * the second component of the revision number is a decimal unit that is incremented with each revision of the manual. -->
  <!-- For example, if the GNOME desktop release is V2.x, the first version of the manual that -->
  <!-- is written in that desktop timeframe is V2.0, the second version of the manual is V2.1, etc. -->
  <!-- When the desktop release version changes to V3.x, the revision number of the manual changes -->
  <!-- to V3.0, and so on. -->
  <revhistory>
    <revision lang="en">
      <revnumber>gnome-subtitles Manual V1.0</revnumber>
      <date>May 2007</date>
      <revdescription>
        <para role="author" lang="en">Erin Bloom
          <email>docwriter2@gnome.org</email>
        </para>
        <para role="publisher" lang="en">GNOME Documentation Project</para>
      </revdescription>
    </revision>
  </revhistory>
  
  <releaseinfo>Este manual descreve a versão 0.4 do gnome-subtitles</releaseinfo>
  <legalnotice>
    <title>Comentários</title>
    <para lang="en">To report a bug or make a suggestion regarding the <application>Gnome Subtitles</application> application or
      this manual, follow the directions in the <ulink url="help:gnome-feedback" type="help">GNOME Feedback Page</ulink>.
    </para>
    <!-- Translators may also add here feedback address for translations -->
  </legalnotice>
</articleinfo>
<!-- Index Section -->
<indexterm zone="index" lang="en">
  <primary>Gnome Subtitles</primary>
</indexterm>
<indexterm zone="index" lang="en">
  <primary>gnomesubtitles</primary>
</indexterm>

<!-- ============= Document Body ============================= -->
<!-- ============= Introduction ============================== -->
<!-- Use the Introduction section to give a brief overview of what
    the application is and what it does. -->
<sect1 id="gs-introduction">
  <title>Introdução</title>
  <para>O <application>Gnome Subtitles</application> é um editor de legendas para a área de trabalho GNOME. O <application>Gnome Subtitles</application> suporta os formatos mais comuns de legenda e permite edição, conversão e sincronização de legendas. Conheça suas características a seguir.</para>
  <itemizedlist>
    <listitem>
      <para>Formatos de legendas:</para> 
      <itemizedlist> 
        <listitem><para>Advanced Sub Station Alpha</para></listitem> 
        <listitem><para>MicroDVD</para></listitem> 
        <listitem><para>MPlayer</para></listitem> 
        <listitem><para>Mplayer 2</para></listitem> 
        <listitem><para>MPSub</para></listitem> 
        <listitem><para>SubRip</para></listitem> 
        <listitem><para>Sub Station Alpha</para></listitem> 
        <listitem><para>SubViewer 1.0</para></listitem> 
        <listitem><para>SubViewer 2.0</para></listitem> 
      </itemizedlist> 
    </listitem> 
    <listitem><para>Visualização de vídeo</para> 
      <itemizedlist>
        <listitem><para>Usa mplayer como retaguarda (backend)</para></listitem>
        <listitem><para>Exibição de comprimento e posição atual de vídeo</para></listitem>
        <listitem><para lang="en">Set subtitle timings based on video position</para></listitem>
        <listitem><para lang="en">Automatically select video when opening subtitles</para></listitem>
      </itemizedlist>
    </listitem> 
    <listitem>
      <para>Interface gráfica</para>
      <itemizedlist>
        <listitem><para>WYSIWYG-O que você vê é o que você tem</para></listitem>
        <listitem><para lang="en">Edit subtitle headers</para></listitem>
        <listitem><para>Procurar e substituir, incluindo suporte a expressões regulares</para></listitem>
        <listitem><para>Desfazer/refazer</para></listitem>
      </itemizedlist>
    </listitem>
    <listitem>
      <para lang="en">Timing Operations</para>
      <itemizedlist>
        <listitem><para lang="en">Auto-adjust timings based on 2 correct time/synchronization points</para></listitem>
        <listitem><para lang="en">Shift subtitles by a specified delay (which can be based on the video)</para></listitem>	
        <listitem><para lang="en">Convert between framerates</para></listitem>
        <listitem><para lang="en">Edit times and frames</para></listitem>
      </itemizedlist>
    </listitem>
    <listitem><para>Outras características</para>
      <itemizedlist>
        <listitem><para lang="en">Character coding a subtitle format auto-detection (on opening)</para></listitem>
        <listitem><para lang="en">Multiple character encoding choice</para></listitem>
        <listitem><para lang="en">Relaxes subtitle reading, to read subtitles that contain errors</para></listitem>
      </itemizedlist>
    </listitem>
  </itemizedlist>
</sect1>

<!-- =========== Getting Started ============================== -->
<!-- Use the Getting Started section to describe the steps required
    to start the application and to describe the user interface components
  of the application. If there is other information that it is important
    for readers to know before they start using the application, you should
    also include this information here. 
    If the information about how to get started is very short, you can 
    include it in the Introduction and omit this section. -->

<sect1 id="gs-getting-started">
  <title>Primeiros passos</title> 
  
  <sect2 id="gs-start">
    <title>Iniciando o <application>Gnome Subtitles</application></title>
    <para>Você pode iniciar o <application><application>Gnome Subtitles</application></application> das seguintes maneiras:</para>
    <variablelist>
      <varlistentry>
        <term>Menu <guimenu>aplicativos</guimenu></term>
        <listitem>
          <para lang="en">Choose
            <menuchoice>
              <guisubmenu>Sound &amp; Video</guisubmenu>
              <guimenuitem>Gnome Subtitles</guimenuitem>
            </menuchoice>. </para>
        </listitem>
      </varlistentry>
      <varlistentry>
        <term>Linha de comando</term>
        <listitem>
          <para lang="en">Type <command>gnome-subtitles</command> <replaceable>filename</replaceable> and press <keycap>Enter</keycap>.</para>
          <para lang="en">The <replaceable>filename</replaceable> argument is optional. If specified, the application will open that file when starting.</para>
        </listitem>
      </varlistentry>
    </variablelist>
  </sect2>
</sect1>
<sect1 id="gs-working-with-files">
  <title>Trabalhando com arquivos</title>
  <sect2 id="gs-creating-new-doc">
    <title>Criando um novo documento de legenda</title>
    <procedure>
      <title>Criando um novo documento de legenda</title>
      <step performance="required">
      <para lang="en">
      Go to <menuchoice>
              <guisubmenu>File</guisubmenu>
              <guimenuitem>New</guimenuitem>
            </menuchoice>. </para>
      <para>Um novo arquivo deve abrir no editor</para>
      </step>
      </procedure>
  </sect2>
    <sect2 id="gs-opendingfile">
    <title>Abrindo um arquivo</title>
      <procedure>
      <title>Abrindo um arquivo</title>
      <step performance="required">
      <para lang="en">
      Go to <menuchoice>
              <guisubmenu>File</guisubmenu>
              <guimenuitem>Open</guimenuitem>
            </menuchoice>. </para>
      <para>A janela de abrir arquivo deve aparecer</para>
      </step>
      <step performance="required">
      <para lang="en">Select the subtitle file you wish to open.
      </para>
      <substeps>
      <step performance="optional">
      <para lang="en">If you want to specify the file's character coding, choose one from the Character Coding list. If not specified, the caracter coding will be auto detected.</para>
      </step>
      <step performance="optional">
      <para lang="en">If you want to choose a video to open immediately, choose a video from the Video list.</para>
      </step>
      </substeps>
      </step>
      </procedure>
      <note><para lang="en">When selecting a video, you can only choose from the videos in the current directory.  
        If the video you want to open is not in the same directory as the subtitle file, 
        you can open the video after opening the subtitle file.</para>
      </note>
  </sect2>
    <sect2 id="gs-saving-file">
    <title>Salvando um arquivo</title>
      <procedure>
      <title>Salvando um arquivo</title>
      <para lang="en">You can either save a file normally, or use Save As to set different options.</para>
      <step performance="required">
      <para lang="en">
      Go to <menuchoice>
              <guisubmenu>File</guisubmenu>
              <guimenuitem>Save</guimenuitem>
            </menuchoice>. </para>
      <para>A janela de abrir arquivo deve aparecer</para>
      </step>
    </procedure>
    <note><para lang="en">If you are saving a new file, the Save As window will appear</para></note>
    <procedure>
      <title>Salvando como um novo arquivo</title>
      <step performance="required">
      <para lang="en">
      Go to <menuchoice>
              <guisubmenu>File</guisubmenu>
              <guimenuitem>Save As</guimenuitem>
            </menuchoice>. </para>
      <para lang="en">The Save As window should open</para>
      </step>
      <step performance="required"><para lang="en">Enter a new name for your subtitle file</para></step>
      <step performance="optional"><para lang="en">If you would like to save the file in a different location, browse to the new location in the
          file browser</para></step>
      <step performance="optional"><para lang="en">If you would like to save the file in a format other than the format
          listed in the Subtitle Format list, choose a different format.</para></step>
      <step performance="optional"><para lang="en">If you would like to save the file in a character encoding other than
          your default encoding which is listed, change the encoding in the character encoding list.</para></step>
      <step performance="required"><para lang="en">Click Save</para></step>
    </procedure>
  </sect2>
  
    <sect2 id="gs-select-char-coding">
    <title lang="en">Selecting a character encoding</title>
    <para lang="en">If you use special characters in your subtitle file, you will want to make sure that your
      file is saved in an encoding that supports those characters.</para>
    <caution><para lang="en">Saving a file in a character encoding that does not support the special characters
        will cause loss of character data.  This occurs when you try to save multi-byte characters in a
        single byte character encoding.</para></caution>
  </sect2>
    <sect2 id="gs-editing-headers">
    <title lang="en">Editing subtitle headers</title>

        <procedure>
  <para lang="en">Some subtitle formats have file headers that contain information about the file.
    You can edit these fields in <application>Gnome Subtitles</application> by</para>
      <step performance="required">
      <para lang="en">
      Go to <menuchoice>
              <guisubmenu>File</guisubmenu>
              <guimenuitem>Headers</guimenuitem>
            </menuchoice>. </para>
      <para lang="en">The Headers window will open.  It has 4 tabs on it.</para>
      </step>
      <step performance="required"> <para lang="en">Select the tab that corresponds to the format of your subtitle file</para></step>
      <step performance="required"><para lang="en">Fill in appropriate fields.</para></step>
      <step performance="required"><para lang="en">When you are done entering header data in, click OK</para></step>
    </procedure>
  </sect2>
</sect1>
    <!-- ============= Working with Subtitles ================================== -->
<sect1 id="gs-working-with-subs">
<title lang="en">Working with Subtitles</title>
<note><para lang="en">This help lists the keyboard shortcuts to accomplish tasks.  All of the tasks can also
  be accomplished by using the Edit menu instead of keyboard shortcuts.</para></note>
      <sect2 id="gs-wws-adding">
        <title>Adicionando uma legenda</title>
        <para lang="en">New subtitles are added either before or after the current subtitle.</para>
        <procedure>
          <title lang="en">To add a new subtitle after the current subtitle</title>
      <step performance="required">
      <para lang="en">Type <keycap>Insert</keycap> or <keycap>Ctrl</keycap>+<keycap>Enter</keycap></para>
      </step>
    </procedure>
            <procedure>
          <title lang="en">To add a new subtitle before the current subtitle</title>
      <step performance="required">
      <para lang="en">Type <keycap>Shift</keycap>+<keycap>Ctrl</keycap>+<keycap>Enter</keycap></para>
      </step>
    </procedure>
      </sect2>     
      <sect2 id="gs-wws-moving">
        <title lang="en">Moving between subtitles</title>
        <para lang="en">To select subtitles you can either use your mouse to click the subtitle
        , or, when your cursor is in the edit window, you can use keyboard shortcuts 
        to move between subtitles</para>
        <procedure>
          <title lang="en">To go to the next subtitle</title>
      <step performance="required">
      <para lang="en">Type <keycap>Ctrl</keycap>+<keycap>Page Down</keycap></para>
      </step>
    </procedure>
            <procedure>
          <title lang="en">To move to the previous subtitle</title>
      <step performance="required">
      <para lang="en">Type <keycap>Ctrl</keycap>+<keycap>Page Up</keycap></para>
      </step>
    </procedure>
      </sect2>     
      <sect2 id="gs-wws-removing">
        <title lang="en">Removing Subtitle(s)</title>
      <procedure><step><para lang="en">Type <keycap>Delete</keycap></para></step></procedure>
      </sect2>      
      <sect2 id="gs-wws-batch">
        <title lang="en">Working with Multiple Subtitles</title>
        <para lang="en">Sometimes you will want to select multiple subtitles.  Use <keycap>Shift</keycap> to
          select sequential subtitles, and <keycap>Ctrl</keycap> to select non-sequential subtitles</para>
        <para lang="en">To select all subtitles type <keycap>Ctrl</keycap>+<keycap>A</keycap></para>
      </sect2>      
      <sect2 id="gs-wws-editing">
        <title>Editando texto</title>
        <para lang="en">To edit text in a subtitle</para>
          <procedure>
            <step><para>Selecione a legenda</para></step>
            <step><para lang="en">Click the edit window to move the cursor into the edit window</para></step>
          </procedure>
      </sect2>
      <sect2 id="gs-wws-format">
        <title>Formato do texto</title>
        <note><para lang="en">A type format (bold, italics, underline) can be applied to the whole line only.
          Currently, if you want to have only certain characters formatted specially, you will need
        to edit the subtitle file in a text editor.</para></note>
      </sect2>
      <sect2 id="gs-wws-oops">
        <title>Desfazendo e refazendo</title>
        <procedure><title>Para desfazer uma ação</title><step><para lang="en">Type <keycap>Ctrl</keycap>+
              <keycap>Z</keycap></para></step></procedure>
        <procedure><title>Para refazer uma ação</title><step><para lang="en">Type <keycap>Ctrl</keycap>+
              <keycap>Y</keycap> or <keycap>Ctrl</keycap>+<keycap>Shift</keycap>+<keycap>Z
              </keycap></para></step></procedure>
      </sect2>      
      <sect2 id="gs-wws-ccp">
        <title>Recortando, copiando e colando</title>
        <para>Você pode recortar, copiar e colar o texto em uma legenda.</para>
        <procedure><title lang="en">To copy text</title><step><para lang="en"><keycap>Ctrl</keycap>+
              <keycap>C</keycap></para></step></procedure>
        <procedure><title lang="en">To cut text</title><step><para lang="en"><keycap>Ctrl</keycap>+
              <keycap>X</keycap></para></step></procedure>
        <procedure><title lang="en">To paste text</title><step><para lang="en"><keycap>Ctrl</keycap>+
              <keycap>V</keycap></para></step></procedure>
      </sect2>
</sect1>
    <!-- ============= find/replace ================================== -->
<!--    <sect1 id="gs-findreplace">
      <title>Finding and Replacing</title>
      <sect2 id="gs-fr-finding">
        <title>Finding</title>
        <para>Questions and general discussions should be posted on the <ulink url="http://sourceforge.net/forum/?group_id=129996" type="http">Forum</ulink> (no registration needed).</para>
      </sect2>
            <sect2 id="gs-fr-replacing">
        <title>Replacing</title>
        <para>You might get a quick response by asking the <ulink url="http://lists.sourceforge.net/lists/listinfo/gnome-subtitles-general" type="http">Mailing List</ulink> (<ulink url="http://sourceforge.net/mailarchive/forum.php?forum=gnome-subtitles-general" type="http">archives</ulink> are available).</para>
      </sect2>
      <sect2 id="gs-fr-options">
        <title>Find and Replace Options</title>
        <para>Questions and general discussions should be posted on the <ulink url="http://sourceforge.net/forum/?group_id=129996" type="http">Forum</ulink> (no registration needed).</para>
      </sect2>
      <sect2 id="gs-regex">
        <title>Using Regular Expressions</title>
        <para>Bugs and Feature Requests can be reported at the official <ulink url="http://bugzilla.gnome.org/browse.cgi?product=gnome-subtitles" type="http">Gnome Bugzilla</ulink>.</para>
      </sect2>
    </sect1>
-->
    <!-- ============= Timings ================================== -->
    <sect1 id="gs-timings">
      <title lang="en">Manipulating Subtitles</title>
      <sect2 id="gs-timings-settingview">
        <title lang="en">Setting subtitle units</title>
        <para lang="en">In <application>Gnome Subtitles</application> you can set and adjust subtitles in time units (minutes and seconds) or in frame units.
              The default unit is time.</para>
            <procedure><title lang="en">To use Frame units</title>
              <step><para lang="en">Go to <menuchoice>
              <guisubmenu>View</guisubmenu>
              <guimenuitem>Frames</guimenuitem>
            </menuchoice>. </para></step></procedure>
              <procedure><title lang="en">To use Time units</title>
              <step><para lang="en">Go to <menuchoice>
              <guisubmenu>View</guisubmenu>
              <guimenuitem>Times</guimenuitem>
            </menuchoice>. </para></step></procedure>
      </sect2>
      <sect2 id="gs-timings-Adjusting">
        <title lang="en">Adjusting Timings</title>
           <procedure>
             <step><para lang="en">If you want to adjust the timings of only some subtitles, select the subtitles
                 that you want to adjust the timing of</para></step>
              <step><para lang="en">Go to <menuchoice>
              <guisubmenu>Timings</guisubmenu>
              <guimenuitem>Adjust</guimenuitem>
            </menuchoice>. </para></step>
            <step><para lang="en">Choose the new starting time of the first subtitle and the new starting time of the last subtitle
            </para></step>
      </procedure>
      </sect2>
            <sect2 id="gs-timings-Shifitng">
        <title lang="en">Shifting Timings</title>
        <para lang="en">Shifting timings allows you to moves one or more subtitles ahead or behind by a 
              specific amount.  Unlike the Adjust subtitles feature, Shifting subtitles does not
              affect the duration of the subtitles.</para>
              <procedure><title lang="en">To use shift subtitles</title>
                <step><para lang="en">Select which subtitle(s) you want to shift.  If you want to shift the time/frame of one
                  subtitle and/or all subtitles before or after a specific subtitle, select that subtitle.
                  If you want to shift more than one specific subtitles, select all of the subtitles that you want to shift.</para></step>
              <step><para lang="en">Go to <menuchoice>
              <guisubmenu>Timings</guisubmenu>
              <guimenuitem>Shift</guimenuitem>
            </menuchoice>. </para></step>
            <step><para lang="en">Enter in the amount of time (or frames, if you are using the frame unit) to 
                        adjust your subtitles by.  If you want to move the subtitles back, enter a negative value,
                        otherwise, enter a positive value.</para></step>
            <step><para lang="en">Select how you want to shift your subtitles.</para></step>
            <step><para lang="en">Click Shift to shift the subtitles.</para></step>
      </procedure>
      </sect2>
      <sect2 id="gs-timings-inputfr">
        <title lang="en">Setting the Input and Video Framerates</title>
        <para lang="en">When you open a video, <application>Gnome Subtitles</application> does not automatically set the framerate of the video.</para>
        <para lang="en"><application>Gnome Subtitles</application> manipulates subtitles based on time, regardless of view mode.  Therefore, when you 
          change the video frame rate, <application>Gnome Subtitles</application> adjusts the frames to compensate for the change.  The duration 
          of the subtitle remains the same.</para>
        <caution><para lang="en">If your subtitle format uses frames to determine when a subtitle is dispayed (MicroDVD), 
          changing the video framerate may cause your subtitles to lose their sync.</para></caution>
        <procedure><title lang="en">To set the video framerate</title>
              <step><para lang="en">Go to <menuchoice>
              <guisubmenu>Timings</guisubmenu>
              <guimenuitem>Video Frame Rate</guimenuitem>
            </menuchoice> and select the appropriate framerate.</para></step>
      </procedure>
      <procedure><title lang="en">To set the input framerate</title>
              <step><para lang="en">Go to <menuchoice>
              <guisubmenu>Timings</guisubmenu>
              <guimenuitem>Input Frame Rate</guimenuitem>
            </menuchoice> and select the appropriate framerate.</para></step>
      </procedure>
      </sect2>
    </sect1>
     <!-- ============= Video ================================== -->
    <sect1 id="gs-video">
      <title lang="en">Working with Videos</title>
      <sect2 id="gs-video-opening">
        <title lang="en">Opening and Closing a Video</title>
        <procedure>
          <title lang="en">To open a video</title>
          <step><para lang="en">Go to <menuchoice>
              <guisubmenu>Video</guisubmenu>
              <guimenuitem>Open</guimenuitem>
            </menuchoice>
          </para></step>
        </procedure>
        <procedure>
          <title lang="en">To close a video</title>
          <step><para lang="en">Go to <menuchoice>
              <guisubmenu>Video</guisubmenu>
              <guimenuitem>Close</guimenuitem>
            </menuchoice>
          </para></step>
        </procedure>
      </sect2>
      <sect2 id="gs-video-playback">
        <title lang="en">Playing back</title>
        <para lang="en"><application>Gnome Subtitles</application> will play the video in the view window with your subtitles.</para>
      </sect2>
      <sect2 id="gs-video-seeking">
        <title lang="en">Seeking to the Selection</title>
        <para lang="en">You can go to the point in the loaded video where a subtitle starts</para>
        <procedure>
          <title lang="en">To seek to a subtitle</title>
          <step><para lang="en">Go to <menuchoice>
              <guisubmenu>Video</guisubmenu>
              <guimenuitem>Seek to Selection</guimenuitem>
            </menuchoice>
          </para></step>
        </procedure>
      </sect2>
      <sect2 id="gs-video-settingsubs">
        <title lang="en">Setting the Subtitle Start or End Positions</title>
        <para lang="en">You can set the subtitles start or end time based on the point at which the loaded video is paused</para>
        <procedure>
          <title lang="en">To set subtitle starting time</title>
          <step><para lang="en">Go to <menuchoice>
              <guisubmenu>Video</guisubmenu>
              <guimenuitem>Set Subtitle Start</guimenuitem>
            </menuchoice>
          </para></step>
        </procedure>
        <procedure>
          <title lang="en">To set subtitle ending time</title>
          <step><para lang="en">Go to <menuchoice>
              <guisubmenu>Video</guisubmenu>
              <guimenuitem>Set Subtitle End</guimenuitem>
            </menuchoice>
          </para></step>
        </procedure>
      </sect2>
    </sect1>
   <!-- ============= Support ================================== -->
    <sect1 id="gs-support">
      <title lang="en">Where to Get Additional Support</title>
      <sect2 id="gs-support-forums">
        <title lang="en">Forums</title>
        <para lang="en">Questions and general discussions should be posted on the <ulink url="http://sourceforge.net/forum/?group_id=129996" type="http">Forum</ulink> (no registration needed).</para>
      </sect2>
            <sect2 id="gs-support-mailinglist">
        <title lang="en">Mailing List</title>
        <para lang="en">You might get a quick response by asking the <ulink url="http://lists.sourceforge.net/lists/listinfo/gnome-subtitles-general" type="http">Mailing List</ulink> (<ulink url="http://sourceforge.net/mailarchive/forum.php?forum_name=gnome-subtitles-general" type="http">archives</ulink> are available).</para>
      </sect2>
      <sect2 id="gs-support-bugzilla">
        <title lang="en">Bugs and Features</title>
        <para lang="en">Bugs and Feature Requests can be reported in the <ulink url="https://gitlab.gnome.org/GNOME/gnome-subtitles/issues/" type="http">GNOME issue tracker</ulink>.</para>
      </sect2>
    </sect1>
  </article>
