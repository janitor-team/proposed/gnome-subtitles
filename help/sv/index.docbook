<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE article PUBLIC "-//OASIS//DTD DocBook XML V4.1.2//EN" "http://www.oasis-open.org/docbook/xml/4.1.2/docbookx.dtd" [
<!ENTITY legal SYSTEM "legal.xml">
<!ENTITY appversion "0.4">
<!ENTITY manrevision "1.0">
<!ENTITY date "May 2007">
<!ENTITY app "<application>Gnome Subtitles</application>">
]>
<!--
    (Do not remove this comment block.)
    Maintained by the GNOME Documentation Project
    http://developer.gnome.org/projects/gdp
    Template version: 2.0 beta
    Template last modified Apr 11, 2002
    -->
<!-- =============Document Header ============================= -->
<article id="index" lang="sv">
  <!-- please do not change the id; for translations, change lang to -->
  <!-- appropriate code -->
  <articleinfo>
    <title>Handbok för <application>Gnome Undertexter</application> v1.0</title>
    
    <copyright><year>2007</year> <holder>Erin Bloom</holder></copyright>
    <abstract role="description"><para>Hur <application>Gnome Undertexter</application> fungerar</para>
    </abstract>
    <!-- An address can be added to the publisher information.  If a role is
          not specified, the publisher/author is the same for all versions of the
          document.  -->
    <publisher>
      <publishername>Dokumentationsprojekt för GNOME</publishername>
    </publisher>
    
      <legalnotice id="legalnotice">
	<para>Tillstånd att kopiera, distribuera och/eller modifiera detta dokument ges under villkoren i GNU Free Documentation License (GFDL), version 1.1 eller senare, utgivet av Free Software Foundation utan standardavsnitt och omslagstexter.  En kopia av GFDL finns att hämta på denna <ulink type="help" url="ghelp:fdl">länk</ulink> eller i filen COPYING-DOCS som medföljer denna handbok.</para>
         <para>Denna handbok utgör en av flera GNOME-handböcker som distribueras under villkoren i GFDL. Om du vill distribuera denna handbok separat från övriga handböcker kan du göra detta genom att lägga till en kopia av licensavtalet i handboken enligt instruktionerna i avsnitt 6 i licensavtalet.</para>

	<para>Flera namn på produkter och tjänster är registrerade varumärken. I de fall dessa namn förekommer i GNOME-dokumentation - och medlemmarna i GNOME-dokumentationsprojektet är medvetna om dessa varumärken - är de skrivna med versaler eller med inledande versal.</para>

	<para>DOKUMENTET OCH MODIFIERADE VERSIONER AV DOKUMENTET TILLHANDAHÅLLS UNDER VILLKOREN I GNU FREE DOCUMENTATION LICENSE ENDAST UNDER FÖLJANDE FÖRUTSÄTTNINGAR: <orderedlist>
		<listitem>
		  <para>DOKUMENTET TILLHANDAHÅLLS I "BEFINTLIGT SKICK" UTAN NÅGRA SOM HELST GARANTIER, VARE SIG UTTRYCKLIGA ELLER UNDERFÖRSTÅDDA, INKLUSIVE, MEN INTE BEGRÄNSAT TILL, GARANTIER ATT DOKUMENTET ELLER EN MODIFIERAD VERSION AV DOKUMENTET INTE INNEHÅLLER NÅGRA FELAKTIGHETER, ÄR LÄMPLIGT FÖR ETT VISST ÄNDAMÅL ELLER INTE STRIDER MOT LAG. HELA RISKEN VAD GÄLLER KVALITET, EXAKTHET OCH UTFÖRANDE AV DOKUMENTET OCH MODIFIERADE VERSIONER AV DOKUMENTET LIGGER HELT OCH HÅLLET PÅ ANVÄNDAREN. OM ETT DOKUMENT ELLER EN MODIFIERAD VERSION AV ETT DOKUMENT SKULLE VISA SIG INNEHÅLLA FELAKTIGHETER I NÅGOT HÄNSEENDE ÄR DET DU (INTE DEN URSPRUNGLIGA SKRIBENTEN, FÖRFATTAREN ELLER NÅGON ANNAN MEDARBETARE) SOM FÅR STÅ FÖR ALLA EVENTUELLA KOSTNADER FÖR SERVICE, REPARATIONER ELLER KORRIGERINGAR. DENNA GARANTIFRISKRIVNING UTGÖR EN VÄSENTLIG DEL AV DETTA LICENSAVTAL. DETTA INNEBÄR ATT ALL ANVÄNDNING AV ETT DOKUMENT ELLER EN MODIFIERAD VERSION AV ETT DOKUMENT BEVILJAS ENDAST UNDER DENNA ANSVARSFRISKRIVNING;</para>
		</listitem>
		<listitem>
		  <para>UNDER INGA OMSTÄNDIGHETER ELLER INOM RAMEN FÖR NÅGON LAGSTIFTNING, OAVSETT OM DET GÄLLER KRÄNKNING (INKLUSIVE VÅRDSLÖSHET), KONTRAKT ELLER DYLIKT, SKA FÖRFATTAREN, DEN URSPRUNGLIGA SKRIBENTEN ELLER ANNAN MEDARBETARE ELLER ÅTERFÖRSÄLJARE AV DOKUMENTET ELLER AV EN MODIFIERAD VERSION AV DOKUMENTET ELLER NÅGON LEVERANTÖR TILL NÅGON AV NÄMNDA PARTER STÄLLAS ANSVARIG GENTEMOT NÅGON FÖR NÅGRA DIREKTA, INDIREKTA, SÄRSKILDA ELLER OFÖRUTSEDDA SKADOR ELLER FÖLJDSKADOR AV NÅGOT SLAG, INKLUSIVE, MEN INTE BEGRÄNSAT TILL, SKADOR BETRÄFFANDE FÖRLORAD GOODWILL, HINDER I ARBETET, DATORHAVERI ELLER NÅGRA ANDRA TÄNKBARA SKADOR ELLER FÖRLUSTER SOM KAN UPPKOMMA PÅ GRUND AV ELLER RELATERAT TILL ANVÄNDNINGEN AV DOKUMENTET ELLER MODIFIERADE VERSIONER AV DOKUMENTET, ÄVEN OM PART SKA HA BLIVIT INFORMERAD OM MÖJLIGHETEN TILL SÅDANA SKADOR.</para>
		</listitem>
	  </orderedlist></para>
  </legalnotice>


    
    <authorgroup>
      <author role="maintainer"><firstname>Erin</firstname> <surname>Bloom</surname> <affiliation> <orgname>Dokumentationsprojekt för GNOME</orgname> <address> <email>doc-writer2@gnome.org</email> </address> </affiliation></author>

<!-- This is appropriate place for other contributors: translators,
      maintainers,  etc. Commented out by default.
      
      <othercredit role="translator">
        <firstname>Latin</firstname>
        <surname>Translator 1</surname>
        <affiliation>
          <orgname>Latin Translation Team</orgname>
          <address> <email>translator@gnome.org</email> </address>
        </affiliation>
        <contrib>Latin translation</contrib>
      </othercredit>
      -->
    </authorgroup>
    
  
  <!-- According to GNU FDL, revision history is mandatory if you are -->
  <!-- modifying/reusing someone else's document.  If not, you can omit it. -->
  <!-- Remember to remove the &manrevision; entity from the revision entries other -->
  <!-- than the current revision. -->
  <!-- The revision numbering system for GNOME manuals is as follows: -->
  <!-- * the revision number consists of two components -->
  <!-- * the first component of the revision number reflects the release version of the GNOME desktop. -->
  <!-- * the second component of the revision number is a decimal unit that is incremented with each revision of the manual. -->
  <!-- For example, if the GNOME desktop release is V2.x, the first version of the manual that -->
  <!-- is written in that desktop timeframe is V2.0, the second version of the manual is V2.1, etc. -->
  <!-- When the desktop release version changes to V3.x, the revision number of the manual changes -->
  <!-- to V3.0, and so on. -->
  <revhistory>
    <revision><revnumber>Handbok för gnome-subtitles v1.0</revnumber> <date>Maj 2007</date> <revdescription>
        <para role="author">Erin Bloom <email>docwriter2@gnome.org</email></para>
        <para role="publisher">Dokumentationsprojekt för GNOME</para>
      </revdescription></revision>
  </revhistory>
  
  <releaseinfo>Den här handboken beskriver version 0.4 av gnome-subtitles</releaseinfo>
  <legalnotice>
    <title>Återkoppling</title>
    <para>För att rapportera ett fel eller föreslå någonting angående programmet <application>Gnome Undertexter</application> eller den här handboken, följ anvisningarna på <ulink url="help:gnome-feedback" type="help">GNOME:s återkopplingssida</ulink>.</para>
    <!-- Translators may also add here feedback address for translations -->
  </legalnotice>

    <othercredit class="translator">
      <personname>
        <firstname>Daniel Nylander</firstname>
      </personname>
      <email>po@danielnylander.se</email>
    </othercredit>
    <copyright>
      
        <year>2007</year>
      
      <holder>Daniel Nylander</holder>
    </copyright>
  
    <othercredit class="translator">
      <personname>
        <firstname>Josef Andersson</firstname>
      </personname>
      <email>josef.andersson@fripost.org</email>
    </othercredit>
    <copyright>
      
        <year>2015</year>
      
      <holder>Josef Andersson</holder>
    </copyright>
  
    <othercredit class="translator">
      <personname>
        <firstname>Anders Jonsson</firstname>
      </personname>
      <email>anders.jonsson@norsjovallen.se</email>
    </othercredit>
    <copyright>
      
        <year>2019</year>
      
      <holder>Anders Jonsson</holder>
    </copyright>
  </articleinfo>
<!-- Index Section -->
<indexterm zone="index"><primary>Gnome Undertexter</primary></indexterm>
<indexterm zone="index"><primary>gnomesubtitles</primary></indexterm>

<!-- ============= Document Body ============================= -->
<!-- ============= Introduction ============================== -->
<!-- Use the Introduction section to give a brief overview of what
    the application is and what it does. -->
<sect1 id="gs-introduction">
  <title>Introduktion</title>
  <para><application>Gnome Undertexter</application> är en undertextredigerare för GNOME-skrivbordet. <application>Gnome Undertexter</application> har stöd för de flesta vanliga textbaserade undertextformaten och erbjuder redigering av undertexter, konvertering och synkronisering. Dess funktioner är som följer.</para>
  <itemizedlist>
    <listitem>
      <para>Undertextformat:</para> 
      <itemizedlist> 
        <listitem><para>Advanced Sub Station Alpha</para></listitem> 
        <listitem><para>MicroDVD</para></listitem> 
        <listitem><para>MPlayer</para></listitem> 
        <listitem><para>Mplayer 2</para></listitem> 
        <listitem><para>MPSub</para></listitem> 
        <listitem><para>SubRip</para></listitem> 
        <listitem><para>Sub Station Alpha</para></listitem> 
        <listitem><para>SubViewer 1.0</para></listitem> 
        <listitem><para>SubViewer 2.0</para></listitem> 
      </itemizedlist> 
    </listitem> 
    <listitem><para>Inbyggd förhandsvisning av video</para> 
      <itemizedlist>
        <listitem><para>Använder mplayer som bakände</para></listitem>
        <listitem><para>Visning av videolängd och aktuell position</para></listitem>
        <listitem><para>Ställ in undertexttider baserat på videoposition</para></listitem>
        <listitem><para>Välj automatiskt video när undertexter öppnas</para></listitem>
      </itemizedlist>
    </listitem> 
    <listitem>
      <para>Användargränssnitt</para>
      <itemizedlist>
        <listitem><para>WYSIWYG-What you see is what you get</para></listitem>
        <listitem><para>Redigering av undertexthuvud</para></listitem>
        <listitem><para>Sök och ersätt, inklusive stöd för reguljära uttryck</para></listitem>
        <listitem><para>Ångra/Gör om</para></listitem>
      </itemizedlist>
    </listitem>
    <listitem>
      <para>Tidsåtgärder</para>
      <itemizedlist>
        <listitem><para>Autojustera tidsvärden baserat på två korrekta tids-/synkroniseringspunkter</para></listitem>
        <listitem><para>Förskjut undertexter genom en specifik fördröjning (som kan baseras på videon)</para></listitem>	
        <listitem><para>Konvertera mellan bildfrekvenser</para></listitem>
        <listitem><para>Redigering av tider och bildrutor</para></listitem>
      </itemizedlist>
    </listitem>
    <listitem><para>Övriga funktioner</para>
      <itemizedlist>
        <listitem><para>Automatisk identifiering av ett undertextformats teckenkodning (vid öppnande)</para></listitem>
        <listitem><para>Flera teckenkodningsval</para></listitem>
        <listitem><para>Tillåtande undertextläsning för att kunna läsa undertexter som innehåller fel</para></listitem>
      </itemizedlist>
    </listitem>
  </itemizedlist>
</sect1>

<!-- =========== Getting Started ============================== -->
<!-- Use the Getting Started section to describe the steps required
    to start the application and to describe the user interface components
  of the application. If there is other information that it is important
    for readers to know before they start using the application, you should
    also include this information here. 
    If the information about how to get started is very short, you can 
    include it in the Introduction and omit this section. -->

<sect1 id="gs-getting-started">
  <title>Komma igång</title> 
  
  <sect2 id="gs-start">
    <title>Starta <application>Gnome Undertexter</application></title>
    <para>Du kan starta <application><application>Gnome Undertexter</application></application> på följande sätt:</para>
    <variablelist>
      <varlistentry>
        <term><guimenu>Program</guimenu>-menyn</term>
        <listitem>
          <para>Välj <menuchoice> <guisubmenu>Ljud och video</guisubmenu> <guimenuitem>Gnome Subtitles</guimenuitem> </menuchoice>.</para>
        </listitem>
      </varlistentry>
      <varlistentry>
        <term>Kommandorad</term>
        <listitem>
          <para>Ange <command>gnome-subtitles</command> <replaceable>filnamn</replaceable> och tryck på <keycap>Retur</keycap>.</para>
          <para>Argumentet <replaceable>filnamn</replaceable> är valfritt. Om det anges kommer programmet att öppna angiven fil vid uppstart.</para>
        </listitem>
      </varlistentry>
    </variablelist>
  </sect2>
</sect1>
<sect1 id="gs-working-with-files">
  <title>Arbeta med filer</title>
  <sect2 id="gs-creating-new-doc">
    <title>Skapa ett nytt undertextdokument</title>
    <procedure>
      <title>Skapa ett nytt undertextdokument</title>
      <step performance="required">
      <para>Gå till <menuchoice> <guisubmenu>Arkiv</guisubmenu> <guimenuitem>Ny</guimenuitem> </menuchoice>.</para>
      <para>En ny fil ska öppnas i redigeraren</para>
      </step>
      </procedure>
  </sect2>
    <sect2 id="gs-opendingfile">
    <title>Öppna en fil</title>
      <procedure>
      <title>Öppna en fil</title>
      <step performance="required">
      <para>Gå till <menuchoice> <guisubmenu>Arkiv</guisubmenu> <guimenuitem>Öppna</guimenuitem> </menuchoice>.</para>
      <para>Fönstret Öppna fil ska öppnas</para>
      </step>
      <step performance="required">
      <para>Välj den undertextfil som du vill öppna.</para>
      <substeps>
      <step performance="optional">
      <para>Om du vill specificera filens teckenkodning, välj en från teckenkodningslistan. Om inte angivet kommer teckenkodningen att autoupptäckas.</para>
      </step>
      <step performance="optional">
      <para>Om du vill välja en video att öppna omedelbart, välj en video från videolistan.</para>
      </step>
      </substeps>
      </step>
      </procedure>
      <note><para>När du väljer en video kan du endast välja från videor i aktuell katalog. Om videon du vill öppna inte finns i samma katalog som undertextfilen kan du öppna videon efter att du har öppnat undertextfilen.</para>
      </note>
  </sect2>
    <sect2 id="gs-saving-file">
    <title>Spara en fil</title>
      <procedure>
      <title>Spara en fil</title>
      <para>Du kan antingen spara filen normalt eller använda ”Spara som” för andra alternativ.</para>
      <step performance="required">
      <para>Gå till <menuchoice> <guisubmenu>Arkiv</guisubmenu> <guimenuitem>Spara</guimenuitem> </menuchoice>.</para>
      <para>Fönstret Öppna fil ska öppnas</para>
      </step>
    </procedure>
    <note><para>Om du sparar en ny fil kommer fönstret ”Spara som” att visas</para></note>
    <procedure>
      <title>Spara som en ny fil</title>
      <step performance="required">
      <para>Gå till <menuchoice> <guisubmenu>Arkiv</guisubmenu> <guimenuitem>Spara som</guimenuitem> </menuchoice>.</para>
      <para>Fönstret ”Spara som” bör öppnas</para>
      </step>
      <step performance="required"><para>Ange ett nytt namn för din undertextfil</para></step>
      <step performance="optional"><para>Om du vill spara filen på en annan plats, bläddra till den nya platsen i filbläddraren</para></step>
      <step performance="optional"><para>Om du vill spara filen i ett annat format än formatet listat i undertextformatslistan, välj ett annat format.</para></step>
      <step performance="optional"><para>Om du vill spara filen i en annan teckenkodning än din standardteckenkodning, ändra teckenkodningen i teckenkodningslistan.</para></step>
      <step performance="required"><para>Klicka på Spara</para></step>
    </procedure>
  </sect2>
  
    <sect2 id="gs-select-char-coding">
    <title>Välja en teckenkodning</title>
    <para>Om du använder speciella tecken i din undertextfil kommer du att vilja vara säker på att din fil sparas i en kodning som stöder dessa tecken.</para>
    <caution><para>Att spara en fil i en teckenkodning som inte stöder specialtecknen kommer att orsaka förlust av teckendata. Det inträffar när du försöker att spara multibytetecken i en teckenkodning som använder enskilda byte.</para></caution>
  </sect2>
    <sect2 id="gs-editing-headers">
    <title>Redigera undertextrubriker</title>

        <procedure>
  <para>En del format hade filhuvuden som innehåller information om filen. Du kan redigera dessa fält i <application>Gnome Undertexter</application> genom</para>
      <step performance="required">
      <para>Gå till <menuchoice> <guisubmenu>Arkiv</guisubmenu> <guimenuitem>Filhuvud</guimenuitem> </menuchoice>.</para>
      <para>Filhuvudfönstret kommer att öppnas. Det har fyra flikar.</para>
      </step>
      <step performance="required"> <para>Välj fliken som motsvarar formatet på din undertextfil</para></step>
      <step performance="required"><para>Fyll i lämpliga fält.</para></step>
      <step performance="required"><para>När du är klar med inmatningen av data i filhuvudet, klicka OK</para></step>
    </procedure>
  </sect2>
</sect1>
    <!-- ============= Working with Subtitles ================================== -->
<sect1 id="gs-working-with-subs">
<title>Arbeta med undertexter</title>
<note><para>Denna hjälp listar tangentbordsgenvägarna för att genomföra uppgifter. Alla uppgifter kan också genomföras genom att använda Redigera-menyn istället för tangentbordsgenvägar.</para></note>
      <sect2 id="gs-wws-adding">
        <title>Lägga till en undertext</title>
        <para>Nya undertexter läggs till antingen före eller efter aktuell undertext.</para>
        <procedure>
          <title>För att lägga till en ny undertext efter aktuell undertext</title>
      <step performance="required">
      <para>Tryck <keycap>Insert</keycap> eller <keycap>Ctrl</keycap>+<keycap>Retur</keycap></para>
      </step>
    </procedure>
            <procedure>
          <title>För att lägga till en ny undertext före aktuell undertext</title>
      <step performance="required">
      <para>Tryck <keycap>Skift</keycap>+<keycap>Ctrl</keycap>+<keycap>Retur</keycap></para>
      </step>
    </procedure>
      </sect2>     
      <sect2 id="gs-wws-moving">
        <title>Flytta mellan undertexter</title>
        <para>För att välja undertexter kan du antingen använda din mus för att klicka på undertexten eller när din markör är i redigerarfönstret kan du använda tangentbordsgenvägar för att flytta mellan undertexter</para>
        <procedure>
          <title>Att flytta till nästa undertext</title>
      <step performance="required">
      <para>Tryck <keycap>Ctrl</keycap>+<keycap>Page Down</keycap></para>
      </step>
    </procedure>
            <procedure>
          <title>Att flytta till föregående undertext</title>
      <step performance="required">
      <para>Tryck <keycap>Ctrl</keycap>+<keycap>Page Up</keycap></para>
      </step>
    </procedure>
      </sect2>     
      <sect2 id="gs-wws-removing">
        <title>Ta bort undertext(er)</title>
      <procedure><step><para>Tryck <keycap>Delete</keycap></para></step></procedure>
      </sect2>      
      <sect2 id="gs-wws-batch">
        <title>Arbeta med flera undertexter</title>
        <para>Ibland kommer du att vilja välja flera undertexter. Använd <keycap>Skift</keycap> för att välja sekventiella undertexter och <keycap>Ctrl</keycap> för att välja icke-sekventiella undertexter</para>
        <para>För att välja alla undertexter tryck <keycap>Ctrl</keycap>+<keycap>A</keycap></para>
      </sect2>      
      <sect2 id="gs-wws-editing">
        <title>Redigera text</title>
        <para>Att redigera text i en undertext</para>
          <procedure>
            <step><para>Välj undertexten</para></step>
            <step><para>Klicka i redigeringsfönstret för att flytta markören in i redigerarfönstret</para></step>
          </procedure>
      </sect2>
      <sect2 id="gs-wws-format">
        <title>Textformat</title>
        <note><para>Ett typformat (fet, kursiv, understruken) kan tillämpas endast på hela raden. För närvarande, om du vill ha vissa tecken speciellt formaterade behöver du redigera undertextfilen i en textredigerare.</para></note>
      </sect2>
      <sect2 id="gs-wws-oops">
        <title>Ångra och göra om</title>
        <procedure><title>Ångra en åtgärd</title><step><para>Tryck <keycap>Ctrl</keycap>+ <keycap>Z</keycap></para></step></procedure>
        <procedure><title>Göra om en åtgärd</title><step><para>Tryck <keycap>Ctrl</keycap>+<keycap>Y</keycap> eller <keycap>Ctrl</keycap>+<keycap>Skift</keycap>+<keycap>Z </keycap></para></step></procedure>
      </sect2>      
      <sect2 id="gs-wws-ccp">
        <title>Klipp ut, kopiera och klistra in</title>
        <para>Du kan klippa ut, kopiera och klistra in text i en undertext.</para>
        <procedure><title>Kopiera text</title><step><para><keycap>Ctrl</keycap>+ <keycap>C</keycap></para></step></procedure>
        <procedure><title>Klippa ut text</title><step><para><keycap>Ctrl</keycap>+ <keycap>X</keycap></para></step></procedure>
        <procedure><title>Klistra in text</title><step><para><keycap>Ctrl</keycap>+ <keycap>V</keycap></para></step></procedure>
      </sect2>
</sect1>
    <!-- ============= find/replace ================================== -->
<!--    <sect1 id="gs-findreplace">
      <title>Finding and Replacing</title>
      <sect2 id="gs-fr-finding">
        <title>Finding</title>
        <para>Questions and general discussions should be posted on the <ulink url="http://sourceforge.net/forum/?group_id=129996" type="http">Forum</ulink> (no registration needed).</para>
      </sect2>
            <sect2 id="gs-fr-replacing">
        <title>Replacing</title>
        <para>You might get a quick response by asking the <ulink url="http://lists.sourceforge.net/lists/listinfo/gnome-subtitles-general" type="http">Mailing List</ulink> (<ulink url="http://sourceforge.net/mailarchive/forum.php?forum=gnome-subtitles-general" type="http">archives</ulink> are available).</para>
      </sect2>
      <sect2 id="gs-fr-options">
        <title>Find and Replace Options</title>
        <para>Questions and general discussions should be posted on the <ulink url="http://sourceforge.net/forum/?group_id=129996" type="http">Forum</ulink> (no registration needed).</para>
      </sect2>
      <sect2 id="gs-regex">
        <title>Using Regular Expressions</title>
        <para>Bugs and Feature Requests can be reported at the official <ulink url="http://bugzilla.gnome.org/browse.cgi?product=gnome-subtitles" type="http">Gnome Bugzilla</ulink>.</para>
      </sect2>
    </sect1>
-->
    <!-- ============= Timings ================================== -->
    <sect1 id="gs-timings">
      <title>Manipulera undertexter</title>
      <sect2 id="gs-timings-settingview">
        <title>Ange undertextenheter</title>
        <para>I <application>Gnome Undertexter</application> kan du ange och justera undertexter i tidsenheter (minuter och sekunder) eller i bildruteenheter. Standardenheten är tid.</para>
            <procedure><title>Att använda bildruteenheter</title>
              <step><para>Gå till <menuchoice> <guisubmenu>Visa</guisubmenu> <guimenuitem>Bildrutor</guimenuitem> </menuchoice>.</para></step></procedure>
              <procedure><title>Att använda tidsenheter</title>
              <step><para>Gå till <menuchoice> <guisubmenu>Visa</guisubmenu> <guimenuitem>Tider</guimenuitem> </menuchoice>.</para></step></procedure>
      </sect2>
      <sect2 id="gs-timings-Adjusting">
        <title>Justera tidsvärden</title>
           <procedure>
             <step><para>Om du vill justera tidsvärdena för endast några få undertexter, markera undertexterna du vill justera</para></step>
              <step><para>Gå till <menuchoice> <guisubmenu>Tider</guisubmenu> <guimenuitem>Justera</guimenuitem> </menuchoice>.</para></step>
            <step><para>Välj den nya starttiden för den första undertexten och den nya starttiden för den sista undertexten</para></step>
      </procedure>
      </sect2>
            <sect2 id="gs-timings-Shifitng">
        <title>Tidsförskjutning</title>
        <para>Förskjut tidsvärden låter dig flytta en eller flera undertexter framåt eller bakåt en angiven mängd tid. Olikt egenskapen Justera undertexter påverkar inte Förskjut undertexter längden på undertexterna.</para>
              <procedure><title>Att använda förskjut undertexter</title>
                <step><para>Välj vilka undertexter du vill förskjuta. Om du vill förskjuta tid/bildruta för en undertext och/eller alla undertexter före eller efter en specifik undertext, markera den undertexten. Om du vill förskjuta fler än en specifik undertext, välj alla undertexter du vill förskjuta.</para></step>
              <step><para>Gå till <menuchoice> <guisubmenu>Tider</guisubmenu> <guimenuitem>Förskjut</guimenuitem> </menuchoice>.</para></step>
            <step><para>Ange tidsmängd (eller bildrutor om du använder enheten bildruta) att justera dina undertexter. Om du vill flytta undertexterna bakåt ange ett negativt värde, annars ange ett positivt värde.</para></step>
            <step><para>Välj hur du vill förskjuta dina undertexter.</para></step>
            <step><para>Klicka Förskjut för att förskjuta undertexterna.</para></step>
      </procedure>
      </sect2>
      <sect2 id="gs-timings-inputfr">
        <title>Ange indata- och videobildfrekvenserna</title>
        <para>När du öppnar en video sätter <application>Gnome Undertexter</application> inte automatiskt bildfrekvensen för videon.</para>
        <para><application>Gnome Undertexter</application> manipulerar undertexter baserat på tid, oavsett deras visningsläge. Därför kommer <application>Gnome undertexter</application> att justera bildrutorna för att kompensera för förändringen. Undertextens längd förblir densamma.</para>
        <caution><para>Om ditt undertextformat använder bildrutor för att avgöra när en undertext visas (MicroDVD), kan en ändring av bildfrekvensen medföra att dina undertexter förlorar sin synk.</para></caution>
        <procedure><title>Att ange videons bildfrekvens</title>
              <step><para>Gå till <menuchoice> <guisubmenu>Tider</guisubmenu> <guimenuitem>Videobildfrekvens</guimenuitem> </menuchoice> och välj passande bildrutehastighet.</para></step>
      </procedure>
      <procedure><title>Att ange bildfrekvens för indata</title>
              <step><para>Gå till <menuchoice> <guisubmenu>Tider</guisubmenu> <guimenuitem>Bildfrekvens för indata</guimenuitem> </menuchoice> och välj passande bildfrekvens.</para></step>
      </procedure>
      </sect2>
    </sect1>
     <!-- ============= Video ================================== -->
    <sect1 id="gs-video">
      <title>Arbeta med videoklipp</title>
      <sect2 id="gs-video-opening">
        <title>Öppna och stänga en video</title>
        <procedure>
          <title>Öppna en video</title>
          <step><para>Gå till <menuchoice> <guisubmenu>Video</guisubmenu> <guimenuitem>Öppna</guimenuitem> </menuchoice></para></step>
        </procedure>
        <procedure>
          <title>Stänga en video</title>
          <step><para>Gå till <menuchoice> <guisubmenu>Video</guisubmenu> <guimenuitem>Stäng</guimenuitem> </menuchoice></para></step>
        </procedure>
      </sect2>
      <sect2 id="gs-video-playback">
        <title>Uppspelning</title>
        <para><application>Gnome Undertexter</application> kommer att spela videon i det nya fönstret med dina undertexter.</para>
      </sect2>
      <sect2 id="gs-video-seeking">
        <title>Spolning till markeringen</title>
        <para>Du kan gå till den punkt i den inlästa videon där en undertext börjar</para>
        <procedure>
          <title>Att spola till en undertext</title>
          <step><para>Gå till <menuchoice> <guisubmenu>Video</guisubmenu> <guimenuitem>Spola till markering</guimenuitem> </menuchoice></para></step>
        </procedure>
      </sect2>
      <sect2 id="gs-video-settingsubs">
        <title>Inställning av undertextens start- eller slutposition</title>
        <para>Du kan sätta start- och sluttid baserat på punkten för när den inlästa videon är pausad</para>
        <procedure>
          <title>För att ställa in starttiden för undertexten</title>
          <step><para>Gå till <menuchoice> <guisubmenu>Video</guisubmenu> <guimenuitem>Ange undertextstart</guimenuitem> </menuchoice></para></step>
        </procedure>
        <procedure>
          <title>För att ställa in sluttiden för undertexten</title>
          <step><para>Gå till <menuchoice> <guisubmenu>Video</guisubmenu> <guimenuitem>Ange undertextslut</guimenuitem> </menuchoice></para></step>
        </procedure>
      </sect2>
    </sect1>
   <!-- ============= Support ================================== -->
    <sect1 id="gs-support">
      <title>Var man kan få ytterligare stöd</title>
      <sect2 id="gs-support-forums">
        <title>Forum</title>
        <para>Frågor och allmänna diskussioner ska postas på <ulink url="http://sourceforge.net/forum/?group_id=129996" type="http">forumet</ulink> (ingen registrering krävs).</para>
      </sect2>
            <sect2 id="gs-support-mailinglist">
        <title>Sändlista</title>
        <para>Du kan få snabba svar genom att fråga på <ulink url="http://lists.sourceforge.net/lists/listinfo/gnome-subtitles-general" type="http">sändlistan</ulink> (<ulink url="http://sourceforge.net/mailarchive/forum.php?forum_name=gnome-subtitles-general" type="http">arkiv</ulink> finns tillgängliga).</para>
      </sect2>
      <sect2 id="gs-support-bugzilla">
        <title>Fel och utvecklingsförslag</title>
        <para>Fel och utvecklingsförslag kan rapporteras i <ulink url="https://gitlab.gnome.org/GNOME/gnome-subtitles/issues/" type="http">GNOME:s felrapporteringssystem</ulink>.</para>
      </sect2>
    </sect1>
  </article>
