<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE article PUBLIC "-//OASIS//DTD DocBook XML V4.1.2//EN" "http://www.oasis-open.org/docbook/xml/4.1.2/docbookx.dtd" [
<!ENTITY legal SYSTEM "legal.xml">
<!ENTITY appversion "0.4">
<!ENTITY manrevision "1.0">
<!ENTITY date "May 2007">
<!ENTITY app "<application>Gnome Subtitles</application>">
]>
<!--
    (Do not remove this comment block.)
    Maintained by the GNOME Documentation Project
    http://developer.gnome.org/projects/gdp
    Template version: 2.0 beta
    Template last modified Apr 11, 2002
    -->
<!-- =============Document Header ============================= -->
<article id="index" lang="de">
  <!-- please do not change the id; for translations, change lang to -->
  <!-- appropriate code -->
  <articleinfo>
    <title><application>Gnome Subtitles</application>-Handbuch V1.0</title>
    
    <copyright lang="en">
      <year>2007</year>
      <holder>Erin Bloom</holder>
    </copyright>
    <abstract role="description"><para>Die Aufgaben von <application>Gnome Subtitles</application></para>
    </abstract>
    <!-- An address can be added to the publisher information.  If a role is
          not specified, the publisher/author is the same for all versions of the
          document.  -->
    <publisher>
      <publishername>GNOME-Dokumentationsprojekt</publishername>
    </publisher>
    
      <legalnotice id="legalnotice">
	<para>Das vorliegende Dokument kann gemäß den Bedingungen der GNU Free Documentation License (GFDL), Version 1.1 oder jeder späteren, von der Free Software Foundation veröffentlichten Version ohne unveränderbare Abschnitte sowie ohne Texte auf dem vorderen und hinteren Buchdeckel kopiert, verteilt und/oder modifiziert werden. Eine Kopie der GFDL finden Sie unter diesem <ulink type="help" url="ghelp:fdl">Link</ulink> oder in der mit diesem Handbuch gelieferten Datei COPYING-DOCS.</para>
         <para>Dieses Handbuch ist Teil einer Sammlung von GNOME-Handbüchern, die unter der GFDL veröffentlicht werden. Wenn Sie dieses Handbuch getrennt von der Sammlung weiterverbreiten möchten, können Sie das tun, indem Sie eine Kopie der Lizenz zum Handbuch hinzufügen, wie es in Abschnitt 6 der Lizenz beschrieben ist.</para>

	<para>Viele der Namen, die von Unternehmen verwendet werden, um ihre Produkte und Dienstleistungen von anderen zu unterscheiden, sind eingetragene Warenzeichen. An den Stellen, an denen diese Namen in einer GNOME-Dokumentation erscheinen, werden die Namen in Großbuchstaben oder mit einem großen Anfangsbuchstaben geschrieben, wenn das GNOME-Dokumentationsprojekt auf diese Warenzeichen hingewiesen wird.</para>

	<para lang="en">
	  DOCUMENT AND MODIFIED VERSIONS OF THE DOCUMENT ARE PROVIDED
	  UNDER  THE TERMS OF THE GNU FREE DOCUMENTATION LICENSE
	  WITH THE FURTHER UNDERSTANDING THAT:

	  <orderedlist>
		<listitem>
		  <para lang="en">DOCUMENT IS PROVIDED ON AN "AS IS" BASIS,
                    WITHOUT WARRANTY OF ANY KIND, EITHER EXPRESSED OR
                    IMPLIED, INCLUDING, WITHOUT LIMITATION, WARRANTIES
                    THAT THE DOCUMENT OR MODIFIED VERSION OF THE
                    DOCUMENT IS FREE OF DEFECTS MERCHANTABLE, FIT FOR
                    A PARTICULAR PURPOSE OR NON-INFRINGING. THE ENTIRE
                    RISK AS TO THE QUALITY, ACCURACY, AND PERFORMANCE
                    OF THE DOCUMENT OR MODIFIED VERSION OF THE
                    DOCUMENT IS WITH YOU. SHOULD ANY DOCUMENT OR
                    MODIFIED VERSION PROVE DEFECTIVE IN ANY RESPECT,
                    YOU (NOT THE INITIAL WRITER, AUTHOR OR ANY
                    CONTRIBUTOR) ASSUME THE COST OF ANY NECESSARY
                    SERVICING, REPAIR OR CORRECTION. THIS DISCLAIMER
                    OF WARRANTY CONSTITUTES AN ESSENTIAL PART OF THIS
                    LICENSE. NO USE OF ANY DOCUMENT OR MODIFIED
                    VERSION OF THE DOCUMENT IS AUTHORIZED HEREUNDER
                    EXCEPT UNDER THIS DISCLAIMER; AND
		  </para>
		</listitem>
		<listitem>
		  <para lang="en">UNDER NO CIRCUMSTANCES AND UNDER NO LEGAL
                       THEORY, WHETHER IN TORT (INCLUDING NEGLIGENCE),
                       CONTRACT, OR OTHERWISE, SHALL THE AUTHOR,
                       INITIAL WRITER, ANY CONTRIBUTOR, OR ANY
                       DISTRIBUTOR OF THE DOCUMENT OR MODIFIED VERSION
                       OF THE DOCUMENT, OR ANY SUPPLIER OF ANY OF SUCH
                       PARTIES, BE LIABLE TO ANY PERSON FOR ANY
                       DIRECT, INDIRECT, SPECIAL, INCIDENTAL, OR
                       CONSEQUENTIAL DAMAGES OF ANY CHARACTER
                       INCLUDING, WITHOUT LIMITATION, DAMAGES FOR LOSS
                       OF GOODWILL, WORK STOPPAGE, COMPUTER FAILURE OR
                       MALFUNCTION, OR ANY AND ALL OTHER DAMAGES OR
                       LOSSES ARISING OUT OF OR RELATING TO USE OF THE
                       DOCUMENT AND MODIFIED VERSIONS OF THE DOCUMENT,
                       EVEN IF SUCH PARTY SHALL HAVE BEEN INFORMED OF
                       THE POSSIBILITY OF SUCH DAMAGES.
		  </para>
		</listitem>
	  </orderedlist>
	</para>
  </legalnotice>


    
    <authorgroup>
      <author role="maintainer" lang="en">
        <firstname>Erin</firstname>
        <surname>Bloom</surname>
        <affiliation>
          <orgname>GNOME Documentation Project</orgname>
          <address> <email>doc-writer2@gnome.org</email> </address>
        </affiliation>
      </author>

<!-- This is appropriate place for other contributors: translators,
      maintainers,  etc. Commented out by default.
      
      <othercredit role="translator">
        <firstname>Latin</firstname>
        <surname>Translator 1</surname>
        <affiliation>
          <orgname>Latin Translation Team</orgname>
          <address> <email>translator@gnome.org</email> </address>
        </affiliation>
        <contrib>Latin translation</contrib>
      </othercredit>
      -->
    </authorgroup>
    
  
  <!-- According to GNU FDL, revision history is mandatory if you are -->
  <!-- modifying/reusing someone else's document.  If not, you can omit it. -->
  <!-- Remember to remove the &manrevision; entity from the revision entries other -->
  <!-- than the current revision. -->
  <!-- The revision numbering system for GNOME manuals is as follows: -->
  <!-- * the revision number consists of two components -->
  <!-- * the first component of the revision number reflects the release version of the GNOME desktop. -->
  <!-- * the second component of the revision number is a decimal unit that is incremented with each revision of the manual. -->
  <!-- For example, if the GNOME desktop release is V2.x, the first version of the manual that -->
  <!-- is written in that desktop timeframe is V2.0, the second version of the manual is V2.1, etc. -->
  <!-- When the desktop release version changes to V3.x, the revision number of the manual changes -->
  <!-- to V3.0, and so on. -->
  <revhistory>
    <revision lang="en">
      <revnumber>gnome-subtitles Manual V1.0</revnumber>
      <date>May 2007</date>
      <revdescription>
        <para role="author" lang="en">Erin Bloom
          <email>docwriter2@gnome.org</email>
        </para>
        <para role="publisher" lang="en">GNOME Documentation Project</para>
      </revdescription>
    </revision>
  </revhistory>
  
  <releaseinfo>Dieses Handbuch beschreibt Version 1.2.8 von Gnome Subtitles.</releaseinfo>
  <legalnotice>
    <title>Rückmeldungen</title>
    <para lang="en">To report a bug or make a suggestion regarding the <application>Gnome Subtitles</application> application or
      this manual, follow the directions in the <ulink url="help:gnome-feedback" type="help">GNOME Feedback Page</ulink>.
    </para>
    <!-- Translators may also add here feedback address for translations -->
  </legalnotice>
</articleinfo>
<!-- Index Section -->
<indexterm zone="index" lang="en">
  <primary>Gnome Subtitles</primary>
</indexterm>
<indexterm zone="index" lang="en">
  <primary>gnomesubtitles</primary>
</indexterm>

<!-- ============= Document Body ============================= -->
<!-- ============= Introduction ============================== -->
<!-- Use the Introduction section to give a brief overview of what
    the application is and what it does. -->
<sect1 id="gs-introduction">
  <title>Einführung</title>
  <para><application>Gnome Subtitles</application> ist ein Untertitel-Editor für den GNOME-Desktop. <application>Gnome Subtitles</application> unterstützt die gebräuchlichsten Untertitel-Textformate und erlaubt das Bearbeiten, Umwandeln und Synchronisieren von Untertiteln. Nachfolgend werden die Funktionsmerkmale beschrieben.</para>
  <itemizedlist>
    <listitem>
      <para>Untertitelformate:</para> 
      <itemizedlist> 
        <listitem><para>Advanced Sub Station Alpha</para></listitem> 
        <listitem><para>MicroDVD</para></listitem> 
        <listitem><para>MPlayer</para></listitem> 
        <listitem><para>Mplayer 2</para></listitem> 
        <listitem><para>MPSub</para></listitem> 
        <listitem><para>SubRip</para></listitem> 
        <listitem><para>Sub Station Alpha</para></listitem> 
        <listitem><para>SubViewer 1.0</para></listitem> 
        <listitem><para>SubViewer 2.0</para></listitem> 
      </itemizedlist> 
    </listitem> 
    <listitem><para>Eingebaute Videovorschau</para> 
      <itemizedlist>
        <listitem><para>Verwendet mplayer als Backend</para></listitem>
        <listitem><para>Anzeige der Videolänge und der aktuellen Position</para></listitem>
        <listitem><para>Setzen der Untertitel-Synchronisation anhand der Videoposition</para></listitem>
        <listitem><para>Automatische Videoauswahl beim Öffnen von Untertiteln</para></listitem>
      </itemizedlist>
    </listitem> 
    <listitem>
      <para>Benutzeroberfläche</para>
      <itemizedlist>
        <listitem><para>WYSIWYG-What you see is what you get</para></listitem>
        <listitem><para>Bearbeiten der Kopfzeilen der Untertitel</para></listitem>
        <listitem><para>Suchen und Ersetzen, einschließlich Unterstützung für reguläre Ausdrücke</para></listitem>
        <listitem><para>Rückgängig machen/Wiederholen</para></listitem>
      </itemizedlist>
    </listitem>
    <listitem>
      <para>Synchronisationen</para>
      <itemizedlist>
        <listitem><para>Automatische Synchronisation anhand zweier korrekter Bezugspunkte</para></listitem>
        <listitem><para>Verschieben von Untertiteln anhand eines vorgegebenen Versatzwertes (auch basierend auf den Werten des Videos)</para></listitem>	
        <listitem><para>Bildraten umwandeln</para></listitem>
        <listitem><para>Bearbeiten anhand von Zeitangaben und Bildpositionen</para></listitem>
      </itemizedlist>
    </listitem>
    <listitem><para>Weitere Funktionsmerkmale</para>
      <itemizedlist>
        <listitem><para>Automatische Erkennung der Zeichenkodierung der Untertitel beim Öffnen</para></listitem>
        <listitem><para>Auswahl aus mehreren Zeichenkodierungen</para></listitem>
        <listitem><para>Vereinfachtes Lesen von Untertiteln, falls diese fehlerhaft sind</para></listitem>
      </itemizedlist>
    </listitem>
  </itemizedlist>
</sect1>

<!-- =========== Getting Started ============================== -->
<!-- Use the Getting Started section to describe the steps required
    to start the application and to describe the user interface components
  of the application. If there is other information that it is important
    for readers to know before they start using the application, you should
    also include this information here. 
    If the information about how to get started is very short, you can 
    include it in the Introduction and omit this section. -->

<sect1 id="gs-getting-started">
  <title>Erste Schritte</title> 
  
  <sect2 id="gs-start">
    <title><application>GNOME Subtitles</application> starten</title>
    <para>Sie können <application>GNOME Subtitles</application> auf eine der folgenden Arten starten:</para>
    <variablelist>
      <varlistentry>
        <term>Menü <guimenu>Anwendungen</guimenu></term>
        <listitem>
          <para lang="en">Choose
            <menuchoice>
              <guisubmenu>Sound &amp; Video</guisubmenu>
              <guimenuitem>Gnome Subtitles</guimenuitem>
            </menuchoice>. </para>
        </listitem>
      </varlistentry>
      <varlistentry>
        <term>Befehlszeile</term>
        <listitem>
          <para lang="en">Type <command>gnome-subtitles</command> <replaceable>filename</replaceable> and press <keycap>Enter</keycap>.</para>
          <para>Das Argument <replaceable>Dateiname</replaceable> ist optional. Falls Sie es angeben, öffnet die Anwendung diese Datei beim Start.</para>
        </listitem>
      </varlistentry>
    </variablelist>
  </sect2>
</sect1>
<sect1 id="gs-working-with-files">
  <title>Arbeiten mit Dateien</title>
  <sect2 id="gs-creating-new-doc">
    <title>Erstellen eines neuen Untertitel-Dokuments</title>
    <procedure>
      <title>Erstellen eines neuen Untertitel-Dokuments</title>
      <step performance="required">
      <para lang="en">
      Go to <menuchoice>
              <guisubmenu>File</guisubmenu>
              <guimenuitem>New</guimenuitem>
            </menuchoice>. </para>
      <para>Eine neue Datei wird im Editor geöffnet.</para>
      </step>
      </procedure>
  </sect2>
    <sect2 id="gs-opendingfile">
    <title>Öffnen einer Datei</title>
      <procedure>
      <title>Öffnen einer Datei</title>
      <step performance="required">
      <para lang="en">
      Go to <menuchoice>
              <guisubmenu>File</guisubmenu>
              <guimenuitem>Open</guimenuitem>
            </menuchoice>. </para>
      <para>Der Dateiauswahldialog wird geöffnet.</para>
      </step>
      <step performance="required">
      <para>Wählen Sie die Untertiteldatei aus, die Sie öffnen wollen.</para>
      <substeps>
      <step performance="optional">
      <para>Falls Sie die Zeichenkodierung angeben wollen, wählen Sie den entsprechenden Eintrag in »Zeichenkodierung« aus. Wenn Sie nichts angeben, wird die Zeichenkodierung automatisch ermittelt.</para>
      </step>
      <step performance="optional">
      <para>Wenn Sie ein ein Video unmittelbar öffnen wollen, wählen Sie eines aus der Videoliste aus.</para>
      </step>
      </substeps>
      </step>
      </procedure>
      <note><para>Bei der Auswahl eines Videos können Sie nur Dateien aus dem gegenwärtig geöffneten Ordner wählen. Falls sich das gewünschte Video nicht im gleichen Ordner wie die Untertiteldatei befindet, können Sie es nach dem Öffnen der Untertiteldatei auswählen.</para>
      </note>
  </sect2>
    <sect2 id="gs-saving-file">
    <title>Speichern einer Datei</title>
      <procedure>
      <title>Speichern einer Datei</title>
      <para>Sie können eine Datei entweder normal speichern oder mit  »Speichern unter« weitere Optionen wählen.</para>
      <step performance="required">
      <para lang="en">
      Go to <menuchoice>
              <guisubmenu>File</guisubmenu>
              <guimenuitem>Save</guimenuitem>
            </menuchoice>. </para>
      <para>Der Dateiauswahldialog wird geöffnet.</para>
      </step>
    </procedure>
    <note><para>Falls Sie in eine neue Datei speichern, wird das »Speichern unter«-Fenster geöffnet.</para></note>
    <procedure>
      <title>Speichern als neue Datei</title>
      <step performance="required">
      <para lang="en">
      Go to <menuchoice>
              <guisubmenu>File</guisubmenu>
              <guimenuitem>Save As</guimenuitem>
            </menuchoice>. </para>
      <para>Das Fenster »Speichern unter« wird geöffnet.</para>
      </step>
      <step performance="required"><para>Geben Sie einen neuen Namen für die Untertiteldatei an.</para></step>
      <step performance="optional"><para>Falls Sie die Datei an einem anderen Ort speichern wollen, wählen Sie den neuen Ort im Dateiauswahldialog aus.</para></step>
      <step performance="optional"><para>Falls Sie die Datei in einem Format speichern wollen, das nicht in der Liste der Untertitelformate aufgeführt ist, wählen Sie ein anderes Format aus.</para></step>
      <step performance="optional"><para>Falls Sie die Datei nicht in der angezeigten Standard-Zeichenkodierung speichern wollen, ändern Sie die Kodierung in der Liste der Zeichenkodierungen wie gewünscht.</para></step>
      <step performance="required"><para>Klicken Sie auf <guilabel>Speichern</guilabel>.</para></step>
    </procedure>
  </sect2>
  
    <sect2 id="gs-select-char-coding">
    <title>Auswahl einer Zeichenkodierung</title>
    <para>Wenn Sie in Ihrer Untertiteldatei Sonderzeichen verwenden, dann sollten Sie sicherstellen, dass Ihre Datei in einer Zeichenkodierung gespeichert wird, die diese Zeichen unterstützt.</para>
    <caution><para>Das Speichern einer Datei in einer Zeichenkodierung, die die Sonderzeichen nicht unterstützt, führt zum Verlust von Daten dieser Zeichen. Dies geschieht, wenn Sie Multi-Byte-Zeichen in einer Single-Byte-Zeichenkodierung zu speichern versuchen.</para></caution>
  </sect2>
    <sect2 id="gs-editing-headers">
    <title>Bearbeiten der Kopfzeilen der Untertitel</title>

        <procedure>
  <para>Einige Untertitelformate haben Kopfzeilen, die Informationen über die Datei enthalten. Sie können diese Felder in <application>Gnome Subtitles</application> folgendermaßen bearbeiten:</para>
      <step performance="required">
      <para lang="en">
      Go to <menuchoice>
              <guisubmenu>File</guisubmenu>
              <guimenuitem>Headers</guimenuitem>
            </menuchoice>. </para>
      <para>Das Kopfzeilen-Fenster wird geöffnet, welches vier Reiter enthält.</para>
      </step>
      <step performance="required"> <para>Wählen Sie den Reiter aus, der dem Format Ihrer Untertiteldatei entspricht.</para></step>
      <step performance="required"><para>Füllen Sie die entsprechenden Felder aus.</para></step>
      <step performance="required"><para>Sobald Sie mit der Eingabe der Kopfzeilen fertig sind, klicken Sie auf <guilabel>OK</guilabel>.</para></step>
    </procedure>
  </sect2>
</sect1>
    <!-- ============= Working with Subtitles ================================== -->
<sect1 id="gs-working-with-subs">
<title>Arbeiten mit Untertiteln</title>
<note><para>Nachfolgend werden zu bestimmten Aufgaben zugeordnete Tastenkürzel aufgelistet. Alle diese Aufgaben erreichen Sie auch über das Menü <guimenu>Bearbeiten</guimenu>.</para></note>
      <sect2 id="gs-wws-adding">
        <title>Hinzufügen eines Untertitels</title>
        <para>Neue Untertitel werden entweder vor oder nach dem gegenwärtigen Untertitel eingefügt.</para>
        <procedure>
          <title>Hinzufügen eines neuen Untertitels nach dem gegenwärtigen Untertitel</title>
      <step performance="required">
      <para>Drücken Sie <keycap>Einfg</keycap> oder <keycap>Strg</keycap>+<keycap>Eingabetaste</keycap>.</para>
      </step>
    </procedure>
            <procedure>
          <title>Hinzufügen eines neuen Untertitels vor dem gegenwärtigen Untertitel</title>
      <step performance="required">
      <para>Drücken Sie <keycap>Umschalt</keycap>+<keycap>Strg</keycap>+<keycap>Eingabetaste</keycap>.</para>
      </step>
    </procedure>
      </sect2>     
      <sect2 id="gs-wws-moving">
        <title>Bewegen innerhalb der Untertitel</title>
        <para>Um Untertitel auszuwählen, können Sie entweder mit der Maus darauf klicken oder mittels Tastenkürzeln zwischen den Untertiteln wechseln, sofern sich der Cursor im Editorfenster befindet.</para>
        <procedure>
          <title>Zum nächsten Untertitel gehen</title>
      <step performance="required">
      <para>Drücken Sie <keycap>Strg</keycap>+<keycap>Bild ab</keycap>.</para>
      </step>
    </procedure>
            <procedure>
          <title>Zum vorherigen Untertitel gehen</title>
      <step performance="required">
      <para>Drücken Sie <keycap>Strg</keycap>+<keycap>Bild auf</keycap>.</para>
      </step>
    </procedure>
      </sect2>     
      <sect2 id="gs-wws-removing">
        <title>Entfernen eines oder mehrerer Untertitel</title>
      <procedure><step><para>Drücken Sie <keycap>Entf</keycap></para></step></procedure>
      </sect2>      
      <sect2 id="gs-wws-batch">
        <title>Arbeiten mit mehreren Untertiteln</title>
        <para>Gelegentlich möchten Sie vielleicht mehrere Untertitel auswählen. Drücken Sie dazu die <keycap>Umschalttaste</keycap>, oder die <keycap>Strg</keycap>-Taste, falls Sie nicht unmittelbar aufeinanderfolgende Untertitel auswählen wollen.</para>
        <para>Um alle Untertitel auszuwählen, drücken Sie <keycap>Strg</keycap>+<keycap>A</keycap>.</para>
      </sect2>      
      <sect2 id="gs-wws-editing">
        <title>Text bearbeiten</title>
        <para>Bearbeiten von Text in einem Untertitel</para>
          <procedure>
            <step><para>Untertitel auswählen</para></step>
            <step><para>Klicken Sie in das Editorfenster, um den Cursor hinein zu setzen.</para></step>
          </procedure>
      </sect2>
      <sect2 id="gs-wws-format">
        <title>Textformat</title>
        <note><para>Ein Schriftschnitt (fett, kursiv oder unterstrichen) kann nur auf die gesamte Zeile angewendet werden. Derzeit müssen Sie die Untertiteldatei in einem Texteditor bearbeiten, wenn Sie lediglich bestimmte Zeichen speziell formatieren wollen.</para></note>
      </sect2>
      <sect2 id="gs-wws-oops">
        <title>Rückgängig machen und Wiederholen</title>
        <procedure><title>Eine Aktion rückgängig machen</title><step><para>Drücken Sie <keycap>Strg</keycap>+ <keycap>Z</keycap></para></step></procedure>
        <procedure><title>Rückgängigmachen einer Aktion</title><step><para>Drücken Sie <keycap>Strg</keycap>+ <keycap>Y</keycap> oder <keycap>Strg</keycap>+<keycap>Umschalt</keycap>-<keycap>Z </keycap>.</para></step></procedure>
      </sect2>      
      <sect2 id="gs-wws-ccp">
        <title>Ausschneiden, Kopieren und Einfügen</title>
        <para>Sie können den Text eines Untertitels ausschneiden, kopieren und einfügen.</para>
        <procedure><title>Text kopieren</title><step><para><keycap>Strg</keycap>+ <keycap>C</keycap></para></step></procedure>
        <procedure><title>Text ausschneiden</title><step><para><keycap>Strg</keycap>+ <keycap>X</keycap></para></step></procedure>
        <procedure><title>Text einfügen</title><step><para><keycap>Strg</keycap>+ <keycap>V</keycap></para></step></procedure>
      </sect2>
</sect1>
    <!-- ============= find/replace ================================== -->
<!--    <sect1 id="gs-findreplace">
      <title>Finding and Replacing</title>
      <sect2 id="gs-fr-finding">
        <title>Finding</title>
        <para>Questions and general discussions should be posted on the <ulink url="http://sourceforge.net/forum/?group_id=129996" type="http">Forum</ulink> (no registration needed).</para>
      </sect2>
            <sect2 id="gs-fr-replacing">
        <title>Replacing</title>
        <para>You might get a quick response by asking the <ulink url="http://lists.sourceforge.net/lists/listinfo/gnome-subtitles-general" type="http">Mailing List</ulink> (<ulink url="http://sourceforge.net/mailarchive/forum.php?forum=gnome-subtitles-general" type="http">archives</ulink> are available).</para>
      </sect2>
      <sect2 id="gs-fr-options">
        <title>Find and Replace Options</title>
        <para>Questions and general discussions should be posted on the <ulink url="http://sourceforge.net/forum/?group_id=129996" type="http">Forum</ulink> (no registration needed).</para>
      </sect2>
      <sect2 id="gs-regex">
        <title>Using Regular Expressions</title>
        <para>Bugs and Feature Requests can be reported at the official <ulink url="http://bugzilla.gnome.org/browse.cgi?product=gnome-subtitles" type="http">Gnome Bugzilla</ulink>.</para>
      </sect2>
    </sect1>
-->
    <!-- ============= Timings ================================== -->
    <sect1 id="gs-timings">
      <title>Bearbeiten von Untertiteln</title>
      <sect2 id="gs-timings-settingview">
        <title>Festlegen der Bearbeitungsbasis für Untertitel</title>
        <para>In <application>Gnome Subtitles</application> können Sie Untertitel entweder anhand von Zeiten (Minuten und Sekunden) oder anhand von Bildeinheiten bearbeiten. Vorgabe ist die Bearbeitung anhand von Zeiten.</para>
            <procedure><title>Verwendung von Bildern</title>
              <step><para lang="en">Go to <menuchoice>
              <guisubmenu>View</guisubmenu>
              <guimenuitem>Frames</guimenuitem>
            </menuchoice>. </para></step></procedure>
              <procedure><title>Verwendung von Zeiten</title>
              <step><para lang="en">Go to <menuchoice>
              <guisubmenu>View</guisubmenu>
              <guimenuitem>Times</guimenuitem>
            </menuchoice>. </para></step></procedure>
      </sect2>
      <sect2 id="gs-timings-Adjusting">
        <title>Synchronisation korrigieren</title>
           <procedure>
             <step><para>Falls Sie die Synchronisation nur für bestimmte Untertitel ändern wollen, wählen Sie die gewünschten Untertitel aus.</para></step>
              <step><para lang="en">Go to <menuchoice>
              <guisubmenu>Timings</guisubmenu>
              <guimenuitem>Adjust</guimenuitem>
            </menuchoice>. </para></step>
            <step><para>Wählen Sie den neuen Startzeitpunkt des ersten Untertitels und den Startzeitpunkt des letzten Untertitels aus.</para></step>
      </procedure>
      </sect2>
            <sect2 id="gs-timings-Shifitng">
        <title>Synchronisation verschieben</title>
        <para>Das Verschieben der Synchronisation ermöglicht Ihnen das Verschieben von einem oder mehreren Untertiteln nach vorn oder zurück um einen bestimmten Wert. Im Gegensatz zum Korrigieren der Synchronisation ändert das Verschieben der Synchronisation die Laufzeit der Untertitel nicht.</para>
              <procedure><title>Verschieben von Untertiteln</title>
                <step><para>Wählen Sie aus, welchen oder welche Untertitel Sie verschieben wollen. Falls Sie Sie die Position eines und/oder aller Untertitel vor oder nach einem bestimmten Untertitel ändern wollen, wählen Sie diesen Untertitel aus. Falls Sie mehrere Untertitel verschieben wollen, wählen Sie alle gewünschten Untertitel aus.</para></step>
              <step><para lang="en">Go to <menuchoice>
              <guisubmenu>Timings</guisubmenu>
              <guimenuitem>Shift</guimenuitem>
            </menuchoice>. </para></step>
            <step><para>Geben Sie Zeit- oder Bildwerte ein, um Ihre Untertitel anzupassen. Falls Sie ihre Untertitel zurück verschieben wollen, geben Sie einen negativen Wert ein, anderenfalls einen positiven Wert.</para></step>
            <step><para>Wählen Sie aus, auf welche Weise Sie Ihre Untertitel verschieben wollen.</para></step>
            <step><para>Klicken Sie auf <guilabel>Verschieben</guilabel>, um die Untertitel zu verschieben.</para></step>
      </procedure>
      </sect2>
      <sect2 id="gs-timings-inputfr">
        <title>Festlegen der Bildraten für Eingabe und Video</title>
        <para>Wenn Sie ein Video öffnen, setzt <application>Gnome Subtitles</application> dessen Bildrate nicht automatisch.</para>
        <para><application>Gnome Subtitles</application> bearbeitet Untertitel auf der Basis von Zeiten, unabhängig vom Ansichtsmodus. Daher ändert <application>Gnome Subtitles</application> die Bildpositionen, wenn Sie die Bildrate ändern, um die Änderungen auszugleichen. Die Laufzeit der Untertitel ändert sich dadurch nicht.</para>
        <caution><para>Wenn Ihr Video sich zur Anzeige der Untertitel an den Bildern orientiert (bei MicroDVD), dann könnte die Änderung der Bildrate dazu führen, dass die Synchronisation der Untertitel verlorengeht.</para></caution>
        <procedure><title>Video-Bildrate setzen</title>
              <step><para lang="en">Go to <menuchoice>
              <guisubmenu>Timings</guisubmenu>
              <guimenuitem>Video Frame Rate</guimenuitem>
            </menuchoice> and select the appropriate framerate.</para></step>
      </procedure>
      <procedure><title>Eingabe-Bildrate setzen</title>
              <step><para lang="en">Go to <menuchoice>
              <guisubmenu>Timings</guisubmenu>
              <guimenuitem>Input Frame Rate</guimenuitem>
            </menuchoice> and select the appropriate framerate.</para></step>
      </procedure>
      </sect2>
    </sect1>
     <!-- ============= Video ================================== -->
    <sect1 id="gs-video">
      <title>Arbeiten mit Videos</title>
      <sect2 id="gs-video-opening">
        <title>Öffnen und Schließen eines Videos</title>
        <procedure>
          <title>Öffnen eines Videos</title>
          <step><para lang="en">Go to <menuchoice>
              <guisubmenu>Video</guisubmenu>
              <guimenuitem>Open</guimenuitem>
            </menuchoice>
          </para></step>
        </procedure>
        <procedure>
          <title>Schließen eines Videos</title>
          <step><para lang="en">Go to <menuchoice>
              <guisubmenu>Video</guisubmenu>
              <guimenuitem>Close</guimenuitem>
            </menuchoice>
          </para></step>
        </procedure>
      </sect2>
      <sect2 id="gs-video-playback">
        <title>Wiedergabe</title>
        <para><application>Gnome Subtitles</application> spielt das Video im Ansichtsfenster mit Ihren Untertiteln ab.</para>
      </sect2>
      <sect2 id="gs-video-seeking">
        <title>Die Auswahl durchsuchen</title>
        <para>Sie können zu einem Punkt im geöffneten Video springen, an dem ein Untertitel beginnt.</para>
        <procedure>
          <title>Nach einem Untertitel suchen</title>
          <step><para lang="en">Go to <menuchoice>
              <guisubmenu>Video</guisubmenu>
              <guimenuitem>Seek to Selection</guimenuitem>
            </menuchoice>
          </para></step>
        </procedure>
      </sect2>
      <sect2 id="gs-video-settingsubs">
        <title>Festlegen der Start- und Endzeitpunkte für Untertitel</title>
        <para>Sie können die Start- und Endzeitpunkte für Untertitel anhand des Zeitpunktes setzen, an dem die Wiedergabe eines geöffneten Videos unterbrochen wurde.</para>
        <procedure>
          <title>Festlegen des Startzeitpunktes für Untertitel</title>
          <step><para lang="en">Go to <menuchoice>
              <guisubmenu>Video</guisubmenu>
              <guimenuitem>Set Subtitle Start</guimenuitem>
            </menuchoice>
          </para></step>
        </procedure>
        <procedure>
          <title>Festlegen des Endzeitpunktes für Untertitel</title>
          <step><para lang="en">Go to <menuchoice>
              <guisubmenu>Video</guisubmenu>
              <guimenuitem>Set Subtitle End</guimenuitem>
            </menuchoice>
          </para></step>
        </procedure>
      </sect2>
    </sect1>
   <!-- ============= Support ================================== -->
    <sect1 id="gs-support">
      <title>Wo Sie zusätzliche Hilfe bekommen</title>
      <sect2 id="gs-support-forums">
        <title>Foren</title>
        <para>Für Fragen und allgemeine Diskussionen besuchen Sie bitte das <ulink url="http://sourceforge.net/forum/?group_id=129996" type="http">Forum</ulink> (keine Registrierung erforderlich).</para>
      </sect2>
            <sect2 id="gs-support-mailinglist">
        <title>Mailingliste</title>
        <para>Möglicherweise erhalten Sie bei einer Anfrage in der <ulink url="http://lists.sourceforge.net/lists/listinfo/gnome-subtitles-general" type="http">Mailingliste</ulink> eine schnellere Antwort. Ein Archiv dieser Mailingliste finden Sie <ulink url="http://sourceforge.net/mailarchive/forum.php?forum_name=gnome-subtitles-general" type="http">hier</ulink>.</para>
      </sect2>
      <sect2 id="gs-support-bugzilla">
        <title>Fehler und Funktionsmerkmale</title>
        <para lang="en">Bugs and Feature Requests can be reported in the <ulink url="https://gitlab.gnome.org/GNOME/gnome-subtitles/issues/" type="http">GNOME issue tracker</ulink>.</para>
      </sect2>
    </sect1>
  </article>
