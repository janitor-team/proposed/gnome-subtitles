<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE article PUBLIC "-//OASIS//DTD DocBook XML V4.1.2//EN" "http://www.oasis-open.org/docbook/xml/4.1.2/docbookx.dtd" [
<!ENTITY legal SYSTEM "legal.xml">
<!ENTITY appversion "0.4">
<!ENTITY manrevision "1.0">
<!ENTITY date "May 2007">
<!ENTITY app "<application>Gnome Subtitles</application>">
]>
<!--
    (Do not remove this comment block.)
    Maintained by the GNOME Documentation Project
    http://developer.gnome.org/projects/gdp
    Template version: 2.0 beta
    Template last modified Apr 11, 2002
    -->
<!-- =============Document Header ============================= -->
<article id="index" lang="cs">
  <!-- please do not change the id; for translations, change lang to -->
  <!-- appropriate code -->
  <articleinfo>
    <title>Příručka V1.0 k aplikaci <application>Titulky GNOME</application></title>
    
    <copyright><year>2007</year> <holder>Erin Bloom</holder></copyright>
    <abstract role="description"><para>Na aplikaci <application>Titulky GNOME</application> pracují</para>
    </abstract>
    <!-- An address can be added to the publisher information.  If a role is
          not specified, the publisher/author is the same for all versions of the
          document.  -->
    <publisher>
      <publishername>Dokumentační projekt GNOME</publishername>
    </publisher>
    
      <legalnotice id="legalnotice">
	<para>Je povoleno kopírovat, šířit a/nebo upravovat tento dokument za podmínek GNU Free Documentation License, verze 1.1 nebo jakékoli další verze vydané nadací Free Software Foundation; bez neměnných oddílů, bez textů předních desek a bez textů zadních desek. Kopie této licence je zahrnuta v oddílu jménem <ulink type="help" url="ghelp:fdl">GNU Free Documentation License</ulink> nebo v souboru COPYING-DOCS dodávaném s touto příručkou.</para>
         <para>Tato příručka je součástí kolekce příruček GNOME, distribuovaných pod licencí GNU FDL. Pokud chcete tento dokument šířit odděleně od kolekce, musíte přiložit kopii licence dle popisu v sekci 6 dané licence.</para>

	<para>Mnoho názvů použitých firmami k zviditelnění produktů nebo služeb jsou ochranné známky. Na místech, kde jsou tyto názvy v dokumentaci použity a členové Dokumentačního projektu GNOME jsou si vědomi skutečnosti, že se jedná o ochrannou známku, je takovýto název psán velkými písmeny celý nebo s velkým písmenem na začátku.</para>

	<para>DOKUMENT A JEHO UPRAVENÉ VERZE JSOU ŠÍŘENY V SOULADU SE ZNĚNÍM LICENCE GNU FREE DOCUMENTATION LICENSE S NÁSLEDUJÍCÍM USTANOVENÍM: <orderedlist>
		<listitem>
		  <para>DOKUMENT JE POSKYTOVÁN V PODOBĚ „JAK JE“ BEZ ZÁRUKY V JAKÉKOLIV PODOBĚ, NEPOSKYTUJÍ SE ANI ODVOZENÉ ZÁRUKY, ZÁRUKY, ŽE DOKUMENT, NEBO JEHO UPRAVENÁ VERZE, JE BEZCHYBNÝ NEBO ZÁRUKY PRODEJNOSTI, VHODNOSTI PRO URČITÝ ÚČEL NEBO NEPORUŠENOSTI. RIZIKO NEKVALITY, NEPŘESNOSTI A ŠPATNÉHO PROVEDENÍ DOKUMENTU, NEBO JEHO UPRAVENÉ VERZE, LEŽÍ NA VÁS. POKUD KVŮLI TOMUTO DOKUMENTU, NEBO JEHO UPRAVENÉ VERZI, NASTANE PROBLÉM, VY (NIKOLIV PŮVODNÍ AUTOR NEBO JAKÝKOLIV PŘISPĚVATEL) PŘEBÍRÁTE JAKÉKOLIV NÁKLADY ZA NUTNÉ ÚPRAVY, OPRAVY ČI SLUŽBY. TOTO PROHLÁŠENÍ O ZÁRUCE PŘEDSTAVUJE ZÁKLADNÍ SOUČÁST TÉTO LICENCE. BEZ TOHOTO PROHLÁŠENÍ NENÍ, PODLE TÉTO DOHODY, POVOLENO UŽÍVÁNÍ ANI ÚPRAVY TOHOTO DOKUMENTU; DÁLE</para>
		</listitem>
		<listitem>
		  <para>ZA ŽÁDNÝCH OKOLNOSTÍ A ŽÁDNÝCH PRÁVNÍCH PŘEDPOKLADŮ, AŤ SE JEDNÁ O PŘEČIN (VČETNĚ NEDBALOSTNÍCH), SMLOUVU NEBO JINÉ, NENÍ AUTOR, PŮVODNÍ PISATEL, KTERÝKOLIV PŘISPĚVATEL NEBO KTERÝKOLIV DISTRIBUTOR TOHOTO DOKUMENTU NEBO UPRAVENÉ VERZE DOKUMENTU NEBO KTERÝKOLIV DODAVATEL NĚKTERÉ Z TĚCHTO STRAN ODPOVĚDNÝ NĚJAKÉ OSOBĚ ZA PŘÍMÉ, NEPŘÍMÉ, SPECIÁLNÍ, NAHODILÉ NEBO NÁSLEDNÉ ŠKODY JAKÉHOKOLIV CHARAKTERU, VČETNĚ, ALE NEJEN, ZA POŠKOZENÍ ZE ZTRÁTY DOBRÉHO JMÉNA, PŘERUŠENÍ PRÁCE, PORUCHY NEBO NESPRÁVNÉ FUNKCE POČÍTAČE NEBO JINÉHO A VŠECH DALŠÍCH ŠKOD NEBO ZTRÁT VYVSTÁVAJÍCÍCH Z NEBO VZTAHUJÍCÍCH SE K POUŽÍVÁNÍ TOHOTO DOKUMENTU NEBO UPRAVENÝCH VERZÍ DOKUMENTU, I KDYŽ BY TAKOVÁTO STRANA BYLA INFORMOVANÁ O MOŽNOSTI TAKOVÉHOTO POŠKOZENÍ.</para>
		</listitem>
	  </orderedlist></para>
  </legalnotice>


    
    <authorgroup>
      <author role="maintainer"><firstname>Erin</firstname> <surname>Bloom</surname> <affiliation><orgname>Dokumentační projekt GNOME</orgname> <address><email>doc-writer2@gnome.org</email></address></affiliation></author>

<!-- This is appropriate place for other contributors: translators,
      maintainers,  etc. Commented out by default.
      
      <othercredit role="translator">
        <firstname>Latin</firstname>
        <surname>Translator 1</surname>
        <affiliation>
          <orgname>Latin Translation Team</orgname>
          <address> <email>translator@gnome.org</email> </address>
        </affiliation>
        <contrib>Latin translation</contrib>
      </othercredit>
      -->
    </authorgroup>
    
  
  <!-- According to GNU FDL, revision history is mandatory if you are -->
  <!-- modifying/reusing someone else's document.  If not, you can omit it. -->
  <!-- Remember to remove the &manrevision; entity from the revision entries other -->
  <!-- than the current revision. -->
  <!-- The revision numbering system for GNOME manuals is as follows: -->
  <!-- * the revision number consists of two components -->
  <!-- * the first component of the revision number reflects the release version of the GNOME desktop. -->
  <!-- * the second component of the revision number is a decimal unit that is incremented with each revision of the manual. -->
  <!-- For example, if the GNOME desktop release is V2.x, the first version of the manual that -->
  <!-- is written in that desktop timeframe is V2.0, the second version of the manual is V2.1, etc. -->
  <!-- When the desktop release version changes to V3.x, the revision number of the manual changes -->
  <!-- to V3.0, and so on. -->
  <revhistory>
    <revision><revnumber>Příručka V1.0 k aplikaci gnome-subtitles</revnumber> <date>May 2007</date> <revdescription>
        <para role="author">Erin Bloom <email>docwriter2@gnome.org</email></para>
        <para role="publisher">Dokumentační projekt GNOME</para>
      </revdescription></revision>
  </revhistory>
  
  <releaseinfo>Tato příručka popisuje aplikaci gnome-subtitles ve verzi 0.4</releaseinfo>
  <legalnotice>
    <title>Ohlasy</title>
    <para>Pokud chcete oznámit chybu nebo navrhnout vylepšení vztahující se k aplikaci <application>Titulky GNOME</application> nebo této příručce, postupujte dle instrukcí na <ulink url="help:gnome-feedback" type="help">stránce ohlasy GNOME</ulink>.</para>
    <!-- Translators may also add here feedback address for translations -->
  </legalnotice>
</articleinfo>
<!-- Index Section -->
<indexterm zone="index"><primary>Titulky GNOME</primary></indexterm>
<indexterm zone="index"><primary>gnomesubtitles</primary></indexterm>

<!-- ============= Document Body ============================= -->
<!-- ============= Introduction ============================== -->
<!-- Use the Introduction section to give a brief overview of what
    the application is and what it does. -->
<sect1 id="gs-introduction">
  <title>Úvod</title>
  <para><application>Titulky GNOME</application> je editor titulků pro pracovní prostředí GNOME. Aplikace podporuje většinu textově založených formátů titulků a umožňuje úpravu titulků a jejich převod a synchronizaci. Výčet funkcí následuje.</para>
  <itemizedlist>
    <listitem>
      <para>Formáty titulků:</para> 
      <itemizedlist> 
        <listitem><para>Advanced Sub Station Alpha</para></listitem> 
        <listitem><para>MicroDVD</para></listitem> 
        <listitem><para>MPlayer</para></listitem> 
        <listitem><para>Mplayer 2</para></listitem> 
        <listitem><para>MPSub</para></listitem> 
        <listitem><para>SubRip</para></listitem> 
        <listitem><para>Sub Station Alpha</para></listitem> 
        <listitem><para>SubViewer 1.0</para></listitem> 
        <listitem><para>SubViewer 2.0</para></listitem> 
      </itemizedlist> 
    </listitem> 
    <listitem><para>Zabudovaný náhled videa</para> 
      <itemizedlist>
        <listitem><para>Jako výkonná část se používá mplayer</para></listitem>
        <listitem><para>Zobrazení délky videa a současné pozice</para></listitem>
        <listitem><para>Nastavení časování titulků založené na pozici ve videu</para></listitem>
        <listitem><para>Automatický výběr videa při otevírání titulků</para></listitem>
      </itemizedlist>
    </listitem> 
    <listitem>
      <para>Uživatelské rozhraní</para>
      <itemizedlist>
        <listitem><para>WYSIWYG (What You See Is What You Get) - Zobrazení výsledku naživo</para></listitem>
        <listitem><para>Úprava hlaviček titulků</para></listitem>
        <listitem><para>Hledání a nahrazování, včetně podpory regulárních výrazů</para></listitem>
        <listitem><para>Funkce zpět/znovu</para></listitem>
      </itemizedlist>
    </listitem>
    <listitem>
      <para>Operace s časováním</para>
      <itemizedlist>
        <listitem><para>Automatické přizpůsobení časování založené na 2 správných časech/bodech synchronizace</para></listitem>
        <listitem><para>Posun titulků o zadanou dobu (která může vycházet z údajů videa)</para></listitem>	
        <listitem><para>Převod mezi snímkovými frekvencemi</para></listitem>
        <listitem><para>Úprava časování a snímkování</para></listitem>
      </itemizedlist>
    </listitem>
    <listitem><para>Ostatní funkce</para>
      <itemizedlist>
        <listitem><para>Automatické zjišťování kódování znaků a formátu titulků (při otevírání)</para></listitem>
        <listitem><para>Volba více kódování znaků</para></listitem>
        <listitem><para>Volnější způsob čtení titulků, který umožňuje načíst i poškozené titulky</para></listitem>
      </itemizedlist>
    </listitem>
  </itemizedlist>
</sect1>

<!-- =========== Getting Started ============================== -->
<!-- Use the Getting Started section to describe the steps required
    to start the application and to describe the user interface components
  of the application. If there is other information that it is important
    for readers to know before they start using the application, you should
    also include this information here. 
    If the information about how to get started is very short, you can 
    include it in the Introduction and omit this section. -->

<sect1 id="gs-getting-started">
  <title>Začínáme</title> 
  
  <sect2 id="gs-start">
    <title>Jak spustit aplikaci <application>Titulky GNOME</application></title>
    <para>Aplikaci <application>Titulky GNOME</application> můžete spustit následujícími způsoby:</para>
    <variablelist>
      <varlistentry>
        <term>Nabídka <guimenu>Aplikace</guimenu></term>
        <listitem>
          <para>Zvolte <menuchoice><guisubmenu>Zvuk a video</guisubmenu><guimenuitem>Titulky GNOME</guimenuitem></menuchoice>.</para>
        </listitem>
      </varlistentry>
      <varlistentry>
        <term>Příkazový řádek</term>
        <listitem>
          <para>Napište <command>gnome-subtitles</command> <replaceable>název_souboru</replaceable> a zmáčkněte <keycap>Enter</keycap>.</para>
          <para>Argument <replaceable>název_souboru</replaceable> je volitelný. Pokud je zadán, aplikace při spuštění tento soubor otevře.</para>
        </listitem>
      </varlistentry>
    </variablelist>
  </sect2>
</sect1>
<sect1 id="gs-working-with-files">
  <title>Práce se soubory</title>
  <sect2 id="gs-creating-new-doc">
    <title>Vytvoření nového dokumentu s titulky</title>
    <procedure>
      <title>Vytvoření nového dokumentu s titulky</title>
      <step performance="required">
      <para>Přejděte na <menuchoice><guisubmenu>Soubor</guisubmenu> <guimenuitem>Nový</guimenuitem></menuchoice>.</para>
      <para>V editoru by se měl otevřít nový soubor</para>
      </step>
      </procedure>
  </sect2>
    <sect2 id="gs-opendingfile">
    <title>Otevření souboru</title>
      <procedure>
      <title>Otevření souboru</title>
      <step performance="required">
      <para>Přejděte na <menuchoice><guisubmenu>Soubor</guisubmenu> <guimenuitem>Otevřít</guimenuitem></menuchoice>.</para>
      <para>Mělo by se otevřít okno Otevření souboru</para>
      </step>
      <step performance="required">
      <para>Vyberte soubor s titulky, který si přejete otevřít</para>
      <substeps>
      <step performance="optional">
      <para>Pokud chcete určit kódování znaků v souboru, zvolte jej v seznamu Kódování znaků. Jestliže žádné neurčíte, bude automaticky zjištěno.</para>
      </step>
      <step performance="optional">
      <para>Pokud chcete zvolit video, které se má ihned otevřít, zvolte jej v seznamu Video k otevření.</para>
      </step>
      </substeps>
      </step>
      </procedure>
      <note><para>Když vybíráte video, můžete volit pouze z videí v aktuální složce. Pokud se video, které chcete otevřít, nenachází v té stejné složce, jako soubor s titulky, můžete video otevřít dodatečně až po otevření souboru s titulky.</para>
      </note>
  </sect2>
    <sect2 id="gs-saving-file">
    <title>Uložení souboru</title>
      <procedure>
      <title>Uložení souboru</title>
      <para>Soubor můžete uložit buď normálně nebo použít Uložit jako k nastavení jiných voleb.</para>
      <step performance="required">
      <para>Přejděte na <menuchoice><guisubmenu>Soubor</guisubmenu> <guimenuitem>Uložit</guimenuitem></menuchoice>.</para>
      <para>Mělo by se otevřít okno Otevření souboru</para>
      </step>
    </procedure>
    <note><para>V případě, že ukládáte nový soubor, objeví se okno Uložení jako</para></note>
    <procedure>
      <title>Uložení jako nového souboru</title>
      <step performance="required">
      <para>Přejděte na <menuchoice><guisubmenu>Soubor</guisubmenu><guimenuitem>Uložit jako</guimenuitem></menuchoice>.</para>
      <para>Mělo by se otevřít okno Uložení jako</para>
      </step>
      <step performance="required"><para>Zadejte nový název pro svůj soubor s titulky.</para></step>
      <step performance="optional"><para>Pokud si přejete soubor uložit na jiné místo, vyberte toto nové místo v prohlížeči souborů.</para></step>
      <step performance="optional"><para>Pokud si přejete soubor uložit v jiné formátu než formátu uvedeném v seznamu Formát titulků, zvolte v tomto seznamu jiný formát.</para></step>
      <step performance="optional"><para>Pokud si přejete uložit soubor v jiném kódování znaků, než je vypsané výchozí kódování, změňte kódování v seznamu kódování znaků.</para></step>
      <step performance="required"><para>Klikněte na Uložit</para></step>
    </procedure>
  </sect2>
  
    <sect2 id="gs-select-char-coding">
    <title>Výběr kódování znaků</title>
    <para>Pokud ve svém souboru s titulky používáte speciální znaky, ujistěte se, že je váš soubor uložen s kódováním, které tyto znaky podporuje.</para>
    <caution><para>Uložení souboru v kódování znaků, které nepodporuje speciální znaky, způsobí ztrátu dat se znaky. To se stává, když zkusíte uložit vícebajtové znaky v jednoznakovém kódování.</para></caution>
  </sect2>
    <sect2 id="gs-editing-headers">
    <title>Úprava hlaviček titulků</title>

        <procedure>
  <para>Některé formáty titulků mají v souboru hlavičku, která obsahuje informace o souboru. Pole takovéto hlavičky můžete v aplikaci <application>Titulky GNOME</application> upravovat následovně:</para>
      <step performance="required">
      <para>Přejděte na <menuchoice><guisubmenu>Soubor</guisubmenu> <guimenuitem>Hlavičky</guimenuitem></menuchoice>.</para>
      <para>Otevře se okno Hlavičky. To obsahuje 4 karty.</para>
      </step>
      <step performance="required"> <para>Vyberte kartu, která odpovídá formátu vašeho souboru s titulky.</para></step>
      <step performance="required"><para>Vyplňte příslušná pole.</para></step>
      <step performance="required"><para>Po dokončení zadávání dat hlavičky klikněte na OK.</para></step>
    </procedure>
  </sect2>
</sect1>
    <!-- ============= Working with Subtitles ================================== -->
<sect1 id="gs-working-with-subs">
<title>Práce s titulky</title>
<note><para>Tato nápověda uvádí k provádění operací klávesové zkratky. Všechny uvedené úkoly mohou být ale provedeny i za pomocí nabídky Upravit, namísto používání klávesových zkratek.</para></note>
      <sect2 id="gs-wws-adding">
        <title>Přidání titulku</title>
        <para>Nové titulky jsou přidávány buď před a nebo za současný titulek.</para>
        <procedure>
          <title>Jak přidat nový titulek za současný titulek</title>
      <step performance="required">
      <para>Zmáčkněte <keycap>Insert</keycap> nebo <keycap>Ctrl</keycap> + <keycap>Enter</keycap></para>
      </step>
    </procedure>
            <procedure>
          <title>Jak přidat nový titulek před současný titulek</title>
      <step performance="required">
      <para>Zmáčkněte <keycap>Shift</keycap> + <keycap>Ctrl</keycap> + <keycap>Enter</keycap></para>
      </step>
    </procedure>
      </sect2>     
      <sect2 id="gs-wws-moving">
        <title>Přesun mezi titulky</title>
        <para>K výběru titulků můžete používat buďto myš tak, že kliknete na titulek, a nebo, pokud se kurzor nachází v editačním okně, můžete použít klávesové zkratky pro přesun mezi titulky.</para>
        <procedure>
          <title>Jak přejít na následující titulek</title>
      <step performance="required">
      <para>Zmáčkněte <keycap>Ctrl</keycap> + <keycap>Page Down</keycap></para>
      </step>
    </procedure>
            <procedure>
          <title>Jak přejít na předchozí titulek</title>
      <step performance="required">
      <para>Zmáčkněte <keycap>Control</keycap> + <keycap>Page Up</keycap></para>
      </step>
    </procedure>
      </sect2>     
      <sect2 id="gs-wws-removing">
        <title>Odstranění titulku či titulků</title>
      <procedure><step><para>Zmáčkněte <keycap>Delete</keycap></para></step></procedure>
      </sect2>      
      <sect2 id="gs-wws-batch">
        <title>Práce s více titulky</title>
        <para>Někdy můžete chtít označit více titulků naráz. K označení ucelené řady použijte <keycap>Shift</keycap> a k označení jednotlivých titulků <keycap>Ctrl</keycap>.</para>
        <para>Pro výběr všech titulků zmáčkněte <keycap>Control</keycap> + <keycap>A</keycap></para>
      </sect2>      
      <sect2 id="gs-wws-editing">
        <title>Úprava textu</title>
        <para>Jak upravit text v titulku</para>
          <procedure>
            <step><para>Vyberte titulek.</para></step>
            <step><para>Klikněte na editační okno, aby se do něj přesunul kurzor.</para></step>
          </procedure>
      </sect2>
      <sect2 id="gs-wws-format">
        <title>Formát textu</title>
        <note><para>Typ formátu (tučné, kurzíva, podtržené) je možné použít pouze na řádek jako celek. Pokud byste chtěli speciálně naformátovat jen určité znaky, musíte v současnosti jedině upravit soubor s titulky v textovém editoru.</para></note>
      </sect2>
      <sect2 id="gs-wws-oops">
        <title>Vracení a opětovné provádění</title>
        <procedure><title>Jak vrátit akci</title><step><para>Zmáčkněte <keycap>Control</keycap>+<keycap>Z</keycap></para></step></procedure>
        <procedure><title>Jak znovu provést akci</title><step><para>Zmáčkněte <keycap>Ctrl</keycap> + <keycap>Y</keycap> nebo <keycap>Ctrl</keycap> + <keycap>Shift</keycap> + <keycap>Z</keycap></para></step></procedure>
      </sect2>      
      <sect2 id="gs-wws-ccp">
        <title>Vyjímání, kopírování a vkládání</title>
        <para>Můžete vyjímat, kopírovat a vkládat text v titulcích.</para>
        <procedure><title>Jak kopírovat text</title><step><para><keycap>Control</keycap> + <keycap>C</keycap></para></step></procedure>
        <procedure><title>Jak vyjmout text</title><step><para><keycap>Control</keycap> + <keycap>X</keycap></para></step></procedure>
        <procedure><title>Jak vložit text</title><step><para><keycap>Control</keycap> + <keycap>V</keycap></para></step></procedure>
      </sect2>
</sect1>
    <!-- ============= find/replace ================================== -->
<!--    <sect1 id="gs-findreplace">
      <title>Finding and Replacing</title>
      <sect2 id="gs-fr-finding">
        <title>Finding</title>
        <para>Questions and general discussions should be posted on the <ulink url="http://sourceforge.net/forum/?group_id=129996" type="http">Forum</ulink> (no registration needed).</para>
      </sect2>
            <sect2 id="gs-fr-replacing">
        <title>Replacing</title>
        <para>You might get a quick response by asking the <ulink url="http://lists.sourceforge.net/lists/listinfo/gnome-subtitles-general" type="http">Mailing List</ulink> (<ulink url="http://sourceforge.net/mailarchive/forum.php?forum=gnome-subtitles-general" type="http">archives</ulink> are available).</para>
      </sect2>
      <sect2 id="gs-fr-options">
        <title>Find and Replace Options</title>
        <para>Questions and general discussions should be posted on the <ulink url="http://sourceforge.net/forum/?group_id=129996" type="http">Forum</ulink> (no registration needed).</para>
      </sect2>
      <sect2 id="gs-regex">
        <title>Using Regular Expressions</title>
        <para>Bugs and Feature Requests can be reported at the official <ulink url="http://bugzilla.gnome.org/browse.cgi?product=gnome-subtitles" type="http">Gnome Bugzilla</ulink>.</para>
      </sect2>
    </sect1>
-->
    <!-- ============= Timings ================================== -->
    <sect1 id="gs-timings">
      <title>Zacházení s titulky</title>
      <sect2 id="gs-timings-settingview">
        <title>Nastavení jednotek titulků</title>
        <para>V aplikaci <application>Titulky GNOME</application> můžete nastavovat a přizpůsobovat titulky v časových jednotkách (minuty a sekundy) nebo v počtech snímků. Výchozí jednotkou jsou časové.</para>
            <procedure><title>Jak použít jako jednotky snímky</title>
              <step><para>Přejděte na <menuchoice><guisubmenu>Zobrazit</guisubmenu> <guimenuitem>Snímky</guimenuitem></menuchoice>.</para></step></procedure>
              <procedure><title>Jak použít jako jednotky čas</title>
              <step><para>Přejděte na <menuchoice><guisubmenu>Zobrazit</guisubmenu> <guimenuitem>Časy</guimenuitem></menuchoice>.</para></step></procedure>
      </sect2>
      <sect2 id="gs-timings-Adjusting">
        <title>Přizpůsobení časování</title>
           <procedure>
             <step><para>Pokud chcete časování přizpůsobit jen pro některé titulky, tak tyto titulky vyberte .</para></step>
              <step><para>Přejděte na <menuchoice><guisubmenu>Časování</guisubmenu> <guimenuitem>Přizpůsobit</guimenuitem></menuchoice>.</para></step>
            <step><para>Zvolte nový počáteční čas prvního titulku a nový počáteční čas posledního titulku.</para></step>
      </procedure>
      </sect2>
            <sect2 id="gs-timings-Shifitng">
        <title>Posun časování</title>
        <para>Posun časování vám umožňuje posunout jeden nebo více titulků vpřed nebo zpátky o zadanou hodnotu. Na rozdíl od funkce Přizpůsobení, posun nemá vliv na dobu, po kterou trvá zobrazení titulku.</para>
              <procedure><title>Jak použít posun titulků</title>
                <step><para>Vyberte titulek či titulky, které chcete posunout. Pokud chcete posunout čas/snímky jednoho titulku a/nebo všech titulků před a nebo za určitým titulkem, vyberte tento titulek. Pokud chcete posunou více než jeden konkrétní titulek, vyberte všechny titulky, které chcete posouvat.</para></step>
              <step><para>Přejděte na <menuchoice><guisubmenu>Časování</guisubmenu> <guimenuitem>Posun</guimenuitem></menuchoice>.</para></step>
            <step><para>Zadejte množství času (nebo snímků, pokud používáte jako jednotku snímky) o kolik chcete snímky přizpůsobit. Pokud chcete titulky posunout zpět, zadejte zápornou hodnotu, v opačném případě kladnou.</para></step>
            <step><para>Vyberte, jak chcete své titulky posunout.</para></step>
            <step><para>Klikněte na Posunout, aby se titulky posunuly.</para></step>
      </procedure>
      </sect2>
      <sect2 id="gs-timings-inputfr">
        <title>Nastavení snímkové frekvence vstupu a videa</title>
        <para>Když otevíráte video, aplikace <application>Titulky GNOME</application> nenastaví snímkovou frekvenci videa automaticky.</para>
        <para>Aplikace <application>Titulky GNOME</application> zachází s titulky na základě času, bez ohledu na režim zobrazení. Z toho důvodu, když změníte snímkovou frekvenci videa, přizpůsobí aplikace <application>Titulky GNOME</application> snímky, aby vykompenzovala změnu. Doba zobrazení titulků zůstává stejná.</para>
        <caution><para>V případě, že formát titulků používá snímky k určení, kdy se má titulek zobrazit (MicroDVD), může změna snímkové frekvence způsobit rozjetí synchronizace vašich titulků.</para></caution>
        <procedure><title>Jak nastavit snímkovou frekvenci videa</title>
              <step><para>Přejděte na <menuchoice><guisubmenu>Časování</guisubmenu> <guimenuitem>Snímková frekvence videa</guimenuitem></menuchoice> a vyberte příslušnou snímkovou frekvenci.</para></step>
      </procedure>
      <procedure><title>Jak nastavit snímkovou frekvenci vstupu</title>
              <step><para>Přejděte na <menuchoice><guisubmenu>Časování</guisubmenu> <guimenuitem>Snímková frekvence vstupu</guimenuitem></menuchoice> a vyberte příslušnou snímkovou rychlost.</para></step>
      </procedure>
      </sect2>
    </sect1>
     <!-- ============= Video ================================== -->
    <sect1 id="gs-video">
      <title>Práce s videi</title>
      <sect2 id="gs-video-opening">
        <title>Otevření a zavření videa</title>
        <procedure>
          <title>Jak otevřít video</title>
          <step><para>Přejděte na <menuchoice><guisubmenu>Video</guisubmenu> <guimenuitem>Otevřít</guimenuitem></menuchoice></para></step>
        </procedure>
        <procedure>
          <title>Jak zavřít video</title>
          <step><para>Přejděte na <menuchoice><guisubmenu>Video</guisubmenu> <guimenuitem>Zavřít</guimenuitem></menuchoice></para></step>
        </procedure>
      </sect2>
      <sect2 id="gs-video-playback">
        <title>Přehrávání</title>
        <para>Aplikace <application>Titulky GNOME</application> přehrává video v zobrazovacím okně s vašimi titulky.</para>
      </sect2>
      <sect2 id="gs-video-seeking">
        <title>Přesun na výběr</title>
        <para>V načteném videu můžete přejít na místo, kde začínají titulky</para>
        <procedure>
          <title>Jak se přesunout na titulek</title>
          <step><para>Přejděte na <menuchoice><guisubmenu>Video</guisubmenu> <guimenuitem>Přejít na výběr</guimenuitem></menuchoice></para></step>
        </procedure>
      </sect2>
      <sect2 id="gs-video-settingsubs">
        <title>Nastavení počáteční a konečné pozice titulku</title>
        <para>Můžete nastavit počáteční a konečný čas titulků na základě bodu, ve kterém je načtené video pozastavené.</para>
        <procedure>
          <title>Jak nastavit počáteční čas titulku</title>
          <step><para>Přejděte na <menuchoice><guisubmenu>Video</guisubmenu> <guimenuitem>Nastavit začátek titulku</guimenuitem></menuchoice></para></step>
        </procedure>
        <procedure>
          <title>Jak nastavit koncový čas titulku</title>
          <step><para>Přejděte na <menuchoice><guisubmenu>Video</guisubmenu> <guimenuitem>Nastavit konec titulku</guimenuitem></menuchoice></para></step>
        </procedure>
      </sect2>
    </sect1>
   <!-- ============= Support ================================== -->
    <sect1 id="gs-support">
      <title>Kde získat další podporu</title>
      <sect2 id="gs-support-forums">
        <title>Diskuzní fóra</title>
        <para>Dotazy a obecné diskuze by měly být zasílány do <ulink url="http://sourceforge.net/forum/?group_id=129996" type="http">diskuzního fóra</ulink> (registrace není zapotřebí).</para>
      </sect2>
            <sect2 id="gs-support-mailinglist">
        <title>Poštovní konference</title>
        <para>Rychlé odpovědi můžete získat po položení dotazu v <ulink url="http://lists.sourceforge.net/lists/listinfo/gnome-subtitles-general" type="http">poštovní konferenci</ulink> (k dispozici je i <ulink url="http://sourceforge.net/mailarchive/forum.php?forum_name=gnome-subtitles-general" type="http">archiv</ulink>).</para>
      </sect2>
      <sect2 id="gs-support-bugzilla">
        <title>Hlášení chyb a přání</title>
        <para lang="en">Bugs and Feature Requests can be reported in the <ulink url="https://gitlab.gnome.org/GNOME/gnome-subtitles/issues/" type="http">GNOME issue tracker</ulink>.</para>
      </sect2>
    </sect1>
  </article>
