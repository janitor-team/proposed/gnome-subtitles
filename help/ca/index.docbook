<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE article PUBLIC "-//OASIS//DTD DocBook XML V4.1.2//EN" "http://www.oasis-open.org/docbook/xml/4.1.2/docbookx.dtd" [
<!ENTITY legal SYSTEM "legal.xml">
<!ENTITY appversion "0.4">
<!ENTITY manrevision "1.0">
<!ENTITY date "May 2007">
<!ENTITY app "<application>Gnome Subtitles</application>">
]>
<!--
    (Do not remove this comment block.)
    Maintained by the GNOME Documentation Project
    http://developer.gnome.org/projects/gdp
    Template version: 2.0 beta
    Template last modified Apr 11, 2002
    -->
<!-- =============Document Header ============================= -->
<article id="index" lang="ca">
  <!-- please do not change the id; for translations, change lang to -->
  <!-- appropriate code -->
  <articleinfo>
    <title>Manual del <application>Gnome Subtitles</application> v1.0</title>
    
    <copyright lang="en">
      <year>2007</year>
      <holder>Erin Bloom</holder>
    </copyright>
    <abstract role="description"><para>Els treballadors del <application>Gnome Subtitles</application></para>
    </abstract>
    <!-- An address can be added to the publisher information.  If a role is
          not specified, the publisher/author is the same for all versions of the
          document.  -->
    <publisher>
      <publishername>Projecte de documentació del GNOME</publishername>
    </publisher>
    
      <legalnotice id="legalnotice">
	<para>Teniu permís per a copiar, distribuir i/o modificar aquest document, sota els termes de la Llicència de documentació lliure GNU (GFDL), versió 1.1 o qualsevol versió publicada posteriorment per la Free Software Foundation, sense seccions invariants, sense texts de portada i sense texts de contraportada. Podeu trobar una còpia de la GFDL en aquest <ulink type="help" url="ghelp:fdl">enllaç</ulink> o en el fitxer COPYING-DOCS distribuït amb aquest manual.</para>
         <para>Aquest manual forma part d'una col·lecció de manuals del GNOME distribuïts sota la GFDL. Si voleu distribuir aquest manual independentment de la col·lecció, podeu fer-ho afegint una còpia de la llicència al manual, tal com es descriu a la secció 6 de la llicència.</para>

	<para>Molts dels noms que les empreses utilitzen per a distingir els seus productes i serveis es consideren marques comercials. Quan aquests noms apareguin en qualsevol documentació del GNOME, si els membres del Projecte de documentació del GNOME han estat avisats pel que fa a les marques, els noms apareixeran en majúscules o amb les inicials en majúscules.</para>

	<para lang="en">
	  DOCUMENT AND MODIFIED VERSIONS OF THE DOCUMENT ARE PROVIDED
	  UNDER  THE TERMS OF THE GNU FREE DOCUMENTATION LICENSE
	  WITH THE FURTHER UNDERSTANDING THAT:

	  <orderedlist>
		<listitem>
		  <para lang="en">DOCUMENT IS PROVIDED ON AN "AS IS" BASIS,
                    WITHOUT WARRANTY OF ANY KIND, EITHER EXPRESSED OR
                    IMPLIED, INCLUDING, WITHOUT LIMITATION, WARRANTIES
                    THAT THE DOCUMENT OR MODIFIED VERSION OF THE
                    DOCUMENT IS FREE OF DEFECTS MERCHANTABLE, FIT FOR
                    A PARTICULAR PURPOSE OR NON-INFRINGING. THE ENTIRE
                    RISK AS TO THE QUALITY, ACCURACY, AND PERFORMANCE
                    OF THE DOCUMENT OR MODIFIED VERSION OF THE
                    DOCUMENT IS WITH YOU. SHOULD ANY DOCUMENT OR
                    MODIFIED VERSION PROVE DEFECTIVE IN ANY RESPECT,
                    YOU (NOT THE INITIAL WRITER, AUTHOR OR ANY
                    CONTRIBUTOR) ASSUME THE COST OF ANY NECESSARY
                    SERVICING, REPAIR OR CORRECTION. THIS DISCLAIMER
                    OF WARRANTY CONSTITUTES AN ESSENTIAL PART OF THIS
                    LICENSE. NO USE OF ANY DOCUMENT OR MODIFIED
                    VERSION OF THE DOCUMENT IS AUTHORIZED HEREUNDER
                    EXCEPT UNDER THIS DISCLAIMER; AND
		  </para>
		</listitem>
		<listitem>
		  <para lang="en">UNDER NO CIRCUMSTANCES AND UNDER NO LEGAL
                       THEORY, WHETHER IN TORT (INCLUDING NEGLIGENCE),
                       CONTRACT, OR OTHERWISE, SHALL THE AUTHOR,
                       INITIAL WRITER, ANY CONTRIBUTOR, OR ANY
                       DISTRIBUTOR OF THE DOCUMENT OR MODIFIED VERSION
                       OF THE DOCUMENT, OR ANY SUPPLIER OF ANY OF SUCH
                       PARTIES, BE LIABLE TO ANY PERSON FOR ANY
                       DIRECT, INDIRECT, SPECIAL, INCIDENTAL, OR
                       CONSEQUENTIAL DAMAGES OF ANY CHARACTER
                       INCLUDING, WITHOUT LIMITATION, DAMAGES FOR LOSS
                       OF GOODWILL, WORK STOPPAGE, COMPUTER FAILURE OR
                       MALFUNCTION, OR ANY AND ALL OTHER DAMAGES OR
                       LOSSES ARISING OUT OF OR RELATING TO USE OF THE
                       DOCUMENT AND MODIFIED VERSIONS OF THE DOCUMENT,
                       EVEN IF SUCH PARTY SHALL HAVE BEEN INFORMED OF
                       THE POSSIBILITY OF SUCH DAMAGES.
		  </para>
		</listitem>
	  </orderedlist>
	</para>
  </legalnotice>


    
    <authorgroup>
      <author role="maintainer" lang="en">
        <firstname>Erin</firstname>
        <surname>Bloom</surname>
        <affiliation>
          <orgname>GNOME Documentation Project</orgname>
          <address> <email>doc-writer2@gnome.org</email> </address>
        </affiliation>
      </author>

<!-- This is appropriate place for other contributors: translators,
      maintainers,  etc. Commented out by default.
      
      <othercredit role="translator">
        <firstname>Latin</firstname>
        <surname>Translator 1</surname>
        <affiliation>
          <orgname>Latin Translation Team</orgname>
          <address> <email>translator@gnome.org</email> </address>
        </affiliation>
        <contrib>Latin translation</contrib>
      </othercredit>
      -->
    </authorgroup>
    
  
  <!-- According to GNU FDL, revision history is mandatory if you are -->
  <!-- modifying/reusing someone else's document.  If not, you can omit it. -->
  <!-- Remember to remove the &manrevision; entity from the revision entries other -->
  <!-- than the current revision. -->
  <!-- The revision numbering system for GNOME manuals is as follows: -->
  <!-- * the revision number consists of two components -->
  <!-- * the first component of the revision number reflects the release version of the GNOME desktop. -->
  <!-- * the second component of the revision number is a decimal unit that is incremented with each revision of the manual. -->
  <!-- For example, if the GNOME desktop release is V2.x, the first version of the manual that -->
  <!-- is written in that desktop timeframe is V2.0, the second version of the manual is V2.1, etc. -->
  <!-- When the desktop release version changes to V3.x, the revision number of the manual changes -->
  <!-- to V3.0, and so on. -->
  <revhistory>
    <revision lang="en">
      <revnumber>gnome-subtitles Manual V1.0</revnumber>
      <date>May 2007</date>
      <revdescription>
        <para role="author" lang="en">Erin Bloom
          <email>docwriter2@gnome.org</email>
        </para>
        <para role="publisher" lang="en">GNOME Documentation Project</para>
      </revdescription>
    </revision>
  </revhistory>
  
  <releaseinfo>Aquest manual descriu l'ús de la versió 0.4 del gnome-subtitles.</releaseinfo>
  <legalnotice>
    <title>Comentaris</title>
    <para lang="en">To report a bug or make a suggestion regarding the <application>Gnome Subtitles</application> application or
      this manual, follow the directions in the <ulink url="help:gnome-feedback" type="help">GNOME Feedback Page</ulink>.
    </para>
    <!-- Translators may also add here feedback address for translations -->
  </legalnotice>
</articleinfo>
<!-- Index Section -->
<indexterm zone="index" lang="en">
  <primary>Gnome Subtitles</primary>
</indexterm>
<indexterm zone="index" lang="en">
  <primary>gnomesubtitles</primary>
</indexterm>

<!-- ============= Document Body ============================= -->
<!-- ============= Introduction ============================== -->
<!-- Use the Introduction section to give a brief overview of what
    the application is and what it does. -->
<sect1 id="gs-introduction">
  <title>Introducció</title>
  <para>El <application>Gnome Subtitles</application> és un editor de subtítols per a l'escriptori GNOME. El <application>Gnome Subtitles</application> admet els formats de subtítols basats amb text més comuns i permet l'edició, conversió i sincronització dels subtítols. Les seves característiques són les següents.</para>
  <itemizedlist>
    <listitem>
      <para>Formats dels subtítols:</para> 
      <itemizedlist> 
        <listitem><para>Advanced Sub Station Alpha</para></listitem> 
        <listitem><para>MicroDVD</para></listitem> 
        <listitem><para>MPlayer</para></listitem> 
        <listitem><para>Mplayer 2</para></listitem> 
        <listitem><para>MPSub</para></listitem> 
        <listitem><para>SubRip</para></listitem> 
        <listitem><para>Sub Station Alpha</para></listitem> 
        <listitem><para>SubViewer 1.0</para></listitem> 
        <listitem><para>SubViewer 2.0</para></listitem> 
      </itemizedlist> 
    </listitem> 
    <listitem><para>Previsualització del vídeo integrada</para> 
      <itemizedlist>
        <listitem><para>Utilitza el mplayer com a rerefons</para></listitem>
        <listitem><para>Visualització de la longitud del vídeo i de la posició actual</para></listitem>
        <listitem><para>Estableix la temporització del subtítol utilitzant la posició del vídeo</para></listitem>
        <listitem><para>Selecciona automàticament un vídeo a l'obrir els subtítols</para></listitem>
      </itemizedlist>
    </listitem> 
    <listitem>
      <para>Interfície d'usuari</para>
      <itemizedlist>
        <listitem><para>WYSIWYG - El què veus és el que obtens (What you see is what you get)</para></listitem>
        <listitem><para>Edita les capçaleres dels subtítols</para></listitem>
        <listitem><para>Cerca i reemplaça, admet l'ús d'expressions regulars</para></listitem>
        <listitem><para>Desfés/refés</para></listitem>
      </itemizedlist>
    </listitem>
    <listitem>
      <para>Operacions de temporització</para>
      <itemizedlist>
        <listitem><para>Ajusta automàticament la temporització utilitzant dos punts de temps/sincronització</para></listitem>
        <listitem><para>Desplaça els subtítols un retard especificat (que pot estar basat en el vídeo)</para></listitem>	
        <listitem><para>Converteix entre diferents freqüències dels fotogrames</para></listitem>
        <listitem><para>Edita els temps i els fotogrames</para></listitem>
      </itemizedlist>
    </listitem>
    <listitem><para>Altres característiques</para>
      <itemizedlist>
        <listitem><para>Detecta automàticament la codificació dels caràcters dels subtítols (a l'obrir)</para></listitem>
        <listitem><para>Permet diferents codificacions de caràcters</para></listitem>
        <listitem><para>Realça la lectura de subtítols, per a llegir subtítols que contenen errors</para></listitem>
      </itemizedlist>
    </listitem>
  </itemizedlist>
</sect1>

<!-- =========== Getting Started ============================== -->
<!-- Use the Getting Started section to describe the steps required
    to start the application and to describe the user interface components
  of the application. If there is other information that it is important
    for readers to know before they start using the application, you should
    also include this information here. 
    If the information about how to get started is very short, you can 
    include it in the Introduction and omit this section. -->

<sect1 id="gs-getting-started">
  <title>Primers passos</title> 
  
  <sect2 id="gs-start">
    <title>Iniciant el <application>Gnome Subtitles</application></title>
    <para>Podeu iniciar el <application><application>Gnome Subtitles</application></application> de les diferents maneres:</para>
    <variablelist>
      <varlistentry>
        <term>Menú d'<guimenu>Aplicacions</guimenu></term>
        <listitem>
          <para lang="en">Choose
            <menuchoice>
              <guisubmenu>Sound &amp; Video</guisubmenu>
              <guimenuitem>Gnome Subtitles</guimenuitem>
            </menuchoice>. </para>
        </listitem>
      </varlistentry>
      <varlistentry>
        <term>Línia d'ordres</term>
        <listitem>
          <para lang="en">Type <command>gnome-subtitles</command> <replaceable>filename</replaceable> and press <keycap>Enter</keycap>.</para>
          <para>L'argument <replaceable>nom del fitxer</replaceable> és opcional. Si s'introdueix, l'aplicació obrirà aquest fitxer en iniciar.</para>
        </listitem>
      </varlistentry>
    </variablelist>
  </sect2>
</sect1>
<sect1 id="gs-working-with-files">
  <title>Treballant amb fitxers</title>
  <sect2 id="gs-creating-new-doc">
    <title>Creant un nou document de subtítols</title>
    <procedure>
      <title>Creant un nou document de subtítols</title>
      <step performance="required">
      <para lang="en">
      Go to <menuchoice>
              <guisubmenu>File</guisubmenu>
              <guimenuitem>New</guimenuitem>
            </menuchoice>. </para>
      <para>Un fitxer nou s'hauria d'obrir a l'editor</para>
      </step>
      </procedure>
  </sect2>
    <sect2 id="gs-opendingfile">
    <title>Obrint un fitxer</title>
      <procedure>
      <title>Obrint un fitxer</title>
      <step performance="required">
      <para lang="en">
      Go to <menuchoice>
              <guisubmenu>File</guisubmenu>
              <guimenuitem>Open</guimenuitem>
            </menuchoice>. </para>
      <para>La finestra «Obre un fitxer» s'hauria d'obrir</para>
      </step>
      <step performance="required">
      <para>Seleccioneu el fitxer de subtítols que desitgeu obrir.</para>
      <substeps>
      <step performance="optional">
      <para lang="en">If you want to specify the file's character coding, choose one from the Character Coding list. If not specified, the caracter coding will be auto detected.</para>
      </step>
      <step performance="optional">
      <para>Si voleu triar un vídeo a obrir immediatament, trieu un vídeo de la llista de «Vídeo».</para>
      </step>
      </substeps>
      </step>
      </procedure>
      <note><para>Quan seleccioneu un vídeo, només podeu triar vídeos de l'actual directori. Si el vídeo que voleu obrir no està al mateix directori que el fitxer de subtítols, podeu obrir el vídeo després d'obrir el fitxer de subtítols.</para>
      </note>
  </sect2>
    <sect2 id="gs-saving-file">
    <title>Desant un fitxer</title>
      <procedure>
      <title>Desant un fitxer</title>
      <para>Podeu desar el fitxer normalment, o utilitzar «Anomena i desa» per a establir diferents opcions.</para>
      <step performance="required">
      <para lang="en">
      Go to <menuchoice>
              <guisubmenu>File</guisubmenu>
              <guimenuitem>Save</guimenuitem>
            </menuchoice>. </para>
      <para>La finestra «Obre un fitxer» s'hauria d'obrir</para>
      </step>
    </procedure>
    <note><para>Si esteu desant un nou fitxer, apareixerà la finestra «Anomena i desa».</para></note>
    <procedure>
      <title>Desant a un fitxer nou</title>
      <step performance="required">
      <para lang="en">
      Go to <menuchoice>
              <guisubmenu>File</guisubmenu>
              <guimenuitem>Save As</guimenuitem>
            </menuchoice>. </para>
      <para>La finestra «Anomena i desa» s'hauria d'obrir</para>
      </step>
      <step performance="required"><para>Introduïu un nom nou per al fitxer de subtítols</para></step>
      <step performance="optional"><para>Si voleu desar el fitxer a un lloc diferent, cerca el nou lloc al navegador de fitxers</para></step>
      <step performance="optional"><para>Si voleu desar el fitxer en un format diferent que l'especificat a la llista de «Format del subtítol», trieu un format diferent.</para></step>
      <step performance="optional"><para>Si voleu desar el fitxer en una codificació de caràcters diferent a la codificació per defecte que està a la llista, seleccioneu la codificació a la llista de codificació de caràcters.</para></step>
      <step performance="required"><para>Cliqueu «Desa»</para></step>
    </procedure>
  </sect2>
  
    <sect2 id="gs-select-char-coding">
    <title>Selecció d'una codificació de caràcters</title>
    <para>Si utilitzeu caràcters especials en el vostre fitxer de subtítols, voldreu estar segur que el vostre fitxer és desat en una codificació que admeti aquests caràcters.</para>
    <caution><para>Desar un fitxer en una codificació de caràcters que no admeti caràcters especials pot causar la pèrdua de caràcters. Això es produeix quan proveu de desar caràcters de múltiples bytes en una codificació de caràcters d'un byte.</para></caution>
  </sect2>
    <sect2 id="gs-editing-headers">
    <title>Editant les capçaleres dels subtítols</title>

        <procedure>
  <para>Alguns formats de subtítols tenen capçaleres de fitxer que contenen informació del fitxer. Podeu editar aquests camps amb el <application>Gnome Subtitles</application></para>
      <step performance="required">
      <para lang="en">
      Go to <menuchoice>
              <guisubmenu>File</guisubmenu>
              <guimenuitem>Headers</guimenuitem>
            </menuchoice>. </para>
      <para>La finestra de «Capçaleres» s'obrirà. Hi ha quatre pestanyes en aquesta.</para>
      </step>
      <step performance="required"> <para>Seleccioneu la pestanya que correspon al format del vostre fitxer de subtítols</para></step>
      <step performance="required"><para>Ompliu els camps apropiats.</para></step>
      <step performance="required"><para>Quan estigui introduïda la informació de la capçalera, cliqueu «D'acord»</para></step>
    </procedure>
  </sect2>
</sect1>
    <!-- ============= Working with Subtitles ================================== -->
<sect1 id="gs-working-with-subs">
<title>Treballant amb subtítols</title>
<note><para>Aquesta ajuda llista les dreceres de teclat per a fer les tasques. Totes les tasques també es poden fer utilitzant el menú «Edita» en comptes de les dreceres de teclat.</para></note>
      <sect2 id="gs-wws-adding">
        <title>Afegint un subtítol</title>
        <para>Els subtítols nous són afegits abans o després del subtítol actual.</para>
        <procedure>
          <title>Per a afegir un subtítol nou després del subtítol actual</title>
      <step performance="required">
      <para lang="en">Type <keycap>Insert</keycap> or <keycap>Ctrl</keycap>+<keycap>Enter</keycap></para>
      </step>
    </procedure>
            <procedure>
          <title>Per a afegir un subtítol nou abans del subtítol actual</title>
      <step performance="required">
      <para lang="en">Type <keycap>Shift</keycap>+<keycap>Ctrl</keycap>+<keycap>Enter</keycap></para>
      </step>
    </procedure>
      </sect2>     
      <sect2 id="gs-wws-moving">
        <title>Movent-se entre els subtítols</title>
        <para>Per a seleccionar subtítols podeu utilitzar el ratolí per a clicar el subtítol, o quan el cursor està a la finestra d'edició podeu utilitzar les dreceres de teclat per a moure-us entre els subtítols.</para>
        <procedure>
          <title>Per a anar al següent subtítol</title>
      <step performance="required">
      <para lang="en">Type <keycap>Ctrl</keycap>+<keycap>Page Down</keycap></para>
      </step>
    </procedure>
            <procedure>
          <title>Per a anar a l'anterior subtítol</title>
      <step performance="required">
      <para lang="en">Type <keycap>Ctrl</keycap>+<keycap>Page Up</keycap></para>
      </step>
    </procedure>
      </sect2>     
      <sect2 id="gs-wws-removing">
        <title>Suprimint subtítols</title>
      <procedure><step><para>Premeu <keycap>Suprimir</keycap></para></step></procedure>
      </sect2>      
      <sect2 id="gs-wws-batch">
        <title>Treballant amb múltiples subtítols</title>
        <para lang="en">Sometimes you will want to select multiple subtitles.  Use <keycap>Shift</keycap> to
          select sequential subtitles, and <keycap>Ctrl</keycap> to select non-sequential subtitles</para>
        <para lang="en">To select all subtitles type <keycap>Ctrl</keycap>+<keycap>A</keycap></para>
      </sect2>      
      <sect2 id="gs-wws-editing">
        <title>Editant text</title>
        <para>Per a editar text en un subtítol</para>
          <procedure>
            <step><para>Seleccioneu el subtítol</para></step>
            <step><para>Cliqueu a la finestra d'edició per a moure el cursor a la finestra d'edició</para></step>
          </procedure>
      </sect2>
      <sect2 id="gs-wws-format">
        <title>Format del text</title>
        <note><para>Només els tipus de format negreta, cursiva i subratllat poden ser aplicats a tota la línia. Actualment si només voleu alguns caràcters formatats especialment, haureu d'editar el fitxer de subtítols en un editor de text.</para></note>
      </sect2>
      <sect2 id="gs-wws-oops">
        <title>Desfent i refent</title>
        <procedure><title>Per a desfer una acció</title><step><para lang="en">Type <keycap>Ctrl</keycap>+
              <keycap>Z</keycap></para></step></procedure>
        <procedure><title>Per a refer una acció</title><step><para lang="en">Type <keycap>Ctrl</keycap>+
              <keycap>Y</keycap> or <keycap>Ctrl</keycap>+<keycap>Shift</keycap>+<keycap>Z
              </keycap></para></step></procedure>
      </sect2>      
      <sect2 id="gs-wws-ccp">
        <title>Retallant, copiant i enganxant</title>
        <para>Podeu retallar, copiar i enganxar text en un subtítol.</para>
        <procedure><title>Per a copiar text</title><step><para lang="en"><keycap>Ctrl</keycap>+
              <keycap>C</keycap></para></step></procedure>
        <procedure><title>Per a retallar text</title><step><para lang="en"><keycap>Ctrl</keycap>+
              <keycap>X</keycap></para></step></procedure>
        <procedure><title>Per a enganxar text</title><step><para lang="en"><keycap>Ctrl</keycap>+
              <keycap>V</keycap></para></step></procedure>
      </sect2>
</sect1>
    <!-- ============= find/replace ================================== -->
<!--    <sect1 id="gs-findreplace">
      <title>Finding and Replacing</title>
      <sect2 id="gs-fr-finding">
        <title>Finding</title>
        <para>Questions and general discussions should be posted on the <ulink url="http://sourceforge.net/forum/?group_id=129996" type="http">Forum</ulink> (no registration needed).</para>
      </sect2>
            <sect2 id="gs-fr-replacing">
        <title>Replacing</title>
        <para>You might get a quick response by asking the <ulink url="http://lists.sourceforge.net/lists/listinfo/gnome-subtitles-general" type="http">Mailing List</ulink> (<ulink url="http://sourceforge.net/mailarchive/forum.php?forum=gnome-subtitles-general" type="http">archives</ulink> are available).</para>
      </sect2>
      <sect2 id="gs-fr-options">
        <title>Find and Replace Options</title>
        <para>Questions and general discussions should be posted on the <ulink url="http://sourceforge.net/forum/?group_id=129996" type="http">Forum</ulink> (no registration needed).</para>
      </sect2>
      <sect2 id="gs-regex">
        <title>Using Regular Expressions</title>
        <para>Bugs and Feature Requests can be reported at the official <ulink url="http://bugzilla.gnome.org/browse.cgi?product=gnome-subtitles" type="http">Gnome Bugzilla</ulink>.</para>
      </sect2>
    </sect1>
-->
    <!-- ============= Timings ================================== -->
    <sect1 id="gs-timings">
      <title>Manipulant subtítols</title>
      <sect2 id="gs-timings-settingview">
        <title>Establint les unitats dels subtítols</title>
        <para>En el <application>Gnome Subtitles</application> podeu establir i ajustar els subtítols en unitats de temps (minuts i segons) o en unitats de fotogrames. La unitat per defecte és el temps.</para>
            <procedure><title>Per a utilitzar unitats de fotogrames</title>
              <step><para lang="en">Go to <menuchoice>
              <guisubmenu>View</guisubmenu>
              <guimenuitem>Frames</guimenuitem>
            </menuchoice>. </para></step></procedure>
              <procedure><title>Per a utilitzar unitats de temps</title>
              <step><para lang="en">Go to <menuchoice>
              <guisubmenu>View</guisubmenu>
              <guimenuitem>Times</guimenuitem>
            </menuchoice>. </para></step></procedure>
      </sect2>
      <sect2 id="gs-timings-Adjusting">
        <title>Ajustant la temporització</title>
           <procedure>
             <step><para>Si voleu ajustar la temporització de només alguns subtítols, seleccioneu els subtítols als quals voleu ajustar la temporització</para></step>
              <step><para lang="en">Go to <menuchoice>
              <guisubmenu>Timings</guisubmenu>
              <guimenuitem>Adjust</guimenuitem>
            </menuchoice>. </para></step>
            <step><para>Trieu el nou temps d'inici del primer subtítol i el nou temps d'inici de l'últim subtítol</para></step>
      </procedure>
      </sect2>
            <sect2 id="gs-timings-Shifitng">
        <title>Desplaçant la temporització</title>
        <para>Desplaçar la temporització us permet moure un o més subtítols endavant o endarrere una determinada quantitat. A diferència de la funció d'ajustar subtítols, desplaçar subtítols no afecta a la duració dels subtítols.</para>
              <procedure><title>Per a desplaçar subtítols</title>
                <step><para>Seleccioneu el(s) subtítol(s) que voleu desplaçar. Si voleu desplaçar el temps/fotogrames d'un subtítol i/o tots els subtítols d'abans o després del subtítol especificat, seleccioneu aquest subtítol. Si voleu desplaçar més d'un subtítol, seleccioneu tots els subtítols que voleu desplaçar.</para></step>
              <step><para lang="en">Go to <menuchoice>
              <guisubmenu>Timings</guisubmenu>
              <guimenuitem>Shift</guimenuitem>
            </menuchoice>. </para></step>
            <step><para>Introduïu la quantitat de temps (o fotogrames, si esteu utilitzant el fotograma com a unitat) a desplaçar els vostres subtítols. Si voleu moure els subtítols endarrere, introduïu un valor negatiu, altrament introduïu un valor positiu.</para></step>
            <step><para>Seleccioneu quant voleu desplaçar els vostres subtítols.</para></step>
            <step><para>Cliqueu «Desplaça» per a desplaçar els subtítols.</para></step>
      </procedure>
      </sect2>
      <sect2 id="gs-timings-inputfr">
        <title>Establint la freqüència dels fotogrames d'entrada i del vídeo</title>
        <para>Quan obriu un vídeo, el <application>Gnome Subtitles</application> no estableix automàticament la freqüència dels fotogrames del vídeo.</para>
        <para>El <application>Gnome Subtitles</application> manipula els subtítols basant-se amb el temps, independentment del mode de visualització. Quan canvieu la freqüència dels fotogrames del vídeo, el <application>Gnome Subtitles</application> ajusta els fotogrames per a compensar el canvi. La duració dels subtítols es manté igual.</para>
        <caution><para>Si el format dels vostres subtítols utilitza fotogrames per a determinar quan es visualitza un subtítol (MicroDVD), canviar la freqüència dels fotogrames del vídeo pot causar que els vostres subtítols perdin la sincronització.</para></caution>
        <procedure><title>Per a establir la freqüència dels fotogrames del vídeo</title>
              <step><para lang="en">Go to <menuchoice>
              <guisubmenu>Timings</guisubmenu>
              <guimenuitem>Video Frame Rate</guimenuitem>
            </menuchoice> and select the appropriate framerate.</para></step>
      </procedure>
      <procedure><title>Per a establir la freqüència dels fotogrames d'entrada</title>
              <step><para lang="en">Go to <menuchoice>
              <guisubmenu>Timings</guisubmenu>
              <guimenuitem>Input Frame Rate</guimenuitem>
            </menuchoice> and select the appropriate framerate.</para></step>
      </procedure>
      </sect2>
    </sect1>
     <!-- ============= Video ================================== -->
    <sect1 id="gs-video">
      <title>Treballant amb vídeos</title>
      <sect2 id="gs-video-opening">
        <title>Obrint i tancant un vídeo</title>
        <procedure>
          <title>Per a obrir un vídeo</title>
          <step><para lang="en">Go to <menuchoice>
              <guisubmenu>Video</guisubmenu>
              <guimenuitem>Open</guimenuitem>
            </menuchoice>
          </para></step>
        </procedure>
        <procedure>
          <title>Per a tancar un vídeo</title>
          <step><para lang="en">Go to <menuchoice>
              <guisubmenu>Video</guisubmenu>
              <guimenuitem>Close</guimenuitem>
            </menuchoice>
          </para></step>
        </procedure>
      </sect2>
      <sect2 id="gs-video-playback">
        <title>Reproduint</title>
        <para>El <application>Gnome Subtitles</application> reproduirà el vídeo a la finestra de visualització amb els vostres subtítols.</para>
      </sect2>
      <sect2 id="gs-video-seeking">
        <title>Cercant fins a la selecció</title>
        <para>Podeu anar al punt del vídeo carregat on un subtítol comença</para>
        <procedure>
          <title>Per a cercar fins a un subtítol</title>
          <step><para lang="en">Go to <menuchoice>
              <guisubmenu>Video</guisubmenu>
              <guimenuitem>Seek to Selection</guimenuitem>
            </menuchoice>
          </para></step>
        </procedure>
      </sect2>
      <sect2 id="gs-video-settingsubs">
        <title>Establint les posicions d'inici o de final dels subtítols</title>
        <para>Podeu establir el temps d'inici o de final dels subtítols utilitzant el punt en què el vídeo carregat està en pausa</para>
        <procedure>
          <title>Per a establir el temps d'inici del subtítol</title>
          <step><para lang="en">Go to <menuchoice>
              <guisubmenu>Video</guisubmenu>
              <guimenuitem>Set Subtitle Start</guimenuitem>
            </menuchoice>
          </para></step>
        </procedure>
        <procedure>
          <title lang="en">To set subtitle ending time</title>
          <step><para lang="en">Go to <menuchoice>
              <guisubmenu>Video</guisubmenu>
              <guimenuitem>Set Subtitle End</guimenuitem>
            </menuchoice>
          </para></step>
        </procedure>
      </sect2>
    </sect1>
   <!-- ============= Support ================================== -->
    <sect1 id="gs-support">
      <title>On aconseguir suport addicional</title>
      <sect2 id="gs-support-forums">
        <title>Fòrums</title>
        <para>Les preguntes i discussions en general s'haurien d'enviar al <ulink url="http://sourceforge.net/forum/?group_id=129996" type="http">Fòrum</ulink> (no és necessari registrar-se).</para>
      </sect2>
            <sect2 id="gs-support-mailinglist">
        <title>Llista de correu</title>
        <para>Podeu aconseguir una resposta ràpida preguntant a la <ulink url="http://lists.sourceforge.net/lists/listinfo/gnome-subtitles-general" type="http">Llista de correu</ulink> (hi ha <ulink url="http://sourceforge.net/mailarchive/forum.php?forum_name=gnome-subtitles-general" type="http">arxius</ulink> disponibles).</para>
      </sect2>
      <sect2 id="gs-support-bugzilla">
        <title>Errors i característiques</title>
        <para lang="en">Bugs and Feature Requests can be reported in the <ulink url="https://gitlab.gnome.org/GNOME/gnome-subtitles/issues/" type="http">GNOME issue tracker</ulink>.</para>
      </sect2>
    </sect1>
  </article>
